package br.com.estiloeco.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.FriendsDataSource;
import br.com.estiloeco.database.NotificationsDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.Notification;
import br.com.estiloeco.model.User;

/**
 * Created by paulomartins on 22/11/14.
 */
public class NotificationAdapter extends BaseAdapter {

    private Context context;
    private List<Notification> list;
    private ListView lvNotifications;
    private TextView tvNoNotifications;
    private Activity activity;
    private RequestQueue request;
    // Datasource
    private NotificationsDataSource notificationsDataSource;
    private FriendsDataSource friendsDataSource;

    public NotificationAdapter(Context context, List<Notification> list, ListView lvNotifications, TextView tvNoNotifications, Activity activity) {
        this.context = context;
        this.list = list;
        this.lvNotifications = lvNotifications;
        this.tvNoNotifications = tvNoNotifications;
        this.activity = activity;
        this.request = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Notification getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_notification, null);
        }

        // Datasource
        notificationsDataSource = Notification.datasource(context);
        friendsDataSource = Friend.datasource(context);

        // Object
        final Notification notification = list.get(i);

        ImageView ivAvatar = (ImageView) view.findViewById(R.id.ivAvatar);
        TextView tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        TextView tvDate = (TextView) view.findViewById(R.id.tvDate);
        Button btnAccept = (Button) view.findViewById(R.id.btnAccept);
        Button btnRefuse = (Button) view.findViewById(R.id.btnRefuse);

        if (Helper.validString(notification.getUserAvatar())) {
            Picasso.with(context).load(AppConfig.ASSETS_URL + notification.getUserAvatar()).skipMemoryCache()
                    .placeholder(R.drawable.default_user).transform(new Helper().new CircleTransform()).into(ivAvatar);
        }

        tvMessage.setText(notification.getUserName() + " " + context.getResources().getString(R.string.message_new_friend));
        tvDate.setText(Helper.convertSqlDateToString(notification.getDate(), true, context));

        if (list.get(i).getStatus() == Notification.READ) {
            view.setBackgroundColor(context.getResources().getColor(R.color.new_notification));
        }

        // Actions
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriend(i);
            }
        });

        btnRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationsDataSource.delete(notification);

                // Remove from list
                list.remove(i);
                notifyDataSetChanged();

                if (list.size() == 0) {
                    tvNoNotifications.setVisibility(View.VISIBLE);
                    lvNotifications.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void addFriend(final int listPosition) {
        User user = User.logged(context);
        String url = AppConfig.webserviceUrl("profile/friend");
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(user.getId()));
        params.put("friendId", String.valueOf(list.get(listPosition).getUserId()));
        params.put("action", "add");

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        activity.setProgressBarIndeterminateVisibility(Boolean.FALSE);

                        try {
                            if (response.getBoolean("status")) {
                                // Friend
                                Friend friend = new Friend();
                                friend.setId(list.get(listPosition).getUserId());
                                friend.setName(list.get(listPosition).getUserName());
                                friend.setAvatar(list.get(listPosition).getUserAvatar());
                                friend.setStatus(Friend.STATUS_ACCEPTED);

                                friendsDataSource.add(friend);

                                // Remove from database
                                notificationsDataSource.delete(list.get(listPosition));

                                Toast.makeText(context, activity.getResources().getString(R.string.message_invite_accepted),
                                        Toast.LENGTH_SHORT).show();

                                // Remove from list
                                list.remove(listPosition);
                                notifyDataSetChanged();

                                if (list.size() == 0) {
                                    tvNoNotifications.setVisibility(View.VISIBLE);
                                    lvNotifications.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, NotificationAdapter.this.getClass().toString() + " - " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        activity.setProgressBarIndeterminateVisibility(Boolean.FALSE);

                        Log.e(AppConfig.TAG, NotificationAdapter.this.getClass().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        activity.setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }
}
