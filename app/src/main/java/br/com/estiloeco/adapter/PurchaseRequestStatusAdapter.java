package br.com.estiloeco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.PurchaseRequestStatus;

/**
 * Created by paulomartins on 07/11/14.
 */
public class PurchaseRequestStatusAdapter extends BaseAdapter {

    private Context context;
    private List<PurchaseRequestStatus> list;

    public PurchaseRequestStatusAdapter(Context context, List<PurchaseRequestStatus> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public PurchaseRequestStatus getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_purchase_request_status, null);
        }

        PurchaseRequestStatus purchaseRequestStatus = list.get(i);

        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        TextView tvDate = (TextView) view.findViewById(R.id.tvDate);

        tvTitle.setText(purchaseRequestStatus.getTitle());
        tvDescription.setText(purchaseRequestStatus.getDescription());
        tvDate.setText(Helper.convertSqlDateToString(purchaseRequestStatus.getDate(), false, context));

        return view;
    }
}
