package br.com.estiloeco.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.database.ItemCartDataSource;
import br.com.estiloeco.helpers.NumberFormats;
import br.com.estiloeco.model.ItemCart;
import br.com.estiloeco.model.Product;
import br.com.estiloeco.ui.ProductActivity;

/**
 * Created by paulomartins on 14/01/15.
 */
public class ProductAdapter extends BaseAdapter {

    private Context context;
    private List<Product> list;

    public ProductAdapter() {
    }

    public ProductAdapter(Context context, List<Product> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_product, null);
        }

        TextView tvProductName = (TextView) view.findViewById(R.id.tvProductName);
        TextView tvProductCost = (TextView) view.findViewById(R.id.tvProductCost);
        ImageView ivProductImage = (ImageView) view.findViewById(R.id.ivProductImage);
        Button btnAddToShoppingCart = (Button) view.findViewById(R.id.btnAddToShoppingCart);

        tvProductCost.setText("R$ " + NumberFormats.toMoney(list.get(i).getCost()));
        tvProductName.setText(list.get(i).getName());

        Picasso.with(context).load(list.get(i).getImage()).into(ivProductImage);

        // Action
        ivProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("productId", list.get(i).getId());
                intent.putExtra("skuId", 0);
                context.startActivity(intent);
            }
        });

        btnAddToShoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                final View dialogView = inflater.inflate(R.layout.dialog_add_to_shopping_cart, null);
                final AlertDialog dialog = new AlertDialog.Builder(context).create();
                dialog.setView(dialogView);

                // Components
                final Spinner spSize = (Spinner) dialogView.findViewById(R.id.spSize);
                final Spinner spAmount = (Spinner) dialogView.findViewById(R.id.spAmount);
                Button btnAdd = (Button) dialogView.findViewById(R.id.btnAdd);
                Button btnCancel = (Button) dialogView.findViewById(R.id.btnCancel);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, list.get(i).getSizes());
                spSize.setAdapter(adapter);

                // Amount
                List<String> amount = new ArrayList<String>();
                amount.add("1");
                amount.add("2");
                amount.add("3");
                amount.add("4");
                amount.add("5");

                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, amount);
                spAmount.setAdapter(adapter2);

                // Actions
                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ItemCart itemCart = new ItemCart();
                        Product product = list.get(i);

                        itemCart.setProduct(product.getName());
                        itemCart.setProductId(product.getId());
                        itemCart.setBrand(product.getBrand());
                        itemCart.setSku(list.get(i).getSkus().get(spSize.getSelectedItemPosition()).getId());
                        itemCart.setSize(spSize.getSelectedItem().toString());
                        itemCart.setAmount(Integer.valueOf(spAmount.getSelectedItem().toString()));
                        itemCart.setImage(product.getImageShoppingCart());
                        itemCart.setCost(product.getCost());

                        ItemCartDataSource datasource = ItemCart.datasource(context);
                        datasource.addItem(itemCart);

                        Toast.makeText(context, context.getResources().getString(R.string.message_item_added), Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        return view;
    }
}
