package br.com.estiloeco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.helpers.NumberFormats;
import br.com.estiloeco.model.PurchaseRequest;

/**
 * Created by paulomartins on 07/11/14.
 */
public class PurchaseRequestAdapter extends BaseAdapter {

    private Context context;
    private List<PurchaseRequest> list;

    public PurchaseRequestAdapter(Context context, List<PurchaseRequest> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public PurchaseRequest getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_purchase_request, null);
        }

        PurchaseRequest purchaseRequest = list.get(i);

        TextView tvNumber = (TextView) view.findViewById(R.id.tvNumber);
        TextView tvDate = (TextView) view.findViewById(R.id.tvDate);
        TextView tvLastStatus = (TextView) view.findViewById(R.id.tvLastStatus);
        TextView tvCost = (TextView) view.findViewById(R.id.tvCost);

        tvNumber.setText(String.valueOf(purchaseRequest.getId()));
        tvDate.setText(Helper.convertSqlDateToString(purchaseRequest.getDate(), false, context));
        tvLastStatus.setText(purchaseRequest.getStatus().get(purchaseRequest.getStatus().size()-1)
                .getDescription().toUpperCase());
        tvCost.setText(context.getResources().getString(R.string.label_real) + " " +
                NumberFormats.toMoney(purchaseRequest.getCost()));

        return view;
    }
}
