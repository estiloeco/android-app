package br.com.estiloeco.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.FriendsDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Friend;

/**
 * Created by paulomartins on 29/10/14.
 */
public class FriendsAdapter extends BaseAdapter {

    private Context context;
    private List<Friend> list;
    private long userId;
    private Activity activity;
    private RequestQueue request;
    private ListView lvFriends;
    private TextView tvNoFriends;

    public FriendsAdapter(Context context, long userId, Activity activity, ListView lvFriends, TextView tvNoFriends) {
        this.context = context;
        this.list = new ArrayList<Friend>();
        this.userId = userId;
        this.activity = activity;
        this.request = Volley.newRequestQueue(context);
        this.lvFriends = lvFriends;
        this.tvNoFriends = tvNoFriends;
    }

    public void refresh(List<Friend> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void remove(int position) {
        list.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Friend getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_friends, null);
        }

        final Friend friend = list.get(i);

        ImageView ivUserPhoto = (ImageView) view.findViewById(R.id.ivUserPhoto);
        TextView tvUserName = (TextView) view.findViewById(R.id.tvUserName);
        ImageButton btnAddUser = (ImageButton) view.findViewById(R.id.btnAddUser);
        ImageButton btnRemoveUser = (ImageButton) view.findViewById(R.id.btnRemoveUser);
        TextView tvStatus = (TextView) view.findViewById(R.id.tvStatus);

        tvUserName.setText(friend.getName());

        if (Helper.validString(friend.getAvatar())) {
            Picasso.with(context).load(AppConfig.ASSETS_URL + friend.getAvatar()).skipMemoryCache()
                    .placeholder(R.drawable.default_user).transform(new Helper().new CircleTransform()).into(ivUserPhoto);
        }

        btnRemoveUser.setVisibility(View.VISIBLE);
        btnRemoveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFriendStatus(i);
            }
        });

        return view;
    }

    private void changeFriendStatus(final int listPosition) {
        String url = AppConfig.webserviceUrl("profile/friend");
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(userId));
        params.put("friendId", String.valueOf(list.get(listPosition).getId()));
        params.put("action", "remove");

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        activity.setProgressBarIndeterminateVisibility(Boolean.FALSE);

                        try {
                            if (response.getBoolean("status")) {
                                FriendsDataSource friendsDataSource = Friend.datasource(context);
                                friendsDataSource.delete(list.get(listPosition));

                                Toast.makeText(context, activity.getResources().getString(R.string.message_user_removed),
                                        Toast.LENGTH_SHORT).show();

                                remove(listPosition);

                                if (list.size() == 0) {
                                    lvFriends.setVisibility(View.GONE);
                                    tvNoFriends.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, FriendsAdapter.this.getClass().toString() + " - " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        activity.setProgressBarIndeterminateVisibility(Boolean.FALSE);

                        Log.e(AppConfig.TAG, FriendsAdapter.this.getClass().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        activity.setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }
}
