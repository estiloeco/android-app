package br.com.estiloeco.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.database.ItemCartDataSource;
import br.com.estiloeco.helpers.NumberFormats;
import br.com.estiloeco.model.ItemCart;
import br.com.estiloeco.ui.MainActivity;
import br.com.estiloeco.ui.ProductActivity;

/**
 * Created by paulomartins on 10/10/14.
 */
public class ShoppingCartAdapter extends BaseAdapter {

    private Context context;
    private List<ItemCart> list;
    private ItemCartDataSource datasource;

    public ShoppingCartAdapter(Context context, List<ItemCart> list, ItemCartDataSource datasource) {
        this.context = context;
        this.list = list;
        this.datasource = datasource;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_shopping_cart, null);
        }

        TextView tvProductName = (TextView) view.findViewById(R.id.tvProductName);
        TextView tvProductBrand = (TextView) view.findViewById(R.id.tvProductBrand);
        TextView tvProductCost = (TextView) view.findViewById(R.id.tvProductCost);
        TextView tvProductSize = (TextView) view.findViewById(R.id.tvProductSize);
        TextView tvProductAmount = (TextView) view.findViewById(R.id.tvProductAmount);
        ImageView ivProductImage = (ImageView) view.findViewById(R.id.ivProductImage);
        final ImageButton btnRemoveItem = (ImageButton) view.findViewById(R.id.btnRemoveItem);

        // Data
        tvProductName.setText(list.get(i).getProduct());
        tvProductBrand.setText(list.get(i).getBrand());
        tvProductSize.setText(list.get(i).getSize());
        tvProductAmount.setText(String.valueOf(list.get(i).getAmount()));
        btnRemoveItem.setTag(list.get(i).getSku());
        tvProductCost.setText(context.getResources().getString(R.string.label_real) + " " +
                NumberFormats.toMoney(list.get(i).getCost()));

        Picasso.with(context).load(list.get(i).getImage()).into(ivProductImage);

        // Product detail
        ivProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("productId", list.get(i).getProductId());
                intent.putExtra("skuId", list.get(i).getSku());
                context.startActivity(intent);
            }
        });

        // Remove item
        btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                    .setMessage(R.string.message_sure_product)
                    .setPositiveButton(R.string.btn_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int ii) {
                            list.remove(i);
                            notifyDataSetChanged();

                            datasource.delete(Integer.valueOf(btnRemoveItem.getTag().toString()));

                            if (list.size() == 0 && context instanceof MainActivity) {
                                ((MainActivity)context).updateShoppingCart();
                            }
                            else {
                                ((MainActivity)context).updateShoppingCartTotal();
                            }

                            if (list.size() > 0) {
                                Toast.makeText(context, R.string.message_product_removed, Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(context, R.string.message_empty_shopping_cart, Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .setNegativeButton(R.string.btn_no, null)
                    .show();
            }
        });

        return view;
    }
}
