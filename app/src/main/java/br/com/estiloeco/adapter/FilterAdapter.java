package br.com.estiloeco.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.database.PersonalFiltersDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Filter;

/**
 * Created by paulomartins on 30/09/14.
 */
public class FilterAdapter extends BaseAdapter {

    private Context context;
    private List<Filter> filters;
    // Database
    PersonalFiltersDataSource personalFiltersDataSource;

    public FilterAdapter() {}

    public FilterAdapter(Context context, List<Filter> filters, PersonalFiltersDataSource personalFiltersDataSource) {
        this.context = context;
        this.filters = filters;
        this.personalFiltersDataSource = personalFiltersDataSource;
    }

    public FilterAdapter(Context context, PersonalFiltersDataSource personalFiltersDataSource) {
        this.context = context;
        this.filters = new ArrayList<Filter>();
        this.personalFiltersDataSource = personalFiltersDataSource;
    }

    public void refresh(List<Filter> filters) {
        this.filters = filters;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return filters.size();
    }

    @Override
    public Filter getItem(int i) {
        return filters.get(i);
    }

    @Override
    public long getItemId(int i) {
        return filters.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_filter, null);
        }

        TextView tvTitleFilter = (TextView) view.findViewById(R.id.tvTitleFilter);
        CheckBox chkFilter = (CheckBox) view.findViewById(R.id.chkFilter);

        tvTitleFilter.setText(filters.get(i).getName());
        chkFilter.setTag(i);
        chkFilter.setChecked(filters.get(i).isChecked());

        chkFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                int position = Integer.valueOf(compoundButton.getTag().toString());

                SharedPreferences.Editor editor = Helper.setPreferences(context, "update-list", true);

                if (isChecked) {
                    personalFiltersDataSource.addFilter((int)filters.get(position).getId());

                    if (!filters.get(position).isChecked()) {
                        editor.commit();
                    }

                    filters.get(position).setChecked(true);
                }
                else {
                    personalFiltersDataSource.deleteFilter((int)filters.get(position).getId());

                    if (filters.get(position).isChecked()) {
                        editor.commit();
                    }

                    filters.get(position).setChecked(false);
                }
            }
        });

        return view;
    }
}
