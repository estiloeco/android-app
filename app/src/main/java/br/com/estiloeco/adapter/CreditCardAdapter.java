package br.com.estiloeco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.database.CreditCardFlagDataSource;
import br.com.estiloeco.model.CreditCard;
import br.com.estiloeco.model.CreditCardFlag;

/**
 * Created by paulomartins on 17/10/14.
 */
public class CreditCardAdapter extends BaseAdapter {

    private Context context;
    private List<CreditCard> list;

    public CreditCardAdapter(Context context, List<CreditCard> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CreditCard getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_credit_card, null);
        }

        CreditCard creditCard = list.get(i);

        TextView tvCreditCardName = (TextView) view.findViewById(R.id.tvCreditCardName);
        TextView tvCCFlag = (TextView) view.findViewById(R.id.tvCreditCardFlag);
        TextView etCreditCardNumber = (TextView) view.findViewById(R.id.etCreditCardNumber);
        TextView etCCExpiry = (TextView) view.findViewById(R.id.etCCExpiry);

        CreditCardFlagDataSource creditCardFlagDataSource = CreditCardFlag.datasource(context);

        tvCreditCardName.setText(creditCard.getName());
        tvCCFlag.setText(creditCardFlagDataSource.get(creditCard.getFlagId()).getDescription());
        etCreditCardNumber.setText(creditCard.getNumber());
        etCCExpiry.setText(creditCard.getExpireDate());

        return view;
    }
}
