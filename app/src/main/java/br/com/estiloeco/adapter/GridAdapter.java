package br.com.estiloeco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Look;

/**
 * Created by paulomartins on 27/09/14.
 */
public class GridAdapter extends BaseAdapter {

    private Context context;
    private List<Look> list;

    public GridAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<Look>();
    }

    public GridAdapter(Context context, List<Look> list) {
        this.context = context;
        this.list = list;
    }

    public void refresh(List<Look> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void add(Look look) {
        list.add(look);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Look getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_grid, null);
        }

        Look look = list.get(i);

        int likes = look.getLikes();
        String image = look.getImage(0);

        ImageView ivLookImage = (ImageView) view.findViewById(R.id.ivLookImage);
        ImageView ivLookLikeIcon = (ImageView) view.findViewById(R.id.ivLookLikeIcon);
        ImageView friend1 = (ImageView) view.findViewById(R.id.friend1);
        ImageView friend2 = (ImageView) view.findViewById(R.id.friend2);
        TextView tvLookLikes = (TextView) view.findViewById(R.id.tvLookLikes);

        // Avatar
        if (Helper.validString(image)) {
            Picasso.with(context).load(image).into(ivLookImage);
        }
        else {
            Picasso.with(context).load(R.drawable.look_no_image).into(ivLookImage);
        }

        // User's like
        if (look.userLike()) {
            ivLookLikeIcon.setImageResource(R.drawable.ic_favorited_list);
        }
        else {
            ivLookLikeIcon.setImageResource(R.drawable.ic_favorite_list);
        }

        tvLookLikes.setText(String.valueOf(likes));

        // Friends
        if (look.getFriends() != null) {
            if (look.getFriends().size() > 0 && Helper.validString(look.getFriends().get(0).getAvatar())) {
                Picasso.with(context).load(AppConfig.ASSETS_URL + look.getFriends().get(0).getAvatar()).skipMemoryCache()
                        .transform(new Helper().new CircleTransform()).into(friend1);
            }

            if (look.getFriends().size() > 1 && Helper.validString(look.getFriends().get(1).getAvatar())) {
                Picasso.with(context).load(AppConfig.ASSETS_URL + look.getFriends().get(1).getAvatar()).skipMemoryCache()
                        .transform(new Helper().new CircleTransform()).into(friend2);
            }
        }

        return view;
    }
}
