package br.com.estiloeco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Address;

/**
 * Created by paulomartins on 17/10/14.
 */
public class AddressAdapter extends BaseAdapter {

    private Context context;
    private List<Address> list;

    public AddressAdapter(Context context, List<Address> list) {
        this.context = context;
        this.list = list;
    }

    public void refresh(List<Address> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Address getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_address, null);
        }

        Address address = list.get(i);

        TextView tvAddressName = (TextView) view.findViewById(R.id.tvAddressName);
        TextView tvPrincipalAddress = (TextView) view.findViewById(R.id.tvPrincipalAddress);
        TextView tvAddressStreetNumberComplement = (TextView) view.findViewById(R.id.tvAddressStreetNumberComplement);
        TextView tvAddressComplement = (TextView) view.findViewById(R.id.tvAddressComplement);
        TextView etAddressCepCityState = (TextView) view.findViewById(R.id.etAddressCepCityState);

        tvAddressName.setText(address.getName());
        tvAddressStreetNumberComplement.setText(address.getStreet() + ", " + address.getNumber());
        etAddressCepCityState.setText(address.getCity() + " / " + address.getState());
        tvAddressComplement.setText((Helper.validString(address.getComplement()) ? address.getComplement() + " - " : "") + address.getCep());

        if (address.isPrincipal()) {
            tvPrincipalAddress.setVisibility(View.VISIBLE);
        }
        else {
            tvPrincipalAddress.setVisibility(View.INVISIBLE);
        }

        return view;
    }
}
