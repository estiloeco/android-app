package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.Friend;

/**
 * Created by paulomartins on 30/01/15.
 */
public class FriendsDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_FRIENDS_COLUMN_STATUS,
            DatabaseHelper.TABLE_FRIENDS_COLUMN_NAME,
            DatabaseHelper.TABLE_FRIENDS_COLUMN_AVATAR
    };

    public FriendsDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(Friend friend) {
        if (get(friend.getId()) == null) {
            open();

            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.COLUMN_ID, friend.getId());
            values.put(DatabaseHelper.TABLE_FRIENDS_COLUMN_STATUS, friend.getStatus());
            values.put(DatabaseHelper.TABLE_FRIENDS_COLUMN_NAME, friend.getName());
            values.put(DatabaseHelper.TABLE_FRIENDS_COLUMN_AVATAR, friend.getAvatar());

            database.insert(DatabaseHelper.TABLE_FRIENDS, null, values);

            close();
        }
    }

    public Friend get(long id) {
        Friend friend = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_FRIENDS, allColumns,
                DatabaseHelper.COLUMN_ID + " = ? ", new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                friend = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return friend;
    }

    public Friend getByName(String name) {
        Friend friend = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_FRIENDS, allColumns,
                DatabaseHelper.TABLE_FRIENDS_COLUMN_NAME + "= ?", new String[]{name}, null, null, null, "1");

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                friend = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return friend;
    }

    public List<Friend> getAll() {
        List<Friend> friends = new ArrayList<Friend>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_FRIENDS, allColumns,
                DatabaseHelper.TABLE_FRIENDS_COLUMN_STATUS + " = ?", new String[]{String.valueOf(Friend.STATUS_ACCEPTED)}, null, null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            friends.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return friends;
    }

    public List<String> getAllNames() {
        List<Friend> friends = getAll();
        List<String> friendsNames = new ArrayList<String>();

        for (Friend friend : friends) {
            friendsNames.add(friend.getName());
        }

        return friendsNames;
    }

    public void updateStatus(long id, int status) {
        ContentValues values = new ContentValues();

        open();

        values.put(DatabaseHelper.TABLE_FRIENDS_COLUMN_STATUS, status);

        database.update(DatabaseHelper.TABLE_FRIENDS, values, DatabaseHelper.COLUMN_ID +
                " = ?", new String[]{String.valueOf(id)});

        close();
    }

    public int count() {
        open();

        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM " + DatabaseHelper.TABLE_FRIENDS + " WHERE " +
                DatabaseHelper.TABLE_FRIENDS_COLUMN_STATUS + " = ?", new String[]{String.valueOf(Friend.STATUS_ACCEPTED)});

        cursor.moveToFirst();

        int count = cursor.getInt(0);

        cursor.close();
        close();

        return count;
    }

    public void delete(Friend friend) {
        open();

        database.delete(DatabaseHelper.TABLE_FRIENDS, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(friend.getId())});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_FRIENDS, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private Friend cursorToObject(Cursor cursor) {
        Friend friend = new Friend();
        friend.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        friend.setStatus(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TABLE_FRIENDS_COLUMN_STATUS)));
        friend.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_FRIENDS_COLUMN_NAME)));
        friend.setAvatar(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_FRIENDS_COLUMN_AVATAR)));

        return friend;
    }
}
