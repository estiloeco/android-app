package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.ItemCart;

/**
 * Created by paulomartins on 21/01/15.
 */
public class ItemCartDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_BRAND,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_PRODUCT_ID,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_PRODUCT,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_DESCRIPTION,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SKU,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SIZE,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_AMOUNT,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_IMAGE,
            DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_COST};

    public ItemCartDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            Log.w(AppConfig.TAG, "Database open!");
            database = dbHelper.getWritableDatabase();
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void addItem(ItemCart item) {
        open();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_BRAND, item.getBrand());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_PRODUCT_ID, item.getProductId());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_PRODUCT, item.getProduct());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SKU, item.getSku());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SIZE, item.getSize());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_AMOUNT, item.getAmount());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_IMAGE, item.getImage());
        values.put(DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_COST, String.valueOf(item.getCost()));

        database.insert(DatabaseHelper.TABLE_SHOPPING_CART, null, values);

        close();
    }

    public ArrayList<ItemCart> getAll() {
        ArrayList<ItemCart> items = new ArrayList<ItemCart>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_SHOPPING_CART, allColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ItemCart itemCart = new ItemCart();

            itemCart.setBrand(cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_BRAND)));
            itemCart.setProductId(cursor.getLong(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_PRODUCT_ID)));
            itemCart.setProduct(cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_PRODUCT)));
            itemCart.setSku(cursor.getInt(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SKU)));
            itemCart.setSize(cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SIZE)));
            itemCart.setAmount(cursor.getInt(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_AMOUNT)));
            itemCart.setImage(cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_IMAGE)));
            itemCart.setCost(Float.parseFloat(cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_COST))));

            items.add(itemCart);

            cursor.moveToNext();
        }

        cursor.close();
        close();

        return items;
    }

    public float getTotal() {
        List<ItemCart> items = getAll();
        Float total = 0.0f;

        for (int i = 0; i < items.size(); i++) {
            total += items.get(i).getCost();
        }

        return total;
    }

    public void delete(Integer skuId) {
        open();

        database.delete(DatabaseHelper.TABLE_SHOPPING_CART,
                DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SKU+ " = " + skuId, null);

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_SHOPPING_CART, DatabaseHelper.TABLE_SHOPPING_CART_COLUMN_SKU + " > -1", null);

        close();
    }

}
