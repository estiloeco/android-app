package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Notification;

/**
 * Created by paulomartins on 30/03/15.
 */
public class NotificationsDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_TYPE,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_ID,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_NAME,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_AVATAR,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_MESSAGE,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_STATUS,
            DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_DATE
    };

    public NotificationsDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            Log.w(AppConfig.TAG, "Database open!");
            database = dbHelper.getWritableDatabase();
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(Notification notification) {
        open();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_ID, notification.getId());
        values.put(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_TYPE, notification.getType());
        values.put(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_ID, notification.getUserId());
        values.put(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_NAME, notification.getUserName());
        values.put(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_AVATAR, notification.getUserAvatar());
        values.put(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_MESSAGE, notification.getMessage());
        values.put(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_DATE, Helper.getDatetime());

        database.insert(DatabaseHelper.TABLE_NOTIFICATIONS, null, values);

        close();
    }

    public List<Notification> getAll() {
        List<Notification> friends = new ArrayList<Notification>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_NOTIFICATIONS, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            friends.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return friends;
    }

    public int count() {
        open();

        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM " + DatabaseHelper.TABLE_NOTIFICATIONS, null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);

        cursor.close();
        close();

        return count;
    }

    public void delete(Notification notification) {
        open();

        database.delete(DatabaseHelper.TABLE_NOTIFICATIONS, DatabaseHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(notification.getId())});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_NOTIFICATIONS, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private Notification cursorToObject(Cursor cursor) {
        Notification notification = new Notification();
        notification.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        notification.setType(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_TYPE)));
        notification.setUserId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_ID)));
        notification.setUserName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_NAME)));
        notification.setUserAvatar(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_USER_AVATAR)));
        notification.setMessage(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_MESSAGE)));
        notification.setStatus(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_STATUS)));
        notification.setDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_NOTIFICATIONS_COLUMN_DATE)));

        return notification;
    }
}
