package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.estiloeco.config.AppConfig;

/**
 * Created by paulomartins on 16/01/15.
 */
public class PersonalFiltersDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID};

    public PersonalFiltersDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            Log.w(AppConfig.TAG, "Database open!");
            database = dbHelper.getWritableDatabase();
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void addFilter(int filterId) {
        if (!filterExists(filterId)) {
            open();

            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID, filterId);

            database.insert(DatabaseHelper.TABLE_FILTERS, null, values);

            close();
        }
    }

    public boolean filterExists(Integer filterId) {
        boolean exists = false;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_FILTERS, allColumns,
                DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID + " = ?", new String[]{String.valueOf(filterId)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst() && cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID)) == filterId) {
                exists = true;
            }
        }

        cursor.close();
        close();

        return exists;
    }

    public List<Integer> getAll() {
        List<Integer> filters = new ArrayList<Integer>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_FILTERS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            filters.add(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID)));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return filters;
    }

    public void deleteFilter(Integer filterId) {
        open();

        database.delete(DatabaseHelper.TABLE_FILTERS,
                DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID + " = " + filterId, null);

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_FILTERS,
                DatabaseHelper.TABLE_FILTERS_COLUMN_FILTER_ID + " > ?", new String[]{"-1"});

        close();
    }
}
