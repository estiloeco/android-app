package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.PurchaseRequestItem;

/**
 * Created by paulomartins on 15/04/15.
 */
public class PurchaseRequestItemDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_PURCHASE_REQUEST,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_SKU,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_AMOUNT,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_COST
    };

    public PurchaseRequestItemDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(PurchaseRequestItem purchaseRequestItem) {
        ContentValues values = new ContentValues();

        open();

        values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_PURCHASE_REQUEST, purchaseRequestItem.getPurchaseRequest());
        values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_SKU, purchaseRequestItem.getSku());
        values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_AMOUNT, purchaseRequestItem.getAmount());
        values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_COST, purchaseRequestItem.getCost());

        database.insert(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM, null, values);

        close();
    }

    public void addMany(ArrayList<PurchaseRequestItem> items) {
        for (PurchaseRequestItem purchaseRequestItem : items) {
            add(purchaseRequestItem);
        }
    }

    public ArrayList<PurchaseRequestItem> getAll(long purchaseRequestId) {
        ArrayList<PurchaseRequestItem> purchaseRequestItems = new ArrayList<PurchaseRequestItem>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM, allColumns,
                DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_PURCHASE_REQUEST + " = ?", new String[]{String.valueOf(purchaseRequestId)}, null, null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            purchaseRequestItems.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return purchaseRequestItems;
    }

    public PurchaseRequestItem get(long id) {
        PurchaseRequestItem purchaseRequestItem = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                purchaseRequestItem = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return purchaseRequestItem;
    }

    public void delete(long id) {
        open();

        database.delete(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM, DatabaseHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private PurchaseRequestItem cursorToObject(Cursor cursor) {
        PurchaseRequestItem purchaseRequestItem = new PurchaseRequestItem();
        purchaseRequestItem.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        purchaseRequestItem.setPurchaseRequest(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_PURCHASE_REQUEST)));
        purchaseRequestItem.setSku(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_SKU)));
        purchaseRequestItem.setAmount(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_AMOUNT)));
        purchaseRequestItem.setCost(Float.parseFloat(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_ITEM_COLUMN_COST))));

        return purchaseRequestItem;
    }

}
