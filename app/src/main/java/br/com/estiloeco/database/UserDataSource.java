package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.User;

/**
 * Created by paulomartins on 30/01/15.
 */
public class UserDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_USER_COLUMN_EMAIL,
            DatabaseHelper.TABLE_USER_COLUMN_NAME,
            DatabaseHelper.TABLE_USER_COLUMN_LAST_NAME,
            DatabaseHelper.TABLE_USER_COLUMN_GENDER,
            DatabaseHelper.TABLE_USER_COLUMN_BIRTHDATE,
            DatabaseHelper.TABLE_USER_COLUMN_DOCUMENT,
            DatabaseHelper.TABLE_USER_COLUMN_AVATAR
    };

    public UserDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(User user) {
        if (get() != null) {
            delete();
        }

        ContentValues values = new ContentValues();

        open();

        values.put(DatabaseHelper.COLUMN_ID, user.getId());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_EMAIL, user.getEmail());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_NAME, user.getName());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_LAST_NAME, user.getLastName());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_GENDER, user.getGender());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_BIRTHDATE, user.getBirthdate());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_DOCUMENT, user.getDocument());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_AVATAR, user.getAvatar());

        database.insert(DatabaseHelper.TABLE_USER, null, values);

        close();
    }

    public void updateAllInfo(User user) {
        ContentValues values = new ContentValues();

        open();

        values.put(DatabaseHelper.TABLE_USER_COLUMN_NAME, user.getName());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_LAST_NAME, user.getLastName());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_GENDER, user.getGender());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_BIRTHDATE, user.getBirthdate());
        values.put(DatabaseHelper.TABLE_USER_COLUMN_DOCUMENT, user.getDocument());

        database.update(DatabaseHelper.TABLE_USER, values, DatabaseHelper.COLUMN_ID +
                " = ?", new String[]{String.valueOf(user.getId())});

        close();
    }

    public User get() {
        User user = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_USER, allColumns, null, null, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                user = new User();
                user.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_EMAIL)));
                user.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_NAME)));
                user.setLastName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_LAST_NAME)));
                user.setGender(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_GENDER)));
                user.setBirthdate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_BIRTHDATE)));
                user.setDocument(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_DOCUMENT)));
                user.setAvatar(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_USER_COLUMN_AVATAR)));
            }
        }

        cursor.close();
        close();

        return user;
    }

    public void update(ContentValues values) {
        User user = get();

        open();

        database.update(DatabaseHelper.TABLE_USER, values, DatabaseHelper.COLUMN_ID +
                " = ?", new String[]{String.valueOf(user.getId())});

        close();
    }

    public void delete() {
        open();

        database.delete(DatabaseHelper.TABLE_USER, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }
}
