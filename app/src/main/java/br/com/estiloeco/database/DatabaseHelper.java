package br.com.estiloeco.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.estiloeco.config.AppConfig;

/**
 * Created by paulomartins on 16/01/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "estiloeco";
    private static final int DATABASE_VERSION = 24;

    // Common columns
    public static final String COLUMN_ID = "_id";

    // User
    public static final String TABLE_USER = "user";
    public static final String TABLE_USER_COLUMN_EMAIL = "email";
    public static final String TABLE_USER_COLUMN_NAME = "name";
    public static final String TABLE_USER_COLUMN_LAST_NAME = "last_name";
    public static final String TABLE_USER_COLUMN_GENDER= "gender";
    public static final String TABLE_USER_COLUMN_BIRTHDATE = "birthdate";
    public static final String TABLE_USER_COLUMN_DOCUMENT = "document";
    public static final String TABLE_USER_COLUMN_AVATAR = "avatar";

    // Addresses
    public static final String TABLE_ADDRESS = "address";
    public static final String TABLE_ADDRESS_COLUMN_NAME = "name";
    public static final String TABLE_ADDRESS_COLUMN_CEP = "cep";
    public static final String TABLE_ADDRESS_COLUMN_STREET = "street";
    public static final String TABLE_ADDRESS_COLUMN_NUMBER = "number";
    public static final String TABLE_ADDRESS_COLUMN_COMPLEMENT = "complement";
    public static final String TABLE_ADDRESS_COLUMN_NEIGHBORHOOD = "neighborhood";
    public static final String TABLE_ADDRESS_COLUMN_CITY = "city";
    public static final String TABLE_ADDRESS_COLUMN_STATE = "state";
    public static final String TABLE_ADDRESS_COLUMN_COUNTRY = "country";
    public static final String TABLE_ADDRESS_COLUMN_OBS = "obs";
    public static final String TABLE_ADDRESS_COLUMN_PRINCIPAL = "principal";

    // Credit cards
    public static final String TABLE_CREDIT_CARD = "credit_card";
    public static final String TABLE_CREDIT_CARD_COLUMN_FLAG = "flag_id";
    public static final String TABLE_CREDIT_CARD_COLUMN_NAME = "name";
    public static final String TABLE_CREDIT_CARD_COLUMN_NUMBER = "number";
    public static final String TABLE_CREDIT_CARD_COLUMN_EXPIRATION_DATE = "expiration_date";

    // Friends
    public static final String TABLE_FRIENDS = "friends";
    public static final String TABLE_FRIENDS_COLUMN_NAME = "name";
    public static final String TABLE_FRIENDS_COLUMN_AVATAR = "avatar";
    public static final String TABLE_FRIENDS_COLUMN_STATUS = "status";

    // Filters
    public static final String TABLE_FILTERS = "filters";
    public static final String TABLE_FILTERS_COLUMN_FILTER_ID = "filter_id";

    // Item Cart (Item of Shopping Cart)
    public static final String TABLE_SHOPPING_CART = "shopping_cart";
    public static final String TABLE_SHOPPING_CART_COLUMN_BRAND = "brand";
    public static final String TABLE_SHOPPING_CART_COLUMN_PRODUCT_ID = "product_id";
    public static final String TABLE_SHOPPING_CART_COLUMN_PRODUCT = "product";
    public static final String TABLE_SHOPPING_CART_COLUMN_DESCRIPTION = "description";
    public static final String TABLE_SHOPPING_CART_COLUMN_SKU = "sku";
    public static final String TABLE_SHOPPING_CART_COLUMN_SIZE = "size";
    public static final String TABLE_SHOPPING_CART_COLUMN_AMOUNT = "amount";
    public static final String TABLE_SHOPPING_CART_COLUMN_IMAGE = "image";
    public static final String TABLE_SHOPPING_CART_COLUMN_COST = "cost";

    // Payment Method
    public static final String TABLE_PAYMENT_METHOD = "payment_method";
    public static final String TABLE_PAYMENT_METHOD_COLUMN_DESCRIPTION = "description";
    public static final String TABLE_PAYMENT_METHOD_COLUMN_TAG = "tag";

    // Credit Card Flags
    public static final String TABLE_CREDIT_CARD_FLAG = "credit_card_flag";
    public static final String TABLE_CREDIT_CARD_FLAG_COLUMN_DESCRIPTION = "description";

    // Shipping types
    public static final String TABLE_SHIPPING_TYPE = "shipping_type";
    public static final String TABLE_SHIPPING_TYPE_COLUMN_DESCRIPTION = "description";
    public static final String TABLE_SHIPPING_TYPE_COLUMN_TAG = "tag";

    // Purchase Requests
    public static final String TABLE_PURCHASE_REQUEST = "purchase_request";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_PAYMENT_METHOD = "payment_method";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_CREDIT_CARD = "credit_card";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_NUMBER = "bank_bill_number";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_EMAIL = "bank_bill_email";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_COST = "cost";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_COST = "shipping_cost";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_TYPE = "shipping_type";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_ADDRESS = "address";
    public static final String TABLE_PURCHASE_REQUEST_COLUMN_DATE = "date";

    // Purchase Request Status
    public static final String TABLE_PURCHASE_REQUEST_STATUS = "purchase_request_status";
    public static final String TABLE_PURCHASE_REQUEST_STATUS_COLUMN_TITLE = "title";
    public static final String TABLE_PURCHASE_REQUEST_STATUS_COLUMN_PURCHASE_REQUEST = "purchase_request";
    public static final String TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DESCRIPTION = "description";
    public static final String TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DATE = "date";

    // Purchase Request Items
    public static final String TABLE_PURCHASE_REQUEST_ITEM = "purchase_request_item";
    public static final String TABLE_PURCHASE_REQUEST_ITEM_COLUMN_PURCHASE_REQUEST = "purchase_request";
    public static final String TABLE_PURCHASE_REQUEST_ITEM_COLUMN_SKU = "sku";
    public static final String TABLE_PURCHASE_REQUEST_ITEM_COLUMN_AMOUNT = "amount";
    public static final String TABLE_PURCHASE_REQUEST_ITEM_COLUMN_COST = "cost";

    // Notifications
    public static final String TABLE_NOTIFICATIONS = "notifications";
    public static final String TABLE_NOTIFICATIONS_COLUMN_TYPE = "type";
    public static final String TABLE_NOTIFICATIONS_COLUMN_USER_ID = "user_id";
    public static final String TABLE_NOTIFICATIONS_COLUMN_USER_NAME = "user_name";
    public static final String TABLE_NOTIFICATIONS_COLUMN_USER_AVATAR = "user_avatar";
    public static final String TABLE_NOTIFICATIONS_COLUMN_MESSAGE = "message";
    public static final String TABLE_NOTIFICATIONS_COLUMN_STATUS = "status";
    public static final String TABLE_NOTIFICATIONS_COLUMN_DATE = "date";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.w(AppConfig.TAG + " - Database", "Creating database " + DATABASE_NAME);

        // User
        String createUser = "CREATE TABLE " + TABLE_USER + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_USER_COLUMN_EMAIL + " varchar(225), " +
                TABLE_USER_COLUMN_NAME + " varchar(125), " +
                TABLE_USER_COLUMN_LAST_NAME + " varchar(125), " +
                TABLE_USER_COLUMN_GENDER + " varchar(10), " +
                TABLE_USER_COLUMN_BIRTHDATE + " varchar(10), " +
                TABLE_USER_COLUMN_DOCUMENT + " varchar(25), " +
                TABLE_USER_COLUMN_AVATAR + " varchar(125) );";

        // Addresses
        String createAddress = "CREATE TABLE " + TABLE_ADDRESS + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_ADDRESS_COLUMN_NAME + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_CEP + " varchar(10), " +
                TABLE_ADDRESS_COLUMN_STREET + " varchar(225), " +
                TABLE_ADDRESS_COLUMN_NUMBER + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_COMPLEMENT + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_NEIGHBORHOOD + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_CITY + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_STATE + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_COUNTRY + " varchar(125), " +
                TABLE_ADDRESS_COLUMN_OBS + " varchar(255), " +
                TABLE_ADDRESS_COLUMN_PRINCIPAL + " integer default 0 );";

        // Credit Cards
        String createCreditCard = "CREATE TABLE " + TABLE_CREDIT_CARD + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_CREDIT_CARD_COLUMN_FLAG + " integer, " +
                TABLE_CREDIT_CARD_COLUMN_NAME + " varchar(125), " +
                TABLE_CREDIT_CARD_COLUMN_NUMBER + " varchar(16), " +
                TABLE_CREDIT_CARD_COLUMN_EXPIRATION_DATE + " varchar(7)); ";

        // Friends
        String createFriends = "CREATE TABLE " + TABLE_FRIENDS + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_FRIENDS_COLUMN_STATUS + " integer default 0, " +
                TABLE_FRIENDS_COLUMN_NAME + " varchar(125), " +
                TABLE_FRIENDS_COLUMN_AVATAR + " varchar(125) );";

        // Filter
        String createFilters = "CREATE TABLE " + TABLE_FILTERS + " (" + COLUMN_ID + " integer primary key autoincrement, " +
                TABLE_FILTERS_COLUMN_FILTER_ID + " integer);";

        // Shopping Cart
        String createShoppingCart = "CREATE TABLE " + TABLE_SHOPPING_CART + " (" + COLUMN_ID + " integer primary key autoincrement, " +
                TABLE_SHOPPING_CART_COLUMN_BRAND + " varchar(125), " +
                TABLE_SHOPPING_CART_COLUMN_PRODUCT_ID + " integer, " +
                TABLE_SHOPPING_CART_COLUMN_PRODUCT + " varchar(125), " +
                TABLE_SHOPPING_CART_COLUMN_DESCRIPTION + " varchar(225), " +
                TABLE_SHOPPING_CART_COLUMN_SKU + " integer, " +
                TABLE_SHOPPING_CART_COLUMN_SIZE + " varchar(10), " +
                TABLE_SHOPPING_CART_COLUMN_AMOUNT + " integer, " +
                TABLE_SHOPPING_CART_COLUMN_IMAGE + " varchar(255), " +
                TABLE_SHOPPING_CART_COLUMN_COST + " varchar(15) );";

        // Payment method
        String createPaymentMethod = "CREATE TABLE " + TABLE_PAYMENT_METHOD + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_PAYMENT_METHOD_COLUMN_DESCRIPTION + " varchar(125), " +
                TABLE_PAYMENT_METHOD_COLUMN_TAG + " varchar(45) ); ";

        // Credit Card Flag
        String createCreditCardFlag = "CREATE TABLE " + TABLE_CREDIT_CARD_FLAG + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_CREDIT_CARD_FLAG_COLUMN_DESCRIPTION + " varchar(125) );";

        // Shipping type
        String createShippingType = "CREATE TABLE " + TABLE_SHIPPING_TYPE + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_SHIPPING_TYPE_COLUMN_DESCRIPTION + " varchar(125), " +
                TABLE_SHIPPING_TYPE_COLUMN_TAG + " varchar(45) );";

        // Purchase Request
        String createPurchaseRequest = "CREATE TABLE " + TABLE_PURCHASE_REQUEST + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_PURCHASE_REQUEST_COLUMN_PAYMENT_METHOD + " integer, " +
                TABLE_PURCHASE_REQUEST_COLUMN_CREDIT_CARD + " integer, " +
                TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_NUMBER + " varchar(255), " +
                TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_EMAIL + " varchar(255), " +
                TABLE_PURCHASE_REQUEST_COLUMN_COST + " varchar(15), " +
                TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_COST + " varchar(15), " +
                TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_TYPE + " integer, " +
                TABLE_PURCHASE_REQUEST_COLUMN_ADDRESS + " integer, " +
                TABLE_PURCHASE_REQUEST_COLUMN_DATE + " varchar(20) );";

        // Purchase Request status
        String createPurchaseRequestStatus = "CREATE TABLE " + TABLE_PURCHASE_REQUEST_STATUS + " (" +
                COLUMN_ID + " integer primary key autoincrement, " +
                TABLE_PURCHASE_REQUEST_STATUS_COLUMN_PURCHASE_REQUEST + " integer, " +
                TABLE_PURCHASE_REQUEST_STATUS_COLUMN_TITLE + " varchar(125), " +
                TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DESCRIPTION + " varchar(255), " +
                TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DATE + " varchar(20) );";

        // Purchase Request item
        String createPurchaseRequestItem = "CREATE TABLE " + TABLE_PURCHASE_REQUEST_ITEM + " (" +
                COLUMN_ID + " integer primary key autoincrement, " +
                TABLE_PURCHASE_REQUEST_ITEM_COLUMN_PURCHASE_REQUEST + " integer, " +
                TABLE_PURCHASE_REQUEST_ITEM_COLUMN_SKU + " integer, " +
                TABLE_PURCHASE_REQUEST_ITEM_COLUMN_AMOUNT + " integer, " +
                TABLE_PURCHASE_REQUEST_ITEM_COLUMN_COST + " varchar(15) )";

        // Notifications
        String createNotifications = "CREATE TABLE " + TABLE_NOTIFICATIONS + " (" +
                COLUMN_ID + " integer primary key, " +
                TABLE_NOTIFICATIONS_COLUMN_TYPE + " varchar(225), " +
                TABLE_NOTIFICATIONS_COLUMN_USER_ID + " integer, " +
                TABLE_NOTIFICATIONS_COLUMN_USER_NAME + " varchar(125), " +
                TABLE_NOTIFICATIONS_COLUMN_USER_AVATAR + " varchar(125), " +
                TABLE_NOTIFICATIONS_COLUMN_MESSAGE + " text, " +
                TABLE_NOTIFICATIONS_COLUMN_STATUS + " integer default 0, " +
                TABLE_NOTIFICATIONS_COLUMN_DATE + " varchar(125) );";

        db.execSQL(createUser);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_USER);

        db.execSQL(createAddress);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_ADDRESS);

        db.execSQL(createCreditCard);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_CREDIT_CARD);

        db.execSQL(createFriends);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_FRIENDS);

        db.execSQL(createFilters);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_FILTERS);

        db.execSQL(createShoppingCart);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_SHOPPING_CART);

        db.execSQL(createPaymentMethod);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_PAYMENT_METHOD);

        db.execSQL(createCreditCardFlag);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_CREDIT_CARD_FLAG);

        db.execSQL(createShippingType);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_SHIPPING_TYPE);

        db.execSQL(createPurchaseRequest);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_PURCHASE_REQUEST);

        db.execSQL(createPurchaseRequestStatus);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_PURCHASE_REQUEST_STATUS);

        db.execSQL(createPurchaseRequestItem);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_PURCHASE_REQUEST_ITEM);

        db.execSQL(createNotifications);
        Log.w(AppConfig.TAG, "Create Table: " + TABLE_NOTIFICATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(AppConfig.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);

        // Drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDIT_CARD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FRIENDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILTERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPPING_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYMENT_METHOD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDIT_CARD_FLAG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIPPING_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PURCHASE_REQUEST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PURCHASE_REQUEST_STATUS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PURCHASE_REQUEST_ITEM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATIONS);

        // Create new tables
        onCreate(db);
    }
}
