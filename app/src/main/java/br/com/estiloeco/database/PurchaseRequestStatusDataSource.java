package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.PurchaseRequestStatus;

/**
 * Created by paulomartins on 15/04/15.
 */
public class PurchaseRequestStatusDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_PURCHASE_REQUEST,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_TITLE,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DESCRIPTION,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DATE
    };

    public PurchaseRequestStatusDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(PurchaseRequestStatus purchaseRequestStatus) {
        if (get(purchaseRequestStatus.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_PURCHASE_REQUEST, purchaseRequestStatus.getPurchaseRequest());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_TITLE, purchaseRequestStatus.getTitle());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DESCRIPTION, purchaseRequestStatus.getDescription());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DATE, purchaseRequestStatus.getDate());

            database.insert(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS, null, values);

            close();
        }
    }

    public ArrayList<PurchaseRequestStatus> getAll(long purchaseRequestId) {
        ArrayList<PurchaseRequestStatus> purchaseRequestStatus = new ArrayList<PurchaseRequestStatus>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS, allColumns,
                DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_PURCHASE_REQUEST + " = ?", new String[]{String.valueOf(purchaseRequestId)}, null, null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            purchaseRequestStatus.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return purchaseRequestStatus;
    }

    public PurchaseRequestStatus get(long id) {
        PurchaseRequestStatus purchaseRequestStatus = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                purchaseRequestStatus = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return purchaseRequestStatus;
    }

    public void delete(long id) {
        open();

        database.delete(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS, DatabaseHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private PurchaseRequestStatus cursorToObject(Cursor cursor) {
        PurchaseRequestStatus purchaseRequestStatus = new PurchaseRequestStatus();
        purchaseRequestStatus.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        purchaseRequestStatus.setPurchaseRequest(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_PURCHASE_REQUEST)));
        purchaseRequestStatus.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_TITLE)));
        purchaseRequestStatus.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DESCRIPTION)));
        purchaseRequestStatus.setDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_STATUS_COLUMN_DATE)));

        return purchaseRequestStatus;
    }

}
