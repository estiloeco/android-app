package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.ShippingType;

/**
 * Created by paulomartins on 14/04/15.
 */
public class ShippingTypeDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_DESCRIPTION,
            DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_TAG
    };

    public ShippingTypeDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(ShippingType shippingType) {
        if (get(shippingType.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.COLUMN_ID, shippingType.getId());
            values.put(DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_DESCRIPTION, shippingType.getDescription());
            values.put(DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_TAG, shippingType.getTag());

            database.insert(DatabaseHelper.TABLE_SHIPPING_TYPE, null, values);

            close();
        }
    }

    public ShippingType get(long id) {
        ShippingType shippingType = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_SHIPPING_TYPE, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                shippingType = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return shippingType;
    }

    public ShippingType getByTag(String tag) {
        ShippingType shippingType = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_SHIPPING_TYPE, allColumns, DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_TAG + " = ?",
                new String[]{tag}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                shippingType = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return shippingType;
    }

    public ArrayList<ShippingType> getAll() {
        ArrayList<ShippingType> shippingTypes = new ArrayList<ShippingType>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_SHIPPING_TYPE, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            shippingTypes.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return shippingTypes;
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_SHIPPING_TYPE, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private ShippingType cursorToObject(Cursor cursor) {
        ShippingType shippingType = new ShippingType();
        shippingType.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        shippingType.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_DESCRIPTION)));
        shippingType.setTag(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_SHIPPING_TYPE_COLUMN_TAG)));

        return shippingType;
    }
}
