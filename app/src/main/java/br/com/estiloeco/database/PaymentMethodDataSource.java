package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.PaymentMethod;

/**
 * Created by paulomartins on 14/04/15.
 */
public class PaymentMethodDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_DESCRIPTION,
            DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_TAG
    };

    public PaymentMethodDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(PaymentMethod paymentMethod) {
        if (get(paymentMethod.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.COLUMN_ID, paymentMethod.getId());
            values.put(DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_DESCRIPTION, paymentMethod.getDescription());
            values.put(DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_TAG, paymentMethod.getTag());

            database.insert(DatabaseHelper.TABLE_PAYMENT_METHOD, null, values);

            close();
        }
    }

    public PaymentMethod get(long id) {
        PaymentMethod paymentMethod = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PAYMENT_METHOD, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                paymentMethod = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return paymentMethod;
    }

    public PaymentMethod getByTag(String tag) {
        PaymentMethod paymentMethod = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PAYMENT_METHOD, allColumns, DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_TAG + " = ?",
                new String[]{tag}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                paymentMethod = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return paymentMethod;
    }

    public ArrayList<PaymentMethod> getAll() {
        ArrayList<PaymentMethod> paymentMethods = new ArrayList<PaymentMethod>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PAYMENT_METHOD, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            paymentMethods.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return paymentMethods;
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_PAYMENT_METHOD, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private PaymentMethod cursorToObject(Cursor cursor) {
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        paymentMethod.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_DESCRIPTION)));
        paymentMethod.setTag(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PAYMENT_METHOD_COLUMN_TAG)));

        return paymentMethod;
    }

}
