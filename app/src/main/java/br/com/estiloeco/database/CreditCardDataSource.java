package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.CreditCard;

/**
 * Created by paulomartins on 10/04/15.
 */
public class CreditCardDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_FLAG,
            DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_NAME,
            DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_NUMBER,
            DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_EXPIRATION_DATE
    };

    public CreditCardDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(CreditCard creditCard) {
        if (get(creditCard.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.COLUMN_ID, creditCard.getId());
            values.put(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_FLAG, creditCard.getFlagId());
            values.put(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_NAME, creditCard.getName());
            values.put(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_NUMBER, creditCard.getNumber());
            values.put(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_EXPIRATION_DATE, creditCard.getExpireDate());

            database.insert(DatabaseHelper.TABLE_CREDIT_CARD, null, values);

            close();
        }
    }

    public ArrayList<CreditCard> getAll() {
        ArrayList<CreditCard> creditCards = new ArrayList<CreditCard>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_CREDIT_CARD, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            creditCards.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return creditCards;
    }

    public CreditCard get(long id) {
        CreditCard creditCard = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_CREDIT_CARD, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                creditCard = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return creditCard;
    }

    public CreditCard getFirst() {
        ArrayList<CreditCard> creditCards = getAll();

        if (creditCards.size() > 0) {
            return creditCards.get(0);
        }
        else {
            return null;
        }
    }

    public void delete(long id) {
        open();

        database.delete(DatabaseHelper.TABLE_CREDIT_CARD, DatabaseHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_CREDIT_CARD, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private CreditCard cursorToObject(Cursor cursor) {
        CreditCard creditCard = new CreditCard();
        creditCard.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        creditCard.setFlagId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_FLAG)));
        creditCard.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_NAME)));
        creditCard.setNumber(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_NUMBER)));
        creditCard.setExpireDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_CREDIT_CARD_COLUMN_EXPIRATION_DATE)));

        return creditCard;
    }

}
