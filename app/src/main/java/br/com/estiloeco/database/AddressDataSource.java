package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.Address;

/**
 * Created by paulomartins on 07/04/15.
 */
public class AddressDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_NAME,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_CEP,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_STREET,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_NUMBER,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_COMPLEMENT,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_NEIGHBORHOOD,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_CITY,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_STATE,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_COUNTRY,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_OBS,
            DatabaseHelper.TABLE_ADDRESS_COLUMN_PRINCIPAL
    };

    public AddressDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(Address address) {
        if (get(address.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.COLUMN_ID, address.getId());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_NAME, address.getName());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_CEP, address.getCep());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_STREET, address.getStreet());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_NUMBER, address.getNumber());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_COMPLEMENT, address.getComplement());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_NEIGHBORHOOD, address.getNeighborhood());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_CITY, address.getCity());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_STATE, address.getState());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_COUNTRY, address.getCountry());
            values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_OBS, address.getObs());

            database.insert(DatabaseHelper.TABLE_ADDRESS, null, values);

            close();

            if (address.isPrincipal()) {
                setAsPrincipal(address);
            }
        }
    }

    public ArrayList<Address> getAll() {
        ArrayList<Address> addresses = new ArrayList<Address>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_ADDRESS, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            addresses.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return addresses;
    }

    public Address get(long id) {
        Address address = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_ADDRESS, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                address = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return address;
    }

    public Address getPrincipal() {
        Address address = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_ADDRESS, allColumns, DatabaseHelper.TABLE_ADDRESS_COLUMN_PRINCIPAL + " = 1", null, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                address = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return address;
    }

    public void update(Address address) {
        ContentValues values = new ContentValues();

        open();

        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_NAME, address.getName());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_CEP, address.getCep());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_STREET, address.getStreet());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_NUMBER, address.getNumber());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_COMPLEMENT, address.getComplement());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_NEIGHBORHOOD, address.getNeighborhood());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_CITY, address.getCity());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_STATE, address.getState());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_COUNTRY, address.getCountry());
        values.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_OBS, address.getObs());

        database.update(DatabaseHelper.TABLE_ADDRESS, values, DatabaseHelper.COLUMN_ID + " > ?",
                new String[]{String.valueOf(address.getId())});

        close();

        if (address.isPrincipal()) {
            setAsPrincipal(address);
        }
        else {
            setAsNonPrincipal(address);
        }
    }

    public void setAsPrincipal(Address address) {
        ContentValues valuesNonPrincipal = new ContentValues();
        valuesNonPrincipal.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_PRINCIPAL, 0);

        ContentValues valuesPrincipal = new ContentValues();
        valuesPrincipal.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_PRINCIPAL, 1);

        open();

        // Set all as non-principal
        database.update(DatabaseHelper.TABLE_ADDRESS, valuesNonPrincipal, DatabaseHelper.COLUMN_ID + " > 0", null);

        // Set as principal
        database.update(DatabaseHelper.TABLE_ADDRESS, valuesPrincipal, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(address.getId())});

        close();
    }

    public void setAsNonPrincipal(Address address) {
        ContentValues valuesNonPrincipal = new ContentValues();
        valuesNonPrincipal.put(DatabaseHelper.TABLE_ADDRESS_COLUMN_PRINCIPAL, 0);

        open();

        // Set as principal
        database.update(DatabaseHelper.TABLE_ADDRESS, valuesNonPrincipal, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(address.getId())});

        close();
    }

    public void delete(long id) {
        open();

        database.delete(DatabaseHelper.TABLE_ADDRESS, DatabaseHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_ADDRESS, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private Address cursorToObject(Cursor cursor) {
        Address address = new Address();
        address.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        address.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_NAME)));
        address.setCep(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_CEP)));
        address.setStreet(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_STREET)));
        address.setNumber(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_NUMBER)));
        address.setComplement(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_COMPLEMENT)));
        address.setNeighborhood(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_NEIGHBORHOOD)));
        address.setCity(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_CITY)));
        address.setState(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_STATE)));
        address.setCountry(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_COUNTRY)));
        address.setObs(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_OBS)));
        address.setPrincipal((cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TABLE_ADDRESS_COLUMN_PRINCIPAL)) == 1 ? true : false));

        return address;
    }
}
