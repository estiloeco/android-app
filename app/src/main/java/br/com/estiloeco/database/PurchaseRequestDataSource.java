package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.PurchaseRequest;
import br.com.estiloeco.model.PurchaseRequestItem;
import br.com.estiloeco.model.PurchaseRequestStatus;

/**
 * Created by paulomartins on 15/04/15.
 */
public class PurchaseRequestDataSource {

    private Context context;
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_PAYMENT_METHOD,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_CREDIT_CARD,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_NUMBER,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_EMAIL,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_COST,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_COST,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_TYPE,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_ADDRESS,
            DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_DATE
    };

    public PurchaseRequestDataSource(Context context) {
        this.context = context;
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(PurchaseRequest purchaseRequest) {
        if (get(purchaseRequest.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.COLUMN_ID, purchaseRequest.getId());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_PAYMENT_METHOD, purchaseRequest.getPaymentMethod());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_CREDIT_CARD, purchaseRequest.getCreditCard());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_NUMBER, purchaseRequest.getBankBillNumber());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_EMAIL, purchaseRequest.getBankBillUserEmail());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_COST, purchaseRequest.getCost());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_COST, purchaseRequest.getShippingCost());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_TYPE, purchaseRequest.getShippingType());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_ADDRESS, purchaseRequest.getAddress());
            values.put(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_DATE, purchaseRequest.getDate());

            database.insert(DatabaseHelper.TABLE_PURCHASE_REQUEST, null, values);

            close();
        }
    }

    public ArrayList<PurchaseRequest> getAll() {
        ArrayList<PurchaseRequest> purchaseRequests = new ArrayList<PurchaseRequest>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PURCHASE_REQUEST, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            purchaseRequests.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return purchaseRequests;
    }

    public PurchaseRequest get(long id) {
        PurchaseRequest purchaseRequest = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_PURCHASE_REQUEST, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                purchaseRequest = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return purchaseRequest;
    }

    public void delete(long id) {
        open();

        database.delete(DatabaseHelper.TABLE_PURCHASE_REQUEST, DatabaseHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});

        close();
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_PURCHASE_REQUEST, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private PurchaseRequest cursorToObject(Cursor cursor) {
        PurchaseRequest purchaseRequest = new PurchaseRequest();
        purchaseRequest.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        purchaseRequest.setPaymentMethod(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_PAYMENT_METHOD)));
        purchaseRequest.setCreditCard(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_CREDIT_CARD)));
        purchaseRequest.setBankBillNumber(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_NUMBER)));
        purchaseRequest.setBankBillUserEmail(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_BANK_BILL_EMAIL)));
        purchaseRequest.setCost(Float.parseFloat(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_COST))));
        purchaseRequest.setShippingCost(Float.parseFloat(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_COST))));
        purchaseRequest.setShippingType(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_SHIPPING_TYPE)));
        purchaseRequest.setAddress(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_ADDRESS)));
        purchaseRequest.setDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_PURCHASE_REQUEST_COLUMN_DATE)));

        purchaseRequest.setItems(PurchaseRequestItem.datasource(context).getAll(purchaseRequest.getId()));
        purchaseRequest.setStatus(PurchaseRequestStatus.datasource(context).getAll(purchaseRequest.getId()));

        return purchaseRequest;
    }

}
