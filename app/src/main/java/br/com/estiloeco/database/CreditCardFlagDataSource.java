package br.com.estiloeco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.CreditCardFlag;

/**
 * Created by paulomartins on 14/04/15.
 */
public class CreditCardFlagDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.TABLE_CREDIT_CARD_FLAG_COLUMN_DESCRIPTION
    };

    public CreditCardFlagDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        if (database == null || (database != null && !database.isOpen())) {
            database = dbHelper.getWritableDatabase();
            Log.w(AppConfig.TAG, "Database open!");
        }
    }

    public void close() {
        if (database != null && database.isOpen()) {
            Log.w(AppConfig.TAG, "Database close!");
            database.close();
        }
    }

    public void add(CreditCardFlag creditCardFlag) {
        if (get(creditCardFlag.getId()) == null) {
            ContentValues values = new ContentValues();

            open();

            values.put(DatabaseHelper.COLUMN_ID, creditCardFlag.getId());
            values.put(DatabaseHelper.TABLE_CREDIT_CARD_FLAG_COLUMN_DESCRIPTION, creditCardFlag.getDescription());

            database.insert(DatabaseHelper.TABLE_CREDIT_CARD_FLAG, null, values);

            close();
        }
    }

    public CreditCardFlag get(long id) {
        CreditCardFlag creditCardFlag = null;

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_CREDIT_CARD_FLAG, allColumns, DatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                creditCardFlag = cursorToObject(cursor);
            }
        }

        cursor.close();
        close();

        return creditCardFlag;
    }

    public ArrayList<CreditCardFlag> getAll() {
        ArrayList<CreditCardFlag> creditCardFlags = new ArrayList<CreditCardFlag>();

        open();

        Cursor cursor = database.query(DatabaseHelper.TABLE_CREDIT_CARD_FLAG, allColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            creditCardFlags.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return creditCardFlags;
    }

    public void deleteAll() {
        open();

        database.delete(DatabaseHelper.TABLE_CREDIT_CARD_FLAG, DatabaseHelper.COLUMN_ID + " > ?", new String[]{"-1"});

        close();
    }

    private CreditCardFlag cursorToObject(Cursor cursor) {
        CreditCardFlag creditCardFlag = new CreditCardFlag();
        creditCardFlag.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
        creditCardFlag.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TABLE_CREDIT_CARD_FLAG_COLUMN_DESCRIPTION)));

        return creditCardFlag;
    }

}
