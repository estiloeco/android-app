package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import br.com.estiloeco.R;
import br.com.estiloeco.database.ShippingTypeDataSource;
import br.com.estiloeco.model.ShippingType;
import br.com.estiloeco.model.User;

public class CheckoutFinishedActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private User user;
    // Data Source
    ShippingTypeDataSource shippingTypeDataSource;
    // Components
    private TextView orderNumber;
    private TextView tvShippingLimit;
    private TextView tvMessage;
    private TextView bankBillNumber;
    private TextView tvBankBillNumberLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeBuyFinished);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_finished);

        // Context
        context = this;

        // Data Source
        shippingTypeDataSource = ShippingType.datasource(context);

        // User
        user = User.logged(context);

        // Prepare Actionbar
        prepareActionBar();

        components();
        componentsData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        orderNumber = (TextView) findViewById(R.id.orderNumber);
        tvShippingLimit = (TextView) findViewById(R.id.tvShippingLimit);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        bankBillNumber = (TextView) findViewById(R.id.bankBillNumber);
        tvBankBillNumberLabel = (TextView) findViewById(R.id.tvBankBillNumberLabel);
    }

    private void componentsData() {
        // Extras
        if (getIntent().hasExtra("purchase_request")) {
            orderNumber.setText(String.valueOf(getIntent().getExtras().getLong("purchase_request")));

            if (getIntent().hasExtra("bank_bill_number") && getIntent().hasExtra("bank_bill_user_email")) {
                tvBankBillNumberLabel.setVisibility(View.VISIBLE);

                tvMessage.setText(getResources().getString(R.string.message_purchase_request_finished_bank_bill)
                        .replace("#email#", getIntent().getExtras().getString("bank_bill_user_email")));

                bankBillNumber.setText(getIntent().getExtras().getString("bank_bill_number"));
            }
            else {
                tvMessage.setText(getResources().getString(R.string.message_purchase_request_finished)
                        .replace("#email#", user.getEmail()));

                tvBankBillNumberLabel.setVisibility(View.INVISIBLE);
            }

            if (getIntent().hasExtra("shipping_type_id")) {
                ShippingType shippingType = shippingTypeDataSource.get(getIntent().getExtras().getLong("shipping_type_id"));
                tvShippingLimit.setText(shippingType.getShortDescription());
            }
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
