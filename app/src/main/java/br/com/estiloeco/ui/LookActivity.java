package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.nirhart.parallaxscroll.views.ParallaxScrollView;
import com.slidinglayer.SlidingLayer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.ProductAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.FriendsDataSource;
import br.com.estiloeco.database.ItemCartDataSource;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.ItemCart;
import br.com.estiloeco.model.Look;
import br.com.estiloeco.model.Product;
import br.com.estiloeco.model.Sku;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;

public class LookActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private RequestQueue request;
    private Look look;
    private User user;
    // Components
    private ParallaxScrollView parallaxScrollView;
    private SlidingLayer slidingLayer;
    private ImageView ivLookImage;
    private ImageButton btnFavorite;
    private ImageButton btnShare;
    private Button btnAddToShoppingCart;
    private ProgressBar progressBarImage;
    private ProgressBar progressBar;
    private TextView tvScrollDown;
    private TextView tvProductUnavailable;
    private ImageView ivScrollDown;
    private ImageView ivFriendOne;
    private ImageButton btnMoreFriends;
    private ListView lvLookProducts;
    private TextView tvTermTyped;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        // View
        setContentView(R.layout.activity_look);

        components();
        componentsActions();

        // Request
        request = Volley.newRequestQueue(context);

        // User
        UserDataSource userDataSource = User.datasource(context);
        user = userDataSource.get();

        // Currrent look
        look = new Look();

        // Extras
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("lookId")) {
                look.setId(getIntent().getExtras().getLong("lookId"));

                ArrayList<String> params = new ArrayList<String>();
                params.add(String.valueOf(look.getId()));

                downloadLook();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.look, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(context, SettingsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_shopping_cart) {
            if (slidingLayer.isOpened()) {
                slidingLayer.closeLayer(true);
            }
            else {
                slidingLayer.openLayer(true);
            }
        }
        else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        parallaxScrollView = (ParallaxScrollView) findViewById(R.id.parallaxScrollView);
        slidingLayer = (SlidingLayer) findViewById(R.id.slidingLayer1);
        ivLookImage = (ImageView) findViewById(R.id.ivLookImage);
        btnFavorite = (ImageButton) findViewById(R.id.btnFavorite);
        btnShare = (ImageButton) findViewById(R.id.btnShare);
        btnAddToShoppingCart = (Button) findViewById(R.id.btnAddToShoppingCart);
        progressBarImage = (ProgressBar) findViewById(R.id.progressBarImage);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvScrollDown = (TextView) findViewById(R.id.tvScrollDown);
        tvProductUnavailable = (TextView) findViewById(R.id.tvProductUnavailable);
        ivScrollDown = (ImageView) findViewById(R.id.ivScrollDown);
        ivFriendOne = (ImageView) findViewById(R.id.ivFriendOne);
        btnMoreFriends = (ImageButton) findViewById(R.id.btnMoreFriends);
        lvLookProducts = (ListView) findViewById(R.id.lvLookProducts);
        tvTermTyped = (TextView) findViewById(R.id.tvTermTyped);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ivLookImage.getLayoutParams().height = metrics.heightPixels-(int)(75*metrics.density);
    }

    private void componentsActions() {
        // Favorite
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user != null) {
                    like();
                }
                else {
                    final AlertDialog alertDialog = AppUtil.confirmDialog(context,
                            getResources().getString(R.string.message_you_need_register_ou_login), R.string.btn_close, R.string.btn_login_register);

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                            Intent intent = new Intent(context, LoginActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                final View dialogView = inflater.inflate(R.layout.dialog_share_look, null);
                final AlertDialog dialog = new AlertDialog.Builder(context).create();
                dialog.setView(dialogView);

                // Components
                Button btnSend = (Button) dialogView.findViewById(R.id.btnSend);
                Button btnCancel = (Button) dialogView.findViewById(R.id.btnCancel);
                final AutoCompleteTextView tvFriend = (AutoCompleteTextView) dialogView.findViewById(R.id.tvFriend);
                final TextView tvErrorMessage = (TextView) dialogView.findViewById(R.id.tvErrorMessage);

                final FriendsDataSource friendsDataSource = Friend.datasource(context);

                String[] friends = friendsDataSource.getAllNames().toArray(new String[0]);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, friends);
                tvFriend.setAdapter(adapter);

                // Actions
                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Friend friend = friendsDataSource.getByName(tvFriend.getText().toString());

                        if (friend != null) {
                            tvErrorMessage.setVisibility(View.GONE);
                            share(friend.getId());
                            dialog.dismiss();
                        }
                        else {
                            tvErrorMessage.setVisibility(View.VISIBLE);
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    private void setBtnFavoriteState() {
        if (look.userLike()) {
            btnFavorite.setBackgroundResource(R.drawable.ic_favorited);
        }
        else {
            btnFavorite.setBackgroundResource(R.drawable.ic_favorite);
        }
    }

    private void populateLook() {
        if (look.getId() != 0) {
            btnFavorite.setVisibility(View.VISIBLE);
            btnShare.setVisibility(View.VISIBLE);
            // btnMoreFriends.setVisibility(View.VISIBLE);

            ArrayList<Product> products = look.getProducts();

            Picasso.with(context).load(look.getImage(0)).into(ivLookImage);
            parallaxScrollView.fullScroll(View.FOCUS_UP);

            if (look.getFriends().size() > 0 && Helper.validString(look.getFriends().get(0).getAvatar())) {
                ivFriendOne.setVisibility(View.VISIBLE);

                Picasso.with(context).load(AppConfig.ASSETS_URL + look.getFriends().get(0).getAvatar()).skipMemoryCache()
                        .transform(new Helper().new CircleTransform()).into(ivFriendOne);
            }
            else {
                ivFriendOne.setVisibility(View.INVISIBLE);
            }

            if (products.size() > 0) {
                ProductAdapter adapter = new ProductAdapter(context, products);
                lvLookProducts.setAdapter(adapter);

                tvScrollDown.setVisibility(View.VISIBLE);
                ivScrollDown.setVisibility(View.VISIBLE);
                lvLookProducts.setVisibility(View.VISIBLE);
            }
            else {
                tvProductUnavailable.setVisibility(View.VISIBLE);
            }
        }

        setBtnFavoriteState();
    }

    private void prepareActionBar() {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_overlay));
    }

    private void downloadLook() {
        String url = AppConfig.webserviceUrl("look");

        // Params
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(look.getId()));

        if (user != null) {
            params.put("userId", String.valueOf(user.getId()));
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.isNull("error")) {
                                Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + response.get("error").toString());
                            }
                            else {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray arrayImages = new JSONArray(data.get("images").toString());
                                JSONArray arrayProducts = new JSONArray(data.get("products").toString());
                                JSONArray arrayFriends = new JSONArray(data.get("friends").toString());

                                ArrayList<String> images = new ArrayList<String>();
                                ArrayList<Product> products = new ArrayList<Product>();
                                ArrayList<Friend> friends = new ArrayList<Friend>();

                                // Images
                                for (int i = 0; i < arrayImages.length(); i++) {
                                    JSONObject image = new JSONObject(arrayImages.getString(i));
                                    images.add(image.getString("file"));
                                }

                                // Products
                                for (int i = 0; i < arrayProducts.length(); i++) {
                                    JSONObject productObject = new JSONObject(arrayProducts.getString(i));
                                    JSONObject productImages = new JSONObject(productObject.get("images").toString());
                                    JSONArray arraySkus = new JSONArray(productObject.get("skus").toString());

                                    Product product = new Product();
                                    product.setId(productObject.getInt("id"));
                                    product.setName(productObject.getString("name"));
                                    product.setDescription(productObject.getString("description"));
                                    product.setBrand(productObject.getString("brand"));
                                    product.setImage(productImages.getString("list"));
                                    product.setImageShoppingCart(productImages.getString("shopping_cart"));
                                    product.setAvailable(productObject.getBoolean("available"));
                                    product.setCost((float)productObject.getDouble("cost"));

                                    // Skus
                                    ArrayList<Sku> skus = new ArrayList<Sku>();

                                    for (int j = 0; j < arraySkus.length(); j++) {
                                        JSONObject skuObject = new JSONObject(arraySkus.getString(j));

                                        Sku sku = new Sku();
                                        sku.setId(skuObject.getInt("id"));
                                        sku.setSize(skuObject.getString("size"));
                                        sku.setColor(skuObject.getString("color"));

                                        skus.add(sku);
                                    }

                                    product.setSkus(skus);

                                    products.add(product);
                                }

                                // Friends
                                for (int i = 0; i < arrayFriends.length(); i++) {
                                    JSONObject friendObject = new JSONObject(arrayFriends.getString(i));

                                    Friend friend = new Friend();
                                    friend.setName(friendObject.getString("name"));
                                    friend.setAvatar(friendObject.getString("avatar"));

                                    friends.add(friend);
                                }

                                look.setLikes(data.getInt("likes"));
                                look.setUserLike(data.getBoolean("like"));
                                look.setImages(images);
                                look.setProducts(products);
                                look.setFriends(friends);

                                populateLook();
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void like() {
        String url = AppConfig.webserviceUrl("profile/look");
        Map<String, String> params = new HashMap<String, String>();

        // Params
        params.put("id", String.valueOf(user.getId()));
        params.put("lookId", String.valueOf(look.getId()));

        if (!look.userLike()) {
            params.put("action", "like");
        }
        else {
            params.put("action", "unlike");
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + response.get("message").toString());
                            }
                            else {
                                if (!look.userLike()) {
                                    look.setUserLike(true);
                                }
                                else {
                                    look.setUserLike(false);
                                }

                                setBtnFavoriteState();
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void share(long friendId) {
        String url = AppConfig.webserviceUrl("look/share");
        Map<String, String> params = new HashMap<String, String>();

        // Params
        params.put("id", String.valueOf(look.getId()));
        params.put("userId", String.valueOf(user.getId()));
        params.put("friendId", String.valueOf(friendId));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");

                            if (!status) {
                                Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + response.get("message").toString());
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, LookActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        btnFavorite.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBarImage.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        btnFavorite.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        progressBarImage.setVisibility(View.GONE);
    }
}
