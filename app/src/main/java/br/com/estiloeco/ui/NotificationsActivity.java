package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.NotificationAdapter;
import br.com.estiloeco.database.NotificationsDataSource;
import br.com.estiloeco.model.Notification;

public class NotificationsActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    // Components
    private ListView lvNotifications;
    private TextView tvNoNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_notifications);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
        componentsData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        lvNotifications = (ListView) findViewById(R.id.lvNotifications);
        tvNoNotifications = (TextView) findViewById(R.id.tvNoNotifications);
    }

    private void componentsData() {
        NotificationsDataSource notificationsDataSource = Notification.datasource(context);
        List<Notification> notifications = notificationsDataSource.getAll();

        if (notifications.size() == 0) {
            tvNoNotifications.setVisibility(View.VISIBLE);
        }
        else {
            lvNotifications.setVisibility(View.VISIBLE);
        }

        NotificationAdapter adapter = new NotificationAdapter(context, notifications, lvNotifications, tvNoNotifications, NotificationsActivity.this);
        lvNotifications.setAdapter(adapter);
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
