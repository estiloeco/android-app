package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.helpers.NumberFormats;

public class ProductActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private RequestQueue request;
    // Components
    private ImageView ivProductImage;
    private TextView tvProductName;
    private TextView tvProductBrand;
    private TextView tvProductCost;
    private TextView tvProductDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_product);

        // Context
        context = this;

        // Request
        request = Volley.newRequestQueue(context);

        // Prepare Actionbar
        prepareActionBar();

        // Calls
        components();

        if (getIntent().hasExtra("productId")) {
            getProduct(getIntent().getExtras().getLong("productId"), getIntent().getExtras().getInt("skuId"));
        }
        else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        ivProductImage = (ImageView) findViewById(R.id.ivProductImage);
        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvProductBrand = (TextView) findViewById(R.id.tvProductBrand);
        tvProductCost = (TextView) findViewById(R.id.tvProductCost);
        tvProductDescription = (TextView) findViewById(R.id.tvProductDescription);
    }

    private void populate(JSONObject response) {
        try {
            if (!response.getBoolean("status")) {
                Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + response.getString("message"));
            }
            else {
                JSONObject object = response.getJSONObject("data");

                if (Helper.validString(object.getString("image"))) {
                    Picasso.with(context).load(object.getString("image")).skipMemoryCache()
                            .into(ivProductImage);
                }

                tvProductName.setText(object.getString("name"));
                tvProductBrand.setText(object.getString("brand"));
                tvProductDescription.setText(object.getString("description"));
                tvProductCost.setText(getResources().getString(R.string.label_real) + " " +
                        NumberFormats.toMoney((float)object.getDouble("cost")));
            }
        } catch (JSONException e) {
            Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + e.getMessage());
        }
    }

    private void getProduct(long id, long skuId) {
        String url = AppConfig.webserviceUrl("product");
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(id));
        params.put("skuId", String.valueOf(skuId));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopLoading();
                        populate(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, ProductActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
