package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.etsy.android.grid.StaggeredGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.GridAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.PersonalFiltersDataSource;
import br.com.estiloeco.model.Filter;
import br.com.estiloeco.model.Look;
import br.com.estiloeco.model.User;

public class SearchActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private ActionBar actionBar;
    private RequestQueue request;
    private String term;
    private GridAdapter gridAdapter;
    private User user;
    // Components
    private StaggeredGridView gridView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvTermTyped;
    private TextView tvMessageNoLookFound;
    // Database
    private PersonalFiltersDataSource personalFiltersDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Context
        context = this;

        // User
        user = User.logged(context);

        // Calls
        prepareActionBar();
        components();
        componentsActions();

        // Database
        personalFiltersDataSource = Filter.datasource(context);

        // Extras
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("term")) {
                term = getIntent().getExtras().getString("term");
                getActionBar().setTitle (getResources().getString(R.string.title_activity_search_results) +
                        " " + term);
            }
        }

        // Request
        request = Volley.newRequestQueue(context);

        // Looks
        gridAdapter = new GridAdapter(context);
        downloadLooks();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        tvTermTyped = (TextView) findViewById(R.id.tvTermTyped);
        tvMessageNoLookFound = (TextView) findViewById(R.id.tvMessageNoLookFound);

        // Pull to refresh
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.principal_dark, R.color.principal_on,
                R.color.principal, R.color.principal_xlight);

        // GridView
        gridView = (StaggeredGridView) findViewById(R.id.grid_view);

        gridView.setOnScrollListener(new StaggeredGridView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                if (i == 0 && i2 > 0 && gridView.getChildAt(0).getTop() >= 0) {
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });
    }

    private void componentsActions() {
        // Gridview
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(context, LookActivity.class);
                intent.putExtra("lookId", gridAdapter.getItem(i).getId());
                startActivity(intent);
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void populateLooksList(JSONObject data) {
        List<Look> list = new ArrayList<Look>();

        try {
            if (!data.isNull("error")) {
                Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + data.get("error").toString());
            }
            else {
                JSONArray array = new JSONArray(data.get("data").toString());

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = new JSONObject(array.getString(i));

                    ArrayList<String> images = new ArrayList<String>();
                    images.add(object.getString("image"));

                    Look look = new Look();
                    look.setId(object.getInt("id"));
                    look.setLikes(object.getInt("likes"));
                    look.setImages(images);

                    list.add(look);
                }

                if (list.size() == 0) {
                    tvTermTyped.setText(term);

                    tvMessageNoLookFound.setVisibility(View.VISIBLE);
                    tvTermTyped.setVisibility(View.VISIBLE);
                }

                if (gridAdapter.getCount() == 0) {
                    gridView.setAdapter(gridAdapter);
                }

                gridAdapter.refresh(list);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void downloadLooks() {
        String url = AppConfig.webserviceUrl("looks");
        Map<String, String> params = new HashMap<String, String>();

        // Terms
        if (!term.equals(null)) {
            params.put("search", term);
        }

        // User
        if (user != null) {
            params.put("userId", String.valueOf(user.getId()));
        }

        // Personal Filters
        List<Integer> personalFilters = personalFiltersDataSource.getAll();
        String filters = "";

        if (personalFilters.size() > 0) {
            for (int i = 0; i < personalFilters.size(); i++) {
                filters += String.valueOf(personalFilters.get(i)) + (i < (personalFilters.size()-1) ? "-" : "");
            }

            params.put("filters", filters);
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopLoading();
                        populateLooksList(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, SearchActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    private void stopLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

}
