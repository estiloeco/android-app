package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.database.AddressDataSource;
import br.com.estiloeco.database.ShippingTypeDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.ShippingType;


public class CheckoutShippingActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private long addressId;
    // Data Source
    private AddressDataSource addressDataSource;
    private ShippingTypeDataSource shippingTypeDataSource;
    // Components
    private TextView tvChangeAddress;
    private TextView tvAddressName;
    private TextView tvAddressStreetNumberObs;
    private TextView tvAddressComplement;
    private TextView etAddressCepCityState;
    private TextView tvSelectAnAddress;
    private RadioGroup rdTypeShipping;
    private Button btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_shipping);

        // Context
        context = this;

        // Datasource
        addressDataSource = Address.datasource(context);
        shippingTypeDataSource = ShippingType.datasource(context);

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
        componentsData();
        componentsActions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        tvChangeAddress = (TextView) findViewById(R.id.tvChangeAddress);
        tvAddressName = (TextView) findViewById(R.id.tvAddressName);
        tvAddressStreetNumberObs = (TextView) findViewById(R.id.tvAddressStreetNumberObs);
        tvAddressComplement = (TextView) findViewById(R.id.tvAddressComplement);
        tvSelectAnAddress = (TextView) findViewById(R.id.tvSelectAnAddress);
        etAddressCepCityState = (TextView) findViewById(R.id.etAddressCepCityState);
        rdTypeShipping = (RadioGroup) findViewById(R.id.rdTypeShipping);
        btnContinue = (Button) findViewById(R.id.btnContinue);
    }

    private void componentsData() {
        ArrayList<ShippingType> shippingTypes = shippingTypeDataSource.getAll();

        if (shippingTypes.size() > 0) {
            for (int i = 0; i < shippingTypes.size(); i++) {
                RadioButton rd = new RadioButton(context);

                rdTypeShipping.addView(rd);

                rd.setText(shippingTypes.get(i).getDescription());
                rd.setTag(shippingTypes.get(i).getTag());
                rd.setTextColor(getResources().getColor(R.color.gray_text));
            }
        }

        setCurrentAddress();
    }

    private void setCurrentAddress() {
        Address address = null;

        if (addressId == 0) {
            address = addressDataSource.getPrincipal();
        }
        else {
            address = addressDataSource.get(addressId);
        }

        if (address != null) {
            addressId = address.getId();

            tvChangeAddress.setText(R.string.btn_change);
            tvSelectAnAddress.setVisibility(View.GONE);

            tvAddressName.setText(address.getName());
            tvAddressStreetNumberObs.setText(address.getStreet() + ", " + address.getNumber());
            etAddressCepCityState.setText(address.getCity() + " / " + address.getState());
            tvAddressComplement.setText((Helper.validString(address.getComplement()) ?
                    address.getComplement() + " - " : "") + address.getCep());
        }
        else {
            tvChangeAddress.setText(R.string.btn_select);
            tvSelectAnAddress.setVisibility(View.VISIBLE);
        }
    }

    private void componentsActions() {
        tvChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CheckoutChangeAddressActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioSelected = rdTypeShipping.getCheckedRadioButtonId();

                if (addressId == 0) {
                    Toast.makeText(context, R.string.message_select_an_address, Toast.LENGTH_SHORT).show();
                }
                else {
                    if (radioSelected > -1) {
                        RadioButton rdTypeShippingSelected = (RadioButton) findViewById(radioSelected);

                        Intent intent = new Intent(context, CheckoutPaymentActivity.class);
                        intent.putExtra("addressId", addressId);
                        intent.putExtra("shippingTypeId", shippingTypeDataSource.getByTag(rdTypeShippingSelected.getTag().toString()).getId());
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, R.string.message_select_type_shipping, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            addressId = data.getLongExtra("addressId", 0);
            setCurrentAddress();
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
