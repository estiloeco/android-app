package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.FilterAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.PersonalFiltersDataSource;
import br.com.estiloeco.model.Filter;

public class FilterActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private RequestQueue request;
    private FilterAdapter filterAdapter;
    // Components
    private ListView listFilters;
    private TextView etSearchTerm;
    // Data
    PersonalFiltersDataSource personalFiltersDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_filter);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        components();
        componentsAction();

        // Personal Filters
        personalFiltersDataSource = Filter.datasource(context);

        // Request
        request = Volley.newRequestQueue(context);

        // Filters
        filterAdapter = new FilterAdapter(context, personalFiltersDataSource);
        downloadFilters();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        listFilters = (ListView) findViewById(R.id.listFilter);
        etSearchTerm = (TextView) findViewById(R.id.etSearchTerm);
    }

    private void componentsAction() {
        etSearchTerm.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra("term", etSearchTerm.getText().toString());
                    startActivity(intent);

                    return true;
                }

                return false;
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void downloadFilters() {
        String url = AppConfig.webserviceUrl("filters");
        Map<String, String> params = new HashMap<String, String>();

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject data = response;
                        ArrayList<Filter> filters = new ArrayList<Filter>();

                        try {
                            if (!data.isNull("error")) {
                                Log.e(AppConfig.TAG, FilterActivity.this.getLocalClassName().toString() + " - Filters: " + data.get("error").toString());
                            }
                            else {
                                JSONArray array = new JSONArray(data.get("data").toString());

                                for (int i = 0; i < array.length(); i++) {
                                    List<Integer> personalFilters = personalFiltersDataSource.getAll();
                                    JSONObject object = new JSONObject(array.getString(i));

                                    Filter filter = new Filter();
                                    filter.setId(object.getInt("id"));
                                    filter.setName(object.getString("name"));
                                    filter.setChecked(personalFilters.contains(object.getInt("id")));

                                    filters.add(filter);
                                }

                                if (filterAdapter.getCount() == 0) {
                                    listFilters.setAdapter(filterAdapter);
                                }

                                filterAdapter.refresh(filters);
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, FilterActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, FilterActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }
}
