package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.CreditCardAdapter;
import br.com.estiloeco.database.CreditCardDataSource;
import br.com.estiloeco.model.CreditCard;

public class SettingsUserCreditCardActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private CreditCardDataSource creditCardDataSource;
    private CreditCardAdapter creditCardAdapter;
    // Components
    private ListView lvCreditCards;
    private TextView tvNoCreditCards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_user_credit_card);

        // Context
        this.context = this;

        // Datasource
        creditCardDataSource = CreditCard.datasource(context);

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
    }

    @Override
    protected void onResume() {
        super.onResume();
        componentsData();
        componentsActions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_user_credit_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        else if (id == R.id.action_add_credit_card) {
            Intent intent = new Intent(context, FormUserCreditCardActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        lvCreditCards = (ListView) findViewById(R.id.lvCreditCards);
        tvNoCreditCards = (TextView) findViewById(R.id.tvNoCreditCards);
    }

    private void componentsData() {
        ArrayList<CreditCard> list = creditCardDataSource.getAll();

        if (list.size() > 0) {
            lvCreditCards.setVisibility(View.VISIBLE);
            tvNoCreditCards.setVisibility(View.INVISIBLE);
        }
        else {
            lvCreditCards.setVisibility(View.GONE);
            tvNoCreditCards.setVisibility(View.VISIBLE);
        }

        creditCardAdapter = new CreditCardAdapter(context, list);
        lvCreditCards.setAdapter(creditCardAdapter);
    }

    private void componentsActions() {
        lvCreditCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(context, FormUserCreditCardActivity.class);
                intent.putExtra("creditCardId", creditCardAdapter.getItem(i).getId());
                startActivity(intent);
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
