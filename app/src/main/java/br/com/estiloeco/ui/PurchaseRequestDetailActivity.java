package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.fragment.PurchaseRequestDetailFragment;
import br.com.estiloeco.fragment.PurchaseRequestStatusFragment;

public class PurchaseRequestDetailActivity extends FragmentActivity {

    private Context context;
    private ActionBar actionBar;
    private Bundle savedInstanceState;
    private TabsAdapter tabsAdapter;
    private ViewPager viewPager;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_request_detail);

        // Context
        context = this;

        // Bundle
        this.savedInstanceState = savedInstanceState;

        if (getIntent().hasExtra("id")) {
            id = getIntent().getExtras().getLong("id");
        }
        else {
            finish();
        }

        // View Pager
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.pager);
        setContentView(viewPager);

        // Prepare Actionbar
        prepareActionBar();

        // Tabs
        tabsAdapter = new TabsAdapter(this, viewPager);
        tabs();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    protected void tabs() {
        tabsAdapter.addTab(
                actionBar.newTab().setText(getResources().getString(R.string.tab_order_detail)),
                PurchaseRequestDetailFragment.class, null);

        tabsAdapter.addTab(
                actionBar.newTab().setText(getResources().getString(R.string.tab_order_status)),
                PurchaseRequestStatusFragment.class, null);

        if (savedInstanceState != null) {
            actionBar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
        }
    }

    public static class TabsAdapter extends FragmentPagerAdapter
            implements ActionBar.TabListener, ViewPager.OnPageChangeListener {

        protected final Context context;
        protected final ActionBar actionBar;
        protected final ViewPager viewPager;
        protected final ArrayList<TabInfo> tabs = new ArrayList<TabInfo>();

        static final class TabInfo {
            protected final Class<?> clss;
            protected final Bundle args;

            TabInfo(Class<?> clss, Bundle args) {
                this.clss = clss;
                this.args = args;
            }
        }

        public TabsAdapter(FragmentActivity activity, ViewPager pager) {
            super(activity.getSupportFragmentManager());
            context = activity;
            actionBar = activity.getActionBar();
            viewPager = pager;
            viewPager.setAdapter(this);
            viewPager.setOnPageChangeListener(this);
        }

        public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
            TabInfo info = new TabInfo(clss, args);
            tab.setTag(info);
            tab.setTabListener(this);
            tabs.add(info);
            actionBar.addTab(tab);
            notifyDataSetChanged();
        }

        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            Object tag = tab.getTag();

            for (int i = 0; i<tabs.size(); i++) {
                if (tabs.get(i) == tag) {
                    viewPager.setCurrentItem(i);
                }
            }
        }

        @Override
        public Fragment getItem(int i) {
            TabInfo info = tabs.get(i);
            return Fragment.instantiate(context, info.clss.getName(), info.args);
        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}

        @Override
        public void onPageScrollStateChanged(int i) {}

        @Override
        public void onPageScrolled(int i, float v, int i2) {}

        @Override
        public void onPageSelected(int i) { actionBar.setSelectedNavigationItem(i); }

        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}

        @Override
        public int getCount() { return tabs.size(); }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setTitle(getResources().getString(R.string.title_activity_order_detail) + id);
    }
}
