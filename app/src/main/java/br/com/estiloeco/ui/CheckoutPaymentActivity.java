package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.CreditCardDataSource;
import br.com.estiloeco.database.CreditCardFlagDataSource;
import br.com.estiloeco.database.ItemCartDataSource;
import br.com.estiloeco.database.PaymentMethodDataSource;
import br.com.estiloeco.database.PurchaseRequestDataSource;
import br.com.estiloeco.database.PurchaseRequestItemDataSource;
import br.com.estiloeco.database.PurchaseRequestStatusDataSource;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.CreditCard;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.ItemCart;
import br.com.estiloeco.model.PaymentMethod;
import br.com.estiloeco.model.PurchaseRequest;
import br.com.estiloeco.model.PurchaseRequestItem;
import br.com.estiloeco.model.PurchaseRequestStatus;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;

public class CheckoutPaymentActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private long creditCardId;
    private PaymentMethod paymentMethod;
    private User user;
    private PurchaseRequest purchaseRequest;
    private RequestQueue request;
    private ProgressDialog progressDialog;
    // Data Source
    private PaymentMethodDataSource paymentMethodDataSource;
    private CreditCardDataSource creditCardDataSource;
    private CreditCardFlagDataSource creditCardFlagDataSource;
    private ItemCartDataSource itemCartDataSource;
    // Components
    private RadioGroup rdPaymentMethod;
    private LinearLayout supplementaryInformation;
    private RelativeLayout supplementaryInformationCreditCard;
    private EditText etUserEmail;
    private EditText etUserDocument;
    private Button btnContinue;
    private TextView tvChangeCreditCard;
    private TextView tvCreditCardFlag;
    private TextView tvCreditCardFinalNumber;
    private TextView tvTrace;
    private TextView tvCreditCardFinalLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_checkout_payment);

        // Context
        context = this;

        // Request
        request = Volley.newRequestQueue(context);

        // Logged user
        user = User.logged(context);

        // Datasource
        paymentMethodDataSource = PaymentMethod.datasource(context);
        creditCardDataSource = CreditCard.datasource(context);
        creditCardFlagDataSource = CreditCardFlag.datasource(context);
        itemCartDataSource = ItemCart.datasource(context);

        // Prepare Actionbar
        prepareActionBar();

        // Extras
        if (getIntent().hasExtra("addressId") && getIntent().hasExtra("shippingTypeId")) {
            long addressId = getIntent().getLongExtra("addressId", 0);
            long shippingTypeId = getIntent().getLongExtra("shippingTypeId", 0);

            if (addressId == 0 || shippingTypeId == 0) {
                finish();
            }

            // Start purchase request object
            purchaseRequest = new PurchaseRequest();

            purchaseRequest.setAddress(addressId);
            purchaseRequest.setShippingType(shippingTypeId);
        }

        // Components
        components();
        componentsData();
        componentsActions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        rdPaymentMethod = (RadioGroup) findViewById(R.id.rdPaymentMethod);
        supplementaryInformation = (LinearLayout) findViewById(R.id.supplementaryInformation);
        supplementaryInformationCreditCard = (RelativeLayout) findViewById(R.id.supplementaryInformationCreditCard);
        etUserEmail = (EditText) findViewById(R.id.etUserEmail);
        etUserDocument = (EditText) findViewById(R.id.etUserDocument);
        btnContinue = (Button) findViewById(R.id.btnContinue);
        tvChangeCreditCard = (TextView) findViewById(R.id.tvChangeCreditCard);
        tvCreditCardFlag = (TextView) findViewById(R.id.tvCreditCardFlag);
        tvCreditCardFinalNumber = (TextView) findViewById(R.id.tvCreditCardFinalNumber);
        tvTrace = (TextView) findViewById(R.id.tvTrace);
        tvCreditCardFinalLabel = (TextView) findViewById(R.id.tvCreditCardFinalLabel);
    }

    private void componentsData() {
        ArrayList<PaymentMethod> paymentMethods = paymentMethodDataSource.getAll();

        if (paymentMethods.size() > 0) {
            for (int i = 0; i < paymentMethods.size(); i++) {
                RadioButton rd = new RadioButton(context);

                rdPaymentMethod.addView(rd);

                rd.setText(paymentMethods.get(i).getDescription());
                rd.setTag(paymentMethods.get(i).getTag());
                rd.setTextColor(getResources().getColor(R.color.gray_text));
            }
        }

        setCurrentCreditCard();
    }

    private void setCurrentCreditCard() {
        CreditCard creditCard = null;

        if (creditCardId == 0) {
            creditCard = creditCardDataSource.getFirst();
        }
        else {
            creditCard = creditCardDataSource.get(creditCardId);
        }

        if (creditCard != null) {
            creditCardId = creditCard.getId();

            tvChangeCreditCard.setText(R.string.btn_change);

            tvTrace.setVisibility(View.VISIBLE);
            tvCreditCardFinalLabel.setVisibility(View.VISIBLE);
            tvCreditCardFlag.setVisibility(View.VISIBLE);
            tvCreditCardFinalNumber.setVisibility(View.VISIBLE);

            tvCreditCardFlag.setText(creditCardFlagDataSource.get(creditCard.getFlagId()).getDescription());
            tvCreditCardFinalNumber.setText(Helper.finalString(creditCard.getNumber(), 4));
        }
        else {
            tvChangeCreditCard.setText(R.string.btn_select_credit_card);

            tvTrace.setVisibility(View.GONE);
            tvCreditCardFinalLabel.setVisibility(View.GONE);
            tvCreditCardFlag.setVisibility(View.GONE);
            tvCreditCardFinalNumber.setVisibility(View.GONE);
        }

        if (!Helper.validString(user.getDocument())) {
            etUserDocument.setVisibility(View.VISIBLE);
        }
    }

    private void componentsActions() {
        // Change payment method
        rdPaymentMethod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selected = rdPaymentMethod.getCheckedRadioButtonId();

                RadioButton rdSelected = (RadioButton) findViewById(selected);

                supplementaryInformation.setVisibility(View.VISIBLE);
                btnContinue.setVisibility(View.VISIBLE);

                if (rdSelected.getTag().equals("credit_card")) {
                    supplementaryInformationCreditCard.setVisibility(View.VISIBLE);
                    etUserEmail.setVisibility(View.GONE);
                }
                else {
                    etUserEmail.setVisibility(View.VISIBLE);
                    etUserEmail.setText(user.getEmail());
                    supplementaryInformationCreditCard.setVisibility(View.GONE);
                }

                paymentMethod = paymentMethodDataSource.getByTag(rdSelected.getTag().toString());
            }
        });


        // Change credit card
        tvChangeCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CheckoutChangeCreditCardActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        // Continue payment
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Helper.isEmpty(etUserDocument) && !Helper.validString(user.getDocument())) {
                    Toast.makeText(context, R.string.message_type_you_document, Toast.LENGTH_SHORT).show();
                }
                else if (paymentMethod.getTag().equals("credit_card")) {
                    if (creditCardId == 0) {
                        Toast.makeText(context, R.string.message_select_an_credit_card, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        LayoutInflater inflater = LayoutInflater.from(context);
                        final View dialogView = inflater.inflate(R.layout.dialog_credit_card_code, null);
                        final AlertDialog dialog = new AlertDialog.Builder(context).create();
                        dialog.setView(dialogView);

                        // Components
                        final EditText etCreditCardCode = (EditText) dialogView.findViewById(R.id.etCreditCardCode);
                        Button btnOk = (Button) dialogView.findViewById(R.id.btnOk);
                        Button btnCancel = (Button) dialogView.findViewById(R.id.btnCancel);

                        // Actions
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (etCreditCardCode.getText().length() > 1) {
                                    dialog.dismiss();

                                    purchaseRequest.setPaymentMethod(paymentMethod.getId());
                                    purchaseRequest.setCreditCardCode(Integer.parseInt(etCreditCardCode.getText().toString()));
                                    purchaseRequest.setCreditCard(creditCardId);


                                    finishPayment();
                                } else {
                                    Toast.makeText(context, R.string.message_type_credit_card_code, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        dialog.show();
                    }
                } else {
                    if (Helper.isEmpty(etUserEmail)) {
                        Toast.makeText(context, R.string.message_type_email, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        final AlertDialog alertDialog = AppUtil.confirmDialog(context,
                                getResources().getString(R.string.message_confirm_your_email), 0, 0);

                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                                purchaseRequest.setPaymentMethod(paymentMethod.getId());
                                purchaseRequest.setBankBillUserEmail(etUserEmail.getText().toString());

                                finishPayment();
                            }
                        });
                    }
                }
            }
        });
    }

    private void finishPayment() {
        String url = AppConfig.webserviceUrl("user/purchase_request");
        Map<String, String> params = new HashMap<String, String>();
        final ArrayList<PurchaseRequestItem> purchaseRequestItems = new ArrayList<PurchaseRequestItem>();

        // User data
        if (!Helper.validString(user.getDocument()) && !Helper.isEmpty(etUserDocument)) {
            user.setDocument(etUserDocument.getText().toString());
            User.datasource(context).updateAllInfo(user);

            params.put("user_document", user.getDocument());
        }

        params.put("user_id", String.valueOf(user.getId()));
        params.put("payment_method_id", String.valueOf(purchaseRequest.getPaymentMethod()));
        params.put("address_id", String.valueOf(purchaseRequest.getAddress()));
        params.put("shipping_type_id", String.valueOf(purchaseRequest.getShippingType()));

        // Credit card params
        if (paymentMethod.getTag().equals("credit_card")) {
            params.put("credit_card_id", String.valueOf(purchaseRequest.getCreditCard()));
            params.put("credit_card_code", String.valueOf(purchaseRequest.getCreditCardCode()));
        }
        // Bank bill params
        else if (paymentMethod.getTag().equals("bank_bill")) {
            params.put("bank_bill_user_email", purchaseRequest.getBankBillUserEmail());
        }

        // Products
        ArrayList<ItemCart> items = itemCartDataSource.getAll();

        for (ItemCart itemCart : items) {
            params.put("sku[]", String.valueOf(itemCart.getSku()));
            params.put("amount[]", String.valueOf(itemCart.getAmount()));
            params.put("cost[]", String.valueOf(itemCart.getCost()));

            PurchaseRequestItem purchaseRequestItem = new PurchaseRequestItem();
            purchaseRequestItem.setSku(itemCart.getSku());
            purchaseRequestItem.setAmount(itemCart.getAmount());
            purchaseRequestItem.setCost(itemCart.getCost());

            purchaseRequestItems.add(purchaseRequestItem);
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, CheckoutPaymentActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                if (response.get("message").toString().equals("Invalid User")) {
                                    Toast.makeText(context, R.string.message_error_on_process_payment_request, Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                // Update purchase request data
                                purchaseRequest.setId(Long.parseLong(response.getString("purchase_request")));
                                purchaseRequest.setDate(response.getString("created_at"));
                                purchaseRequest.setCost(itemCartDataSource.getTotal());
                                purchaseRequest.setShippingCost((float) response.getDouble("shipping_cost"));

                                if (paymentMethod.getTag().equals("bank_bill")) {
                                    purchaseRequest.setBankBillNumber(response.getString("bank_bill_number"));
                                }

                                for (PurchaseRequestItem purchaseRequestItem : purchaseRequestItems) {
                                    purchaseRequestItem.setPurchaseRequest(purchaseRequest.getId());
                                }

                                // Save Purchase Request
                                PurchaseRequestDataSource purchaseRequestDataSource = PurchaseRequest.datasource(context);
                                purchaseRequestDataSource.add(purchaseRequest);

                                // Save Purchase Request items
                                PurchaseRequestItemDataSource purchaseRequestItemDataSource = PurchaseRequestItem.datasource(context);
                                purchaseRequestItemDataSource.addMany(purchaseRequestItems);

                                // Save Purchase Request status
                                JSONArray statusArray = response.getJSONArray("purchase_request_status");

                                for (int i = 0; i < statusArray.length(); i++) {
                                    JSONObject statusObject = statusArray.getJSONObject(i);

                                    PurchaseRequestStatus purchaseRequestStatus = new PurchaseRequestStatus();
                                    purchaseRequestStatus.setPurchaseRequest(purchaseRequest.getId());
                                    purchaseRequestStatus.setTitle(statusObject.getString("title"));
                                    purchaseRequestStatus.setDescription(statusObject.getString("description"));
                                    purchaseRequestStatus.setDate(statusObject.getString("created_at"));

                                    PurchaseRequestStatusDataSource purchaseRequestStatusDataSource = PurchaseRequestStatus.datasource(context);
                                    purchaseRequestStatusDataSource.add(purchaseRequestStatus);
                                }

                                // Clear shopping cart
                                itemCartDataSource.deleteAll();

                                // Start new intent
                                Intent intent = new Intent(context, CheckoutFinishedActivity.class);
                                intent.putExtra("purchase_request", purchaseRequest.getId());
                                intent.putExtra("shipping_type_id", purchaseRequest.getShippingType());

                                if (paymentMethod.getTag().equals("bank_bill")) {
                                    intent.putExtra("bank_bill_number", purchaseRequest.getBankBillNumber());
                                    intent.putExtra("bank_bill_user_email", purchaseRequest.getBankBillUserEmail());
                                }

                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, CheckoutPaymentActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                        }

                        stopProgressBarDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, CheckoutPaymentActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            creditCardId = data.getLongExtra("creditCardId", 0);
            setCurrentCreditCard();
        }
    }

    private void startProgressDialog() {
        progressDialog = ProgressDialog.show(context, null, getResources().getString(R.string.message_wait), true);
    }

    private void stopProgressBarDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
