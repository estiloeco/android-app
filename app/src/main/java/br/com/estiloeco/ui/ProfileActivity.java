package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.etsy.android.grid.StaggeredGridView;
import com.slidinglayer.SlidingLayer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.GridAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.FriendsDataSource;
import br.com.estiloeco.database.NotificationsDataSource;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.Look;
import br.com.estiloeco.model.Notification;
import br.com.estiloeco.model.User;

public class ProfileActivity extends Activity {

    private int RESULT_LOAD_IMAGE = 1;
    private int RESULT_TAKE_PICTURE = 2;

    private Context context;
    private ActionBar actionBar;
    private RequestQueue request;
    private GridAdapter gridAdapter;
    private User user;
    private String stringAvatar;
    private Uri imageUri;
    private UserDataSource userDataSource;
    private FriendsDataSource friendsDataSource;
    private NotificationsDataSource notificationsDataSource;
    // Components
    private GridView gridView;
    private SlidingLayer slidingLayer;
    private ImageView ivUserPhoto;
    private TextView tvUserName;
    private TextView tvFriends;
    private TextView tvNotifications;
    private TextView tvChangeAvatar;
    private TextView tvNoFavoriteLooks;
    private LinearLayout llBtnFriends;
    private LinearLayout llBtnNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_profile);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        // Request
        request = Volley.newRequestQueue(context);

        // Looks
        gridAdapter = new GridAdapter(context);

        // Datasource
        userDataSource = User.datasource(context);
        friendsDataSource = Friend.datasource(context);
        notificationsDataSource = Notification.datasource(context);

        // User
        user = userDataSource.get();

        components();
        componentsActions();
        Helper.buttonMenuBugFix(context);

        // Looks
        downloadLooks();
    }

    @Override
    protected void onResume() {
        super.onResume();
        componentsData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(context, SettingsActivity.class);
            startActivity(intent);
        }
        else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.profile_avatar_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        int id = item.getItemId();

        if (id == R.id.action_gallery) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, RESULT_LOAD_IMAGE);
        }
        else if (id == R.id.action_camera) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "Nova foto");
            values.put(MediaStore.Images.Media.DESCRIPTION, "Da galeria");

            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(cameraIntent, RESULT_TAKE_PICTURE);
        }
        else {
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            stringAvatar = Helper.convertImageToBase64(picturePath);

            updateAvatar();
        }
        else if (requestCode == RESULT_TAKE_PICTURE && resultCode == RESULT_OK) {
            Bitmap avatar = null;

            try {
                avatar = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                stringAvatar = Helper.convertBitmapToBase64(avatar);
            } catch (IOException e) {
                e.printStackTrace();
            }

            updateAvatar();
        }
    }

    private void components() {
        gridView = (GridView) findViewById(R.id.grid_view);
        gridView.setAdapter(gridAdapter);

        slidingLayer = (SlidingLayer) findViewById(R.id.slidingLayer1);
        ivUserPhoto = (ImageView) findViewById(R.id.ivUserPhoto);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvFriends = (TextView) findViewById(R.id.tvFriends);
        tvNotifications = (TextView) findViewById(R.id.tvNotifications);
        tvChangeAvatar = (TextView) findViewById(R.id.tvChangeAvatar);
        tvNoFavoriteLooks = (TextView) findViewById(R.id.tvNoFavoriteLooks);
        llBtnFriends = (LinearLayout) findViewById(R.id.llBtnFriends);
        llBtnNotifications = (LinearLayout) findViewById(R.id.llBtnNotifications);
    }

    private void componentsData() {
        tvUserName.setText(user.getName());
        tvFriends.setText(String.valueOf(friendsDataSource.count()));
        tvNotifications.setText(String.valueOf(notificationsDataSource.count()));

        if (Helper.validString(user.getAvatar())) {
            loadAvatar(user.getAvatar(), true);
        }
    }

    private void componentsActions() {
        ivUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerForContextMenu(ivUserPhoto);
                openContextMenu(view);
            }
        });

        llBtnFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FriendsActivity.class);
                startActivity(intent);
            }
        });

        llBtnNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NotificationsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void loadAvatar(String avatar, boolean cache) {
        if (cache) {
            Picasso.with(context).load(AppConfig.ASSETS_URL + avatar)
                    .transform(new Helper().new CircleTransform()).into(ivUserPhoto);
        }
        else {
            Picasso.with(context).load(AppConfig.ASSETS_URL + avatar).skipMemoryCache()
                    .transform(new Helper().new CircleTransform()).into(ivUserPhoto);
        }

        tvChangeAvatar.setVisibility(View.INVISIBLE);
    }

    private void updateAvatar() {
        String url = AppConfig.webserviceUrl("profile/avatar");
        Map<String, String> params = new HashMap<String, String>();

        // Params
        params.put("email", String.valueOf(user.getEmail()));
        params.put("avatar", stringAvatar);

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject data = response;

                        try {
                            if (!data.getBoolean("status")) {
                                Log.e(AppConfig.TAG, ProfileActivity.this.getLocalClassName().toString() + " - " + data.get("message").toString());
                            }
                            else {
                                loadAvatar(data.getString("avatar").toString(), false);

                                // Update local avatar
                                ContentValues values = new ContentValues();
                                values.put("avatar", data.getString("avatar").toString());
                                userDataSource.update(values);
                            }
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, ProfileActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, ProfileActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void populateLooksList(JSONObject data) {
        List<Look> list = new ArrayList<Look>();

        try {
            if (!data.isNull("error")) {
                gridView.setVisibility(View.GONE);
                tvNoFavoriteLooks.setVisibility(View.VISIBLE);

                Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + data.get("error").toString());
            }
            else {
                Log.i(AppConfig.TAG, " - " + data.toString());

                JSONArray array = new JSONArray(data.get("data").toString());

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = new JSONObject(array.getString(i));

                    ArrayList<String> images = new ArrayList<String>();
                    images.add(object.getString("image"));

                    Look look = new Look();
                    look.setId(object.getInt("id"));
                    look.setLikes(object.getInt("likes"));
                    look.setImages(images);
                    look.setUserLike(object.getBoolean("like"));

                    list.add(look);
                }

                if (gridAdapter.getCount() == 0) {
                    gridView.setAdapter(gridAdapter);
                }

                gridAdapter.refresh(list);

                // Set height of gridView
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                ViewGroup.LayoutParams layoutParams = gridView.getLayoutParams();
                layoutParams.height = (int)((Math.round((float)gridAdapter.getCount()/2)*350) * metrics.density);
                gridView.setLayoutParams(layoutParams);
                gridView.requestLayout();

                if (gridAdapter.getCount() > 0) {
                    gridView.setVisibility(View.VISIBLE);
                    tvNoFavoriteLooks.setVisibility(View.GONE);
                }
                else {
                    gridView.setVisibility(View.GONE);
                    tvNoFavoriteLooks.setVisibility(View.VISIBLE);
                }
            }
        } catch (JSONException e) {
            gridView.setVisibility(View.GONE);
            tvNoFavoriteLooks.setVisibility(View.VISIBLE);

            Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + e.getMessage());
        }
    }

    private void downloadLooks() {
        String url = AppConfig.webserviceUrl("profile/looks");
        Map<String, String> params = new HashMap<String, String>();
        params.put("userId", String.valueOf(user.getId()));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopLoading();
                        populateLooksList(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, ProfileActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }
}
