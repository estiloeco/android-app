package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.AddressDataSource;
import br.com.estiloeco.database.CreditCardFlagDataSource;
import br.com.estiloeco.database.PaymentMethodDataSource;
import br.com.estiloeco.database.ShippingTypeDataSource;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.PaymentMethod;
import br.com.estiloeco.model.ShippingType;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;
import br.com.estiloeco.utils.GcmUtil;

public class RegisterAddressActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private ArrayAdapter<String> adapterStates;
    private User user;
    private Address address;
    private boolean isCheckout;
    private RequestQueue request;
    // Components
    private TextView tvErrorMessage;
    private EditText etAddressName;
    private EditText etAddressCep;
    private EditText etAddressStreet;
    private EditText etAddressNumber;
    private EditText etAddressComplement;
    private EditText etAddressNeighborhood;
    private EditText etAddressCity;
    private Spinner spiAddressState;
    private Button btnCreateAnAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_register_address);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        // Request
        request = Volley.newRequestQueue(context);

        // Is checkout
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.containsKey("isCheckout")) {
                isCheckout = extras.getBoolean("isCheckout");
            }
        }

        // Components
        components();
        componentsData();
        componentsAction();

        // Register data
        ArrayList<String> register = getIntent().getStringArrayListExtra("register");

        user = new User();
        user.setName(register.get(0));
        user.setLastName(register.get(1));
        user.setGender(register.get(2));
        user.setBirthdate(register.get(3));
        user.setDocument(register.get(4));
        user.setEmail(register.get(5));
        user.setPassword(register.get(6));
        user.setOptIn(Boolean.valueOf(register.get(7)));
        user.setFacebookId(register.get(8));
        user.setFacebookToken(register.get(9));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        tvErrorMessage = (TextView) findViewById(R.id.tvErrorMessage);
        etAddressName = (EditText) findViewById(R.id.etAddressName);
        etAddressCep = (EditText) findViewById(R.id.etAddressCep);
        etAddressStreet = (EditText) findViewById(R.id.etAddressStreet);
        etAddressNumber = (EditText) findViewById(R.id.etAddressNumber);
        etAddressComplement = (EditText) findViewById(R.id.etAddressComplement);
        etAddressNeighborhood = (EditText) findViewById(R.id.etAddressNeighborhood);
        etAddressCity = (EditText) findViewById(R.id.etAddressCity);
        spiAddressState = (Spinner) findViewById(R.id.spiAddressState);
        btnCreateAnAccount = (Button) findViewById(R.id.btnCreateAnAccount);
    }

    private void componentsData() {
        adapterStates = new ArrayAdapter<String>(context,
                R.layout.simple_spinner_item, getResources().getStringArray(R.array.states));
        adapterStates.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spiAddressState.setAdapter(adapterStates);
    }

    private void componentsAction() {
        etAddressCep.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_UP) && etAddressCep.getText().toString().length() >= 8) {
                    verifyCep(etAddressCep.getText().toString());
                }

                return false;
            }
        });

        btnCreateAnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    address = new Address();
                    address.setName(etAddressName.getText().toString());
                    address.setCep(etAddressCep.getText().toString());
                    address.setStreet(etAddressStreet.getText().toString());
                    address.setNumber(etAddressNumber.getText().toString());
                    address.setComplement(etAddressComplement.getText().toString());
                    address.setNeighborhood(etAddressNeighborhood.getText().toString());
                    address.setCity(etAddressCity.getText().toString());
                    address.setState(spiAddressState.getSelectedItem().toString());
                    address.setPrincipal(true);

                    saveUserData();
                }
            }
        });
    }

    private boolean validation() {
        boolean status = true;

        if (Helper.isEmpty(etAddressName) || Helper.isEmpty(etAddressCep) ||
                Helper.isEmpty(etAddressStreet) || Helper.isEmpty(etAddressNumber) ||
                Helper.isEmpty(etAddressNeighborhood) || Helper.isEmpty(etAddressCity)) {

            Toast.makeText(context, R.string.message_error_fields, Toast.LENGTH_SHORT).show();
            status = false;
        }

        if (!status) {
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
        else {
            tvErrorMessage.setVisibility(View.GONE);
        }

        return status;
    }

    private void success() {
        // Save user
        UserDataSource userDatasource = User.datasource(context);
        userDatasource.add(user);

        // Save principal address
        AddressDataSource addressDataSource = Address.datasource(context);
        addressDataSource.add(address);

        Intent intent = new Intent(context, RegisterSuccessActivity.class);
        intent.putExtra("userEmail", user.getEmail());

        if (isCheckout) {
            intent.putExtra("isCheckout", true);
        }

        startActivity(intent);
    }

    private void error() {
        Toast.makeText(context, R.string.message_user_already_exists, Toast.LENGTH_LONG).show();
    }

    private void loadSystemData(JSONObject system) {
        // Payment methods
        PaymentMethodDataSource paymentMethodDataSource = PaymentMethod.datasource(context);

        try {
            JSONArray paymentMethodsArray = system.getJSONArray("payment_methods");

            for (int i = 0; i < paymentMethodsArray.length(); i++) {
                JSONObject paymentMethodObject = paymentMethodsArray.getJSONObject(i);

                PaymentMethod paymentMethod = new PaymentMethod();
                paymentMethod.setId(paymentMethodObject.getLong("id"));
                paymentMethod.setDescription(paymentMethodObject.getString("description"));
                paymentMethod.setTag(paymentMethodObject.getString("tag"));

                paymentMethodDataSource.add(paymentMethod);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Credit Card Flags
        CreditCardFlagDataSource creditCardFlagDataSource = CreditCardFlag.datasource(context);

        try {
            JSONArray creditCardFlagsArray = system.getJSONArray("credit_card_flags");

            for (int i = 0; i < creditCardFlagsArray.length(); i++) {
                JSONObject creditCardFlagObject = creditCardFlagsArray.getJSONObject(i);

                CreditCardFlag creditCardFlag = new CreditCardFlag();
                creditCardFlag.setId(creditCardFlagObject.getLong("id"));
                creditCardFlag.setDescription(creditCardFlagObject.getString("description"));

                creditCardFlagDataSource.add(creditCardFlag);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Shipping types
        ShippingTypeDataSource shippingTypeDataSource = ShippingType.datasource(context);

        try {
            JSONArray shippingTypesArray = system.getJSONArray("shipping_types");

            for (int i = 0; i < shippingTypesArray.length(); i++) {
                JSONObject shippingTypeObject = shippingTypesArray.getJSONObject(i);

                ShippingType shippingType= new ShippingType();
                shippingType.setId(shippingTypeObject.getLong("id"));
                shippingType.setDescription(shippingTypeObject.getString("description"));
                shippingType.setTag(shippingTypeObject.getString("tag"));

                shippingTypeDataSource.add(shippingType);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void populateAddress(Address address) {
        etAddressStreet.setText(address.getStreet());
        etAddressNeighborhood.setText(address.getNeighborhood());
        etAddressCity.setText(address.getCity());
        spiAddressState.setSelection(adapterStates.getPosition(address.getState()));
    }

    private void verifyCep(String cep) {
        String url = AppConfig.WEBSERVICE_CEP_URL + cep + ".json";
        Map<String, String> params = new HashMap<String, String>();

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(Request.Method.GET, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Address address = new Address();
                            address.setStreet(response.get("logradouro").toString());
                            address.setNeighborhood(response.get("bairro").toString());
                            address.setCity(response.get("localidade").toString());
                            address.setState(response.get("uf").toString());

                            populateAddress(address);
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, RegisterAddressActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                            Toast.makeText(context, R.string.message_cep_not_found, Toast.LENGTH_SHORT).show();
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, RegisterAddressActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_cep_not_found, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void saveUserData() {
        String url = AppConfig.webserviceUrl("user");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("name", user.getName());
        params.put("last_name", user.getLastName());
        params.put("gender", user.getGender());
        params.put("birthdate", user.getBirthdate());
        params.put("document", user.getDocument());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());
        params.put("password", user.getPassword());
        params.put("optin", (user.isOptIn() ? "1" : "0"));
        params.put("facebookId", user.getFacebookId());
        params.put("facebookToken", user.getFacebookToken());
        params.put("legal_text_accepted", "1");
        params.put("address_name", address.getName());
        params.put("address_cep", address.getCep());
        params.put("address_street", address.getStreet());
        params.put("address_number", address.getNumber());
        params.put("address_complement", address.getComplement());
        params.put("address_neighborhood", address.getNeighborhood());
        params.put("address_city", address.getCity());
        params.put("address_state", address.getState());
        params.put("type", AppConfig.DEVICE_TYPE);
        params.put("gcm_id", GcmUtil.getId(getBaseContext()));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, RegisterAddressActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                if (response.get("message").toString().equals("User already exists")) {
                                    error();
                                }
                            }
                            else {
                                user.setId(Long.valueOf(response.getString("user")));
                                address.setId(Long.valueOf(response.getString("address")));

                                loadSystemData(response.getJSONObject("system"));

                                success();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, RegisterAddressActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(AppConfig.WEBSERVICE_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }
}
