package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.AddressDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;
import br.com.estiloeco.utils.GcmUtil;

public class FormUserAddressActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private User user;
    private Address address;
    private RequestQueue request;
    private AddressDataSource addressDataSource;
    private ArrayAdapter<String> adapterStates;
    private boolean isNew;
    private ProgressDialog progressDialog;
    // Components
    private EditText etAddressName;
    private EditText etAddressCep;
    private EditText etAddressStreet;
    private EditText etAddressNumber;
    private EditText etAddressComplement;
    private EditText etAddressNeighborhood;
    private EditText etAddressCity;
    private Spinner spiAddressState;
    private CheckBox chkPrincipalAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_form_user_address);

        // Context
        context = this;

        // Request
        request = Volley.newRequestQueue(context);

        // User
        user = User.logged(context);

        // Datasource
        addressDataSource = Address.datasource(context);

        // Current address
        address = new Address();

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
        componentsData();
        componentsActions();
        readOnly();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_user_address, menu);

        if (!isNew) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
            getActionBar().setTitle(getResources().getString(R.string.title_address) + " " + address.getName());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if ((!Helper.isEmpty(etAddressName) || !Helper.isEmpty(etAddressCep)) && address.getId() == 0) {
                AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources().getString(R.string.message_save_new_data), 0, 0);

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
            }
            else {
                finish();
            }
        }
        else if (id == R.id.action_save) {
            if (validation()) {
                address.setName(etAddressName.getText().toString());
                address.setCep(etAddressCep.getText().toString());
                address.setStreet(etAddressStreet.getText().toString());
                address.setNumber(etAddressNumber.getText().toString());
                address.setComplement(etAddressComplement.getText().toString());
                address.setNeighborhood(etAddressNeighborhood.getText().toString());
                address.setCity(etAddressCity.getText().toString());
                address.setState(spiAddressState.getSelectedItem().toString());
                address.setPrincipal(chkPrincipalAddress.isChecked());

                saveAddress();
            }
        }
        else if (id == R.id.action_remove) {
            final AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources()
                    .getString(R.string.message_sure_address), 0, 0);

            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    removeAddress();
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        etAddressName = (EditText) findViewById(R.id.etAddressName);
        etAddressCep = (EditText) findViewById(R.id.etAddressCep);
        etAddressStreet = (EditText) findViewById(R.id.etAddressStreet);
        etAddressNumber = (EditText) findViewById(R.id.etAddressNumber);
        etAddressComplement = (EditText) findViewById(R.id.etAddressComplement);
        etAddressNeighborhood = (EditText) findViewById(R.id.etAddressNeighborhood);
        etAddressCity = (EditText) findViewById(R.id.etAddressCity);
        spiAddressState = (Spinner) findViewById(R.id.spiAddressState);
        chkPrincipalAddress = (CheckBox) findViewById(R.id.chkPrincipalAddress);
    }

    private void componentsData() {
        adapterStates = new ArrayAdapter<String>(context,
                R.layout.simple_spinner_item, getResources().getStringArray(R.array.states));
        adapterStates.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spiAddressState.setAdapter(adapterStates);

        // Extras
        if (getIntent().hasExtra("addressId")) {
            address = addressDataSource.get(getIntent().getExtras().getLong("addressId"));

            if (address.getId() != 0) {
                etAddressName.setText(address.getName());
                etAddressCep.setText(address.getCep());
                etAddressStreet.setText(address.getStreet());
                etAddressNumber.setText(address.getNumber());
                etAddressComplement.setText(address.getComplement());
                etAddressNeighborhood.setText(address.getNeighborhood());
                etAddressCity.setText(address.getCity());
                spiAddressState.setSelection(adapterStates.getPosition(address.getState()));
                chkPrincipalAddress.setChecked(address.isPrincipal());
            }
        }
        else {
            isNew = true;
        }
    }

    private void componentsActions() {
        etAddressCep.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_UP) && etAddressCep.getText().toString().length() >= 8) {
                    verifyCep(etAddressCep.getText().toString());
                }

                return false;
            }
        });

        chkPrincipalAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!isNew) {
                    address.setPrincipal(b);
                    saveAddress();
                }
            }
        });
    }

    private void readOnly() {
        if (!isNew) {
            etAddressName.setEnabled(false);
            etAddressCep.setEnabled(false);
            etAddressStreet.setEnabled(false);
            etAddressNumber.setEnabled(false);
            etAddressComplement.setEnabled(false);
            etAddressNeighborhood.setEnabled(false);
            etAddressCity.setEnabled(false);
            spiAddressState.setEnabled(false);
        }
    }

    private boolean validation() {
        boolean status = true;

        if (Helper.isEmpty(etAddressName) || Helper.isEmpty(etAddressCep) || Helper.isEmpty(etAddressStreet) ||
                Helper.isEmpty(etAddressNumber) || Helper.isEmpty(etAddressNeighborhood) || Helper.isEmpty(etAddressCity)) {

            Toast.makeText(context, R.string.message_error_fields, Toast.LENGTH_SHORT).show();
            status = false;
        }

        return status;
    }

    private void success() {
        if (isNew) {
            // Save address
            addressDataSource.add(address);

            Toast.makeText(context, R.string.message_address_added, Toast.LENGTH_LONG).show();
            finish();
        }
        else {
            if (address.isPrincipal()) {
                addressDataSource.setAsPrincipal(address);
            }
            else {
                addressDataSource.setAsNonPrincipal(address);
            }

            Toast.makeText(context, R.string.message_address_updated, Toast.LENGTH_LONG).show();
        }
    }

    private void error() {
        Toast.makeText(context, R.string.message_error_on_save_data, Toast.LENGTH_LONG).show();
    }

    private void populateAddress(Address address) {
        etAddressStreet.setText(address.getStreet());
        etAddressNeighborhood.setText(address.getNeighborhood());
        etAddressCity.setText(address.getCity());
        spiAddressState.setSelection(adapterStates.getPosition(address.getState()));
    }

    private void verifyCep(String cep) {
        String url = AppConfig.WEBSERVICE_CEP_URL + cep + ".json";
        Map<String, String> params = new HashMap<String, String>();

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(Request.Method.GET, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Address address = new Address();
                            address.setStreet(response.get("logradouro").toString());
                            address.setNeighborhood(response.get("bairro").toString());
                            address.setCity(response.get("localidade").toString());
                            address.setState(response.get("uf").toString());

                            populateAddress(address);
                        } catch (JSONException e) {
                            Log.e(AppConfig.TAG, FormUserAddressActivity.this.getLocalClassName().toString() + " - " + e.getMessage());
                            Toast.makeText(context, R.string.message_cep_not_found, Toast.LENGTH_SHORT).show();
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, FormUserAddressActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_cep_not_found, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void removeAddress() {
        String url = AppConfig.webserviceUrl("user/address/delete");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("id", String.valueOf(address.getId()));
        params.put("user_id", String.valueOf(user.getId()));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, FormUserAddressActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                Toast.makeText(context, R.string.message_error_on_remove_address, Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(context, R.string.message_address_removed, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopProgressBarDialog();

                        addressDataSource.delete(address.getId());

                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, FormUserAddressActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    private void saveAddress() {
        String url = AppConfig.webserviceUrl("user/address");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("user_id", String.valueOf(user.getId()));
        params.put("name", address.getName());
        params.put("cep", address.getCep());
        params.put("street", address.getStreet());
        params.put("number", address.getNumber());
        params.put("complement", address.getComplement());
        params.put("neighborhood", address.getNeighborhood());
        params.put("city", address.getCity());
        params.put("state", address.getState());
        params.put("principal", address.isPrincipal() ? "1" : "0");

        if (address.getId() != 0) {
            params.put("id", String.valueOf(address.getId()));
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, FormUserAddressActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                if (response.get("message").toString().equals("Invalid User")) {
                                    error();
                                }
                            }
                            else {
                                address.setId(response.getLong("address"));
                                success();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopProgressBarDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, FormUserAddressActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    private void startProgressDialog() {
        progressDialog = ProgressDialog.show(context, null, getResources().getString(R.string.message_wait), true);
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }

    private void stopProgressBarDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
