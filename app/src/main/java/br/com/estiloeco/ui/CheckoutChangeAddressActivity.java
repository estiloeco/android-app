package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.AddressAdapter;
import br.com.estiloeco.database.AddressDataSource;
import br.com.estiloeco.model.Address;

public class CheckoutChangeAddressActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private AddressDataSource addressDataSource;
    private AddressAdapter addressAdapter;
    // Components
    private ListView lvAddresses;
    private TextView tvNoAddresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_change_address);

        // Context
        context = this;

        // Datasource
        addressDataSource = Address.datasource(context);

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
    }

    @Override
    protected void onResume() {
        super.onResume();
        componentsData();
        componentsActions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_buy_change_address, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        else if (id == R.id.action_add_address) {
            Intent intent = new Intent(context, FormUserAddressActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        lvAddresses = (ListView) findViewById(R.id.lvAddresses);
        tvNoAddresses = (TextView) findViewById(R.id.tvNoAddresses);

        addressAdapter = new AddressAdapter(context, new ArrayList<Address>());
        lvAddresses.setAdapter(addressAdapter);
    }

    private void componentsData() {
        ArrayList<Address> list = addressDataSource.getAll();

        if (list.size() > 0) {
            lvAddresses.setVisibility(View.VISIBLE);
            tvNoAddresses.setVisibility(View.INVISIBLE);
        }
        else {
            lvAddresses.setVisibility(View.GONE);
            tvNoAddresses.setVisibility(View.VISIBLE);
        }

        addressAdapter.refresh(list);
    }

    private void componentsActions() {
        // List
        lvAddresses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("addressId", addressAdapter.getItem(i).getId());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
