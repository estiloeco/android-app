package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.GridAdapter;
import br.com.estiloeco.adapter.ShoppingCartAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.ItemCartDataSource;
import br.com.estiloeco.database.PersonalFiltersDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.helpers.NumberFormats;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.CreditCard;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.Filter;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.ItemCart;
import br.com.estiloeco.model.Look;
import br.com.estiloeco.model.Notification;
import br.com.estiloeco.model.PaymentMethod;
import br.com.estiloeco.model.PurchaseRequest;
import br.com.estiloeco.model.PurchaseRequestItem;
import br.com.estiloeco.model.PurchaseRequestStatus;
import br.com.estiloeco.model.ShippingType;
import br.com.estiloeco.model.User;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.etsy.android.grid.StaggeredGridView;
import com.slidinglayer.SlidingLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private ActionBar actionBar;
    private GridAdapter gridAdapter;
    private User user;
    private Menu menu;
    private RequestQueue request;
    private List<ItemCart> listCart;
    // Components
    private GridView gridView;
    private SlidingLayer slidingLayer;
    private ListView lvItemsShoppingCart;
    private Button btnCalculateShipping;
    private Button btnCheckout;
    private RelativeLayout containerShipping;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvEmptyShoppingCart;
    private RelativeLayout rlSubtotal;
    private TextView tvFinalCost;
    private LinearLayout rlCantLoadLooks;
    private Button btnReloadLooks;
    private TextView tvNoLooks;

    // Database
    private PersonalFiltersDataSource personalFiltersDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeHome);
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        setContentView(R.layout.activity_main);

        // Context
        context = this;

        // Direct Entry
        directEntry();

        // Calls
        prepareActionBar();
        components();
        componentsData();
        componentsActions();
        Helper.buttonMenuBugFix(context);

        // Open Database
        personalFiltersDataSource = Filter.datasource(context);

        // Request
        request = Volley.newRequestQueue(context);

        // Logged User
        user = User.logged(context);

        // Looks
        downloadLooks();
    }

    private void directEntry() {
        if (getIntent().hasExtra("directEntry")) {
            String key = getIntent().getExtras().getString("directEntry").toString();

            if (key.equals("notifications")) {
                Intent intent = new Intent(this, NotificationsActivity.class);
                startActivity(intent);
            }
            else if (key.equals("friends")) {
                Intent intent = new Intent(this, FriendsActivity.class);
                startActivity(intent);
            }
        }
        else if (getIntent().hasExtra("goToLook")) {
            long lookId = getIntent().getLongExtra("goToLook", 0);

            if (lookId > 0) {
                Intent intent = new Intent(context, LookActivity.class);
                intent.putExtra("lookId", lookId);
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logged User
        user = User.logged(context);

        // Look's list
        if (Helper.getBooleanPreference(context, "update-list")) {
            downloadLooks();
            Helper.setPreferences(context, "update-list", false).commit();
        }

        // Shopping cart
        updateShoppingCart();

        if (menu != null) {
            updateMenu();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;
        updateMenu();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(context, SettingsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_filter) {
            Intent intent = new Intent(context, FilterActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_profile) {
            Intent intent = new Intent(context, ProfileActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_shopping_cart) {
            if (slidingLayer.isOpened()) {
                slidingLayer.closeLayer(true);
            }
            else {
                slidingLayer.openLayer(true);
            }
        }
        else if (id == R.id.action_my_orders) {
            Intent intent = new Intent(context, PurchaseRequestActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_login) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_logout) {
            User.datasource(context).delete();
            Address.datasource(context).deleteAll();
            Friend.datasource(context).deleteAll();
            Notification.datasource(context).deleteAll();
            Filter.datasource(context).deleteAll();
            CreditCard.datasource(context).deleteAll();
            CreditCardFlag.datasource(context).deleteAll();
            PaymentMethod.datasource(context).deleteAll();
            ShippingType.datasource(context).deleteAll();
            PurchaseRequest.datasource(context).deleteAll();
            PurchaseRequestItem.datasource(context).deleteAll();
            PurchaseRequestStatus.datasource(context).deleteAll();

            user = null;

            updateMenu();
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateMenu() {
        if (user != null) {
            menu.getItem(2).setVisible(Boolean.TRUE);
            menu.getItem(3).setVisible(Boolean.TRUE);
            menu.getItem(5).setVisible(Boolean.TRUE);
            menu.getItem(6).setVisible(Boolean.FALSE);
        }
        else {
            menu.getItem(2).setVisible(Boolean.FALSE);
            menu.getItem(3).setVisible(Boolean.FALSE);
            menu.getItem(5).setVisible(Boolean.FALSE);
            menu.getItem(6).setVisible(Boolean.TRUE);
        }

        if (listCart.size() > 0) {
            menu.getItem(1).setVisible(true);
        }
        else {
            menu.getItem(1).setVisible(false);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && slidingLayer.isOpened()) {
            slidingLayer.closeLayer(true);
            return false;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void components() {
        // Pull to refresh
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.principal_dark, R.color.principal_on,
                R.color.principal, R.color.principal_xlight);

        // Other components
        gridView = (GridView) findViewById(R.id.grid_view);
        slidingLayer = (SlidingLayer) findViewById(R.id.slidingLayer1);
        lvItemsShoppingCart = (ListView) findViewById(R.id.lvItemsShoppingCart);
        btnCalculateShipping = (Button) findViewById(R.id.btnCalculateShipping);
        btnCheckout = (Button) findViewById(R.id.btnCheckout);
        containerShipping = (RelativeLayout) findViewById(R.id.containerShipping);
        tvEmptyShoppingCart = (TextView) findViewById(R.id.tvEmptyShoppingCart);
        rlSubtotal = (RelativeLayout) findViewById(R.id.rlSubtotal);
        tvFinalCost = (TextView) findViewById(R.id.tvFinalCost);
        rlCantLoadLooks = (LinearLayout) findViewById(R.id.rlCantLoadLooks);
        btnReloadLooks = (Button) findViewById(R.id.btnReloadLooks);
        tvNoLooks = (TextView) findViewById(R.id.tvNoLooks);
    }

    private void componentsData() {
        // Shopping cart
        updateShoppingCart();
    }

    private void componentsActions() {
        // GridView Scroll listener
        gridView.setOnScrollListener(new StaggeredGridView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                if (i == 0 && i2 > 0 && gridView.getChildAt(0).getTop() >= 0) {
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });

        // Reload looks
        btnReloadLooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlCantLoadLooks.setVisibility(View.GONE);
                downloadLooks();
            }
        });

        // GridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (slidingLayer.isOpened()) {
                    closeShoppingCart();
                }
                else {
                    Intent intent = new Intent(context, LookActivity.class);
                    intent.putExtra("lookId", gridAdapter.getItem(i).getId());
                    startActivity(intent);
                }
            }
        });

        // Calculate Shipping
        btnCalculateShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                final View dialogView = inflater.inflate(R.layout.dialog_calculate_shipping, null);
                final AlertDialog dialog = new AlertDialog.Builder(context).create();
                dialog.setView(dialogView);

                // Components
                Button btnCalculate = (Button) dialogView.findViewById(R.id.btnCalculate);
                Button btnCancel = (Button) dialogView.findViewById(R.id.btnCancel);

                // Actions
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                // Shipping
                btnCalculate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        containerShipping.setVisibility(View.VISIBLE);
                        btnCalculateShipping.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        // Finish shop
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeShoppingCart();
                Intent intent = null;

                if (User.logged(context) != null) {
                    intent = new Intent(context, CheckoutShippingActivity.class);
                }
                else {
                    Helper.setPreferences(context, "isCheckout", true).commit();
                    Helper.getBooleanPreference(context, "isCheckout");
                    intent = new Intent(context, LoginActivity.class);
                }

                startActivity(intent);
            }
        });
    }

    public void updateShoppingCart() {
        ItemCartDataSource datasource = ItemCart.datasource(context);
        listCart = datasource.getAll();

        if (listCart.size() > 0) {
            tvEmptyShoppingCart.setVisibility(View.GONE);
            btnCalculateShipping.setVisibility(View.VISIBLE);
            btnCheckout.setVisibility(View.VISIBLE);
            rlSubtotal.setVisibility(View.VISIBLE);

            updateShoppingCartTotal();

            ShoppingCartAdapter adapterShoppingCart = new ShoppingCartAdapter(context, listCart, datasource);
            lvItemsShoppingCart.setAdapter(adapterShoppingCart);

            if (menu != null) {
                menu.getItem(1).setVisible(true);
            }
        }
        else {
            tvEmptyShoppingCart.setVisibility(View.VISIBLE);
            btnCalculateShipping.setVisibility(View.INVISIBLE);
            btnCheckout.setVisibility(View.INVISIBLE);
            rlSubtotal.setVisibility(View.INVISIBLE);

            tvFinalCost.setText("R$ 0,00");

            if (menu != null) {
                menu.getItem(1).setVisible(false);
            }

            closeShoppingCart();
        }
    }

    public void updateShoppingCartTotal() {
        ItemCartDataSource itemCartDataSource = ItemCart.datasource(context);
        tvFinalCost.setText("R$ " + NumberFormats.toMoney(itemCartDataSource.getTotal()));
    }

    private void closeShoppingCart() {
        // CLose slidingLayer when is open
        if (slidingLayer.isOpened()) {
            slidingLayer.closeLayer(true);
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    private void populateLooksList(JSONObject data) {
        List<Look> list = new ArrayList<Look>();

        try {
            if (!data.isNull("error")) {
                Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + data.get("error").toString());
            }
            else {
                JSONArray array = data.getJSONArray("data");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);

                    ArrayList<String> images = new ArrayList<String>();
                    images.add(object.getString("image"));

                    Look look = new Look();
                    look.setId(object.getInt("id"));
                    look.setLikes(object.getInt("likes"));
                    look.setImages(images);
                    look.setUserLike(object.getBoolean("like"));

                    // Friends
                    JSONArray friendsArray = object.getJSONArray("friends");

                    if (friendsArray.length() > 0) {
                        ArrayList<Friend> friends = new ArrayList<Friend>();

                        for (int j = 0; j < friendsArray.length(); j++) {
                            JSONObject friendObject = friendsArray.getJSONObject(j);

                            Friend friend = new Friend();
                            friend.setName(friendObject.getString("name"));
                            friend.setAvatar(friendObject.getString("avatar"));

                            friends.add(friend);
                        }

                        look.setFriends(friends);
                    }

                    list.add(look);
                }

                if (gridAdapter == null) {
                    gridAdapter = new GridAdapter(context, list);
                    gridView.setAdapter(gridAdapter);
                }
                else {
                    gridAdapter.refresh(list);
                }

                if (list.size() > 0) {
                    tvNoLooks.setVisibility(View.GONE);
                    gridView.setVisibility(View.VISIBLE);
                }
                else {
                    tvNoLooks.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.INVISIBLE);
                }
            }
        } catch (JSONException e) {
            Log.e(AppConfig.TAG, this.getLocalClassName().toString() + " - " + e.getMessage());
        }
    }

    private void downloadLooks() {
        String url = AppConfig.webserviceUrl("looks");
        Map<String, String> params = new HashMap<String, String>();

        // Personal Filters
        List<Integer> personalFilters = personalFiltersDataSource.getAll();

        if (personalFilters.size() > 0) {
            String filters = "";

            for (int i = 0; i < personalFilters.size(); i++) {
                filters += String.valueOf(personalFilters.get(i)) + (i < (personalFilters.size()-1) ? "-" : "");
            }

            params.put("filters", filters);
            Log.i(AppConfig.TAG, this.getLocalClassName().toString() + " - " + filters);
        }

        if (user != null) {
            params.put("userId", String.valueOf(user.getId()));
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    stopLoading();
                    populateLooksList(response);

                    rlCantLoadLooks.setVisibility(View.GONE);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    stopLoading();
                    Log.e(AppConfig.TAG, MainActivity.this.getLocalClassName().toString() + " - " + error);
                    Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();

                    rlCantLoadLooks.setVisibility(View.VISIBLE);
                }
            }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    private void stopLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

}
