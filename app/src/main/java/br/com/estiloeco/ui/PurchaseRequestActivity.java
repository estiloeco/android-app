package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.PurchaseRequestAdapter;
import br.com.estiloeco.model.PurchaseRequest;

public class PurchaseRequestActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private PurchaseRequestAdapter purchaseRequestAdapter;
    // Components
    private ListView lvPurchaseRequests;
    private TextView tvNoPurchaseRequests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_request);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
        componentsData();
        componentsActions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        lvPurchaseRequests = (ListView) findViewById(R.id.lvPurchaseRequests);
        tvNoPurchaseRequests = (TextView) findViewById(R.id.tvNoPurchaseRequests);
    }

    private void componentsData() {
        ArrayList<PurchaseRequest> purchaseRequests = PurchaseRequest.datasource(context).getAll();

        if (purchaseRequests.size() == 0) {
            lvPurchaseRequests.setVisibility(View.GONE);
            tvNoPurchaseRequests.setVisibility(View.VISIBLE);
        }
        else {
            lvPurchaseRequests.setVisibility(View.VISIBLE);
            tvNoPurchaseRequests.setVisibility(View.GONE);

            purchaseRequestAdapter = new PurchaseRequestAdapter(context, purchaseRequests);
            lvPurchaseRequests.setAdapter(purchaseRequestAdapter);
        }
    }

    private void componentsActions() {
        lvPurchaseRequests.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(context, PurchaseRequestDetailActivity.class);
                intent.putExtra("id", purchaseRequestAdapter.getItem(i).getId());
                startActivity(intent);
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
