package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import br.com.estiloeco.R;

public class RegisterSuccessActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private boolean isCheckout;
    // Components
    private TextView tvUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeBuyFinished);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_success);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        components();

        if (getIntent().hasExtra("userEmail")) {
            // is Checkout
            isCheckout = getIntent().getExtras().getBoolean("isCheckout");

            // user screen
            tvUserEmail.setText(getIntent().getExtras().getString("userEmail"));
        }
        else {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (isCheckout) {
                Intent intent = new Intent(context, CheckoutShippingActivity.class);
                startActivity(intent);
            }
            else {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        tvUserEmail = (TextView) findViewById(R.id.tvUserEmail);
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
