package br.com.estiloeco.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.GcmUtil;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends Activity {

    private static int TIME_OUT = 1000;
    private Context context;
    private RequestQueue request;
    private String userGcmId;
    // Components
    private ProgressBar progressBar;
    private TextView tvDeviceWithoutGooglePlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        setContentView(R.layout.activity_splash);

        context = this;

        // Request
        request = Volley.newRequestQueue(context);

        components();

        if (preRequisites()) {
            setGcmId();
        }
        else {
            tvDeviceWithoutGooglePlay.setVisibility(View.VISIBLE);
        }
    }

    private void goToMain(int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);

                finish();
            }
        }, delay);
    }

    private void components() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvDeviceWithoutGooglePlay = (TextView) findViewById(R.id.tvDeviceWithoutGooglePlay);
    }

    public boolean preRequisites() {
        boolean status = true;

        if (!Helper.checkPlayServices(SplashActivity.this)) {
            status = false;
        }

        return status;
    }

    private void setGcmId() {
        String userGcmId = GcmUtil.getId(getBaseContext());

        if (userGcmId == null) {
            new registerOnGcm().execute();
        }
        else {
            Log.i(AppConfig.TAG, "GCM ID: " + userGcmId);
            goToMain(0);
        }
    }

    private void registerDevice() {
        User user = User.logged(context);

        String url = AppConfig.webserviceUrl("gcm");
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", AppConfig.DEVICE_TYPE);
        params.put("gcm_id", userGcmId);

        if (user != null) {
            params.put("user_id", String.valueOf(user.getId()));
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopLoading();
                        goToMain(TIME_OUT);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Start loading
        startLoading();
    }

    private void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    private class registerOnGcm extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startLoading();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
                userGcmId = gcm.register(AppConfig.GOOGLE_GCM_SENDER_ID);
            } catch (IOException e) {
                Log.i(AppConfig.TAG, "GCM - " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            stopLoading();

            GcmUtil.register(getBaseContext(), userGcmId);
            registerDevice();
        }
    }

}
