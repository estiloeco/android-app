package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.view.MenuItem;
import br.com.estiloeco.R;
import br.com.estiloeco.model.User;

public class SettingsActivity extends PreferenceActivity {

    private Context context;
    private ActionBar actionBar;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        PreferenceScreen preferenceScreen = (PreferenceScreen) findPreference("preferenceScreen");
        PreferenceCategory preferenceCategoryPersonalData = (PreferenceCategory) findPreference("pref_key_personal_data");

        // Logged user
        user = User.logged(this);

        if (user == null) {
            preferenceScreen.removePreference(preferenceCategoryPersonalData);
        }

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
