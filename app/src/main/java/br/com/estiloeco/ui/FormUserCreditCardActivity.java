package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.CreditCardDataSource;
import br.com.estiloeco.database.CreditCardFlagDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.CreditCard;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;

public class FormUserCreditCardActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private User user;
    private RequestQueue request;
    private CreditCard creditCard;
    private CreditCardDataSource creditCardDataSource;
    private CreditCardFlagDataSource creditCardFlagDataSource;
    private ArrayAdapter adapterFlags;
    private boolean isNew;
    private ProgressDialog progressDialog;
    // components
    private EditText etCreditCardName;
    private EditText etCreditCardNumber;
    private EditText etCreditCardExpiry;
    private Spinner spiCreditCardFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_form_user_credit_card);

        // Context
        context = this;

        // Request
        request = Volley.newRequestQueue(context);

        // User
        user = User.logged(context);

        // Datasource
        creditCardDataSource = CreditCard.datasource(context);
        creditCardFlagDataSource = CreditCardFlag.datasource(context);

        // Current Credit card
        creditCard = new CreditCard();

        // Prepare Actionbar
        prepareActionBar();
        components();
        componentsData();
        componentsActions();
        readOnly();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_user_credit_card, menu);

        if (!isNew) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
            getActionBar().setTitle(getResources().getString(R.string.title_credit_card_final_number) + " " +
                    creditCardFlagDataSource.get(creditCard.getFlagId()).getDescription() + " " +
                    getResources().getString(R.string.title_credit_card_final_number) +
                    creditCard.getNumber().substring(creditCard.getNumber().length()-4, creditCard.getNumber().length()));
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        else if (id == R.id.action_save) {
            if (validation()) {
                CreditCardFlag creditCardFlag = (CreditCardFlag) adapterFlags.getItem(spiCreditCardFlag.getSelectedItemPosition());
                creditCard.setName(etCreditCardName.getText().toString());
                creditCard.setFlagId(creditCardFlag.getId());
                creditCard.setNumber(etCreditCardNumber.getText().toString());
                creditCard.setExpireDate(etCreditCardExpiry.getText().toString());

                saveCreditCard();
            }
        }
        else if (id == R.id.action_remove) {
            final AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources()
                    .getString(R.string.message_sure_credit_card), 0, 0);

            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    removeCreditCard();
                    creditCardDataSource.delete(creditCard.getId());

                    finish();
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        etCreditCardName = (EditText) findViewById(R.id.etCreditCardName);
        etCreditCardNumber = (EditText) findViewById(R.id.etCreditCardNumber);
        spiCreditCardFlag = (Spinner) findViewById(R.id.spiCreditCardFlag);
        etCreditCardExpiry = (EditText) findViewById(R.id.etCreditCardExpiry);
    }

    private void componentsData() {
        ArrayList<CreditCardFlag> creditCardFlags = creditCardFlagDataSource.getAll();

        adapterFlags = new ArrayAdapter<CreditCardFlag>(context,
                R.layout.simple_spinner_item, creditCardFlags);

        adapterFlags.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spiCreditCardFlag.setAdapter(adapterFlags);

        // Extras
        if (getIntent().hasExtra("creditCardId")) {
            creditCard = creditCardDataSource.get(getIntent().getExtras().getLong("creditCardId"));

            if (creditCard.getId() != 0) {
                etCreditCardName.setText(creditCard.getName());
                spiCreditCardFlag.setSelection(adapterFlags.getPosition(creditCard));
                etCreditCardNumber.setText(creditCard.getNumber());
                etCreditCardExpiry.setText(creditCard.getExpireDate());
            }
        }
        else {
            isNew = true;
        }
    }

    private void componentsActions() {
        // Date field
        etCreditCardExpiry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Calendar calendar = Calendar.getInstance();

                    DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            String m = String.valueOf((month + 1));

                            m = m.length() == 1 ? "0" + m : m;

                            etCreditCardExpiry.setText(m + "/" + year);
                        }
                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                    // Remove the day of datepicker
                    int day = context.getResources().getIdentifier("android:id/day", null, null);

                    if (day != 0) {
                        View dayPicker = datePickerDialog.getDatePicker().findViewById(day);
                        if (dayPicker != null) {
                            dayPicker.setVisibility(View.GONE);
                        }
                    }

                    datePickerDialog.show();
                }
            }
        });
    }

    private void readOnly() {
        if (!isNew) {
            etCreditCardName.setEnabled(false);
            spiCreditCardFlag.setEnabled(false);
            etCreditCardNumber.setEnabled(false);
            etCreditCardExpiry.setEnabled(false);
        }
    }

    private boolean validation() {
        boolean status = true;

        if (Helper.isEmpty(etCreditCardName)|| Helper.isEmpty(etCreditCardNumber) ||
                Helper.isEmpty(etCreditCardExpiry)) {

            Toast.makeText(context, R.string.message_error_fields, Toast.LENGTH_SHORT).show();
            status = false;
        }

        return status;
    }

    private void success() {
        // Save credit card
        creditCardDataSource.add(creditCard);

        Toast.makeText(context, R.string.message_credit_card_added, Toast.LENGTH_LONG).show();
        finish();
    }

    private void error() {
        Toast.makeText(context, R.string.message_error_on_save_data, Toast.LENGTH_LONG).show();
    }

    private void removeCreditCard() {
        String url = AppConfig.webserviceUrl("user/credit_card/delete");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("id", String.valueOf(creditCard.getId()));
        params.put("user_id", String.valueOf(user.getId()));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, FormUserCreditCardActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                Toast.makeText(context, R.string.message_error_on_remove_credit_card, Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(context, R.string.message_credit_card_removed, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopProgressBarDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, FormUserCreditCardActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    private void saveCreditCard() {
        String url = AppConfig.webserviceUrl("user/credit_card");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("user_id", String.valueOf(user.getId()));
        params.put("flag_id", String.valueOf(creditCard.getFlagId()));
        params.put("name", creditCard.getName());
        params.put("number", creditCard.getNumber());
        params.put("expiration_date", creditCard.getExpireDate());

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, FormUserCreditCardActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                if (response.get("message").toString().equals("Invalid User")) {
                                    error();
                                }
                            }
                            else {
                                creditCard.setId(response.getLong("credit_card"));
                                success();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopProgressBarDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, FormUserCreditCardActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    private void startProgressDialog() {
        progressDialog = ProgressDialog.show(context, null, getResources().getString(R.string.message_wait), true);
    }

    private void stopProgressBarDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
