package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.CreditCardFlagDataSource;
import br.com.estiloeco.database.PaymentMethodDataSource;
import br.com.estiloeco.database.ShippingTypeDataSource;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.PaymentMethod;
import br.com.estiloeco.model.ShippingType;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;
import br.com.estiloeco.utils.GcmUtil;

public class RegisterActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private ArrayAdapter<String> genderAdapter;
    private boolean isCheckout;
    private RequestQueue request;
    private User user;
    // Components
    private TextView tvErrorMessage;
    private EditText etUserName;
    private EditText etUserLastName;
    private EditText etUserDocument;
    private EditText etUserEmail;
    private EditText etUserPassword;
    private EditText etUserConfirmPassword;
    private CheckBox chkUserOptin;
    private CheckBox chkUserLegalText;
    private Spinner spiUserGender;
    private EditText etUserBirthdate;
    private Button btnContinue;
    private TextView tvUserLegalText;
    private EditText etUserFacebookId;
    private EditText etUserFacebookToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_register);

        // Context
        context = this;

        // Prepare Actionbar
        prepareActionBar();

        // Request
        request = Volley.newRequestQueue(context);

        // Is checkout
        isCheckout = Helper.getBooleanPreference(context, "isCheckout");
        Helper.setPreferences(context, "isCheckout", false).commit();

        // Components
        components();
        componentsData();
        componentsActions();

        // Facebook data
        facebookData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancelAll(AppConfig.TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void facebookData() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.containsKey("userFirstName")) {
                etUserName.setText(extras.getString("userFirstName"));
            }

            if (extras.containsKey("userLastName")) {
                etUserLastName.setText(extras.getString("userLastName"));
            }

            if (extras.containsKey("userGender")) {
                spiUserGender.setSelection(genderAdapter.getPosition((extras.getString("userGender").equals("male") ? "Masculino" : "Feminino")));
            }

            if (extras.containsKey("userEmail")) {
                etUserEmail.setText(extras.getString("userEmail"));
            }

            // Hidden fields
            if (extras.containsKey("facebookId")) {
                etUserFacebookId.setText(extras.getString("facebookId"));
            }

            if (extras.containsKey("facebookAccessToken")) {
                etUserFacebookToken.setText(extras.getString("facebookAccessToken"));
            }
        }
    }

    private void components() {
        tvErrorMessage = (TextView) findViewById(R.id.tvErrorMessage);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etUserLastName = (EditText) findViewById(R.id.etUserLastName);
        etUserDocument = (EditText) findViewById(R.id.etUserDocument);
        etUserEmail = (EditText) findViewById(R.id.etUserEmail);
        etUserPassword = (EditText) findViewById(R.id.etUserPassword);
        etUserConfirmPassword = (EditText) findViewById(R.id.etUserConfirmPassword);
        chkUserOptin = (CheckBox) findViewById(R.id.chkUserOptin);
        chkUserLegalText = (CheckBox) findViewById(R.id.chkUserLegalText);
        spiUserGender = (Spinner) findViewById(R.id.spiUserGender);
        etUserBirthdate = (EditText) findViewById(R.id.etUserBirthdate);
        tvUserLegalText = (TextView) findViewById(R.id.tvUserLegalText);
        etUserFacebookId = (EditText) findViewById(R.id.etUserFacebookId);
        etUserFacebookToken = (EditText) findViewById(R.id.etUserFacebookToken);
        btnContinue = (Button) findViewById(R.id.btnContinue);

        if (isCheckout) {
            etUserDocument.setVisibility(View.VISIBLE);
        }
        else {
            btnContinue.setText(R.string.btn_finish);
        }
    }

    private void componentsData() {
        genderAdapter = new ArrayAdapter<String>(context,
                R.layout.simple_spinner_item, getResources().getStringArray(R.array.genders));
        genderAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spiUserGender.setAdapter(genderAdapter);
    }

    private void componentsActions() {
        // Date field
        etUserBirthdate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Calendar calendar = Calendar.getInstance();
                    Map<String, String> dateParts = Helper.getDateParts(Helper.convertBrDateToSqlDate(etUserBirthdate.getText().toString()), false);
                    int calendarYear = Helper.validString(dateParts.get("year")) ? Integer.parseInt(dateParts.get("year")) : 1997;
                    int calendarMonth = Helper.validString(dateParts.get("month")) ? Integer.parseInt(dateParts.get("month"))-1 : calendar.get(Calendar.MONTH);
                    int calendarDay = Helper.validString(dateParts.get("day")) ? Integer.parseInt(dateParts.get("day")) : calendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            String d = String.valueOf(day);
                            String m = String.valueOf((month + 1));

                            d = d.length() == 1 ? "0" + d : d;
                            m = m.length() == 1 ? "0" + m : m;

                            etUserBirthdate.setText(d + "/" + m + "/" + year);
                        }
                    }, calendarYear, calendarMonth, calendarDay);

                    datePickerDialog.show();
                }
            }
        });

        // Terms of use
        tvUserLegalText.setText(Html.fromHtml(getString(R.string.tv_terms_of_use)));
        tvUserLegalText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LegalActivity.class);
                startActivity(intent);
            }
        });

        // Continue
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    btnContinue.setEnabled(false);

                    user = new User();
                    user.setName(etUserName.getText().toString());
                    user.setLastName(etUserLastName.getText().toString());
                    user.setGender(spiUserGender.getSelectedItem().toString().equals(getResources().getString(R.string.male)) ? "M" : "F");
                    user.setBirthdate(Helper.convertBrDateToSqlDate(etUserBirthdate.getText().toString()));
                    user.setDocument(etUserDocument.getText().toString());
                    user.setEmail(etUserEmail.getText().toString());
                    user.setPassword(AppUtil.generateUserPassword(etUserPassword.getText().toString()));
                    user.setOptIn(chkUserOptin.isChecked());
                    user.setFacebookId(etUserFacebookId.getText().toString());
                    user.setFacebookToken(etUserFacebookToken.getText().toString());

                    if (isCheckout) {
                        ArrayList<String> register = new ArrayList<String>();

                        register.add(user.getName());
                        register.add(user.getLastName());
                        register.add(user.getGender());
                        register.add(user.getBirthdate());
                        register.add(user.getDocument());
                        register.add(user.getEmail());
                        register.add(user.getPassword());
                        register.add(String.valueOf(user.isOptIn()));
                        register.add(user.getFacebookId());
                        register.add(user.getFacebookToken());

                        Intent intent = new Intent(context, RegisterAddressActivity.class);
                        intent.putStringArrayListExtra("register", register);
                        intent.putExtra("isCheckout", true);
                        startActivity(intent);
                    } else {
                        saveUserData();
                    }
                }
            }
        });
    }

    private boolean validation() {
        boolean status = true;

        if (Helper.isEmpty(etUserName) || Helper.isEmpty(etUserLastName) ||
                Helper.isEmpty(etUserBirthdate) || Helper.isEmpty(etUserEmail) ||
                Helper.isEmpty(etUserPassword) || Helper.isEmpty(etUserConfirmPassword)) {

            Toast.makeText(context, R.string.message_error_fields, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (isCheckout && Helper.isEmpty(etUserDocument)) {
            Toast.makeText(context, R.string.message_error_fields, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (!Helper.isValidEmail(etUserEmail.getText().toString())) {
            Toast.makeText(context, R.string.message_invalid_email, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (!etUserPassword.getText().toString().equals(etUserConfirmPassword.getText().toString())) {
            Toast.makeText(context, R.string.message_password_do_not_match, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (etUserPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(context, R.string.message_short_password, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (!chkUserLegalText.isChecked()) {
            Toast.makeText(context, R.string.message_legal_terms, Toast.LENGTH_SHORT).show();
            status = false;
        }


        if (!status) {
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
        else {
            tvErrorMessage.setVisibility(View.GONE);
        }

        return status;
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void success() {
        // Save user
        UserDataSource userDatasource = User.datasource(context);
        userDatasource.add(user);

        Intent intent = new Intent(context, RegisterSuccessActivity.class);
        intent.putExtra("userEmail", user.getEmail());

        if (isCheckout) {
            intent.putExtra("isCheckout", true);
        }

        startActivity(intent);
    }

    private void error() {
        Toast.makeText(context, R.string.message_user_already_exists, Toast.LENGTH_LONG).show();
    }

    private void loadSystemData(JSONObject system) {
        // Payment methods
        PaymentMethodDataSource paymentMethodDataSource = PaymentMethod.datasource(context);

        try {
            JSONArray paymentMethodsArray = system.getJSONArray("payment_methods");

            for (int i = 0; i < paymentMethodsArray.length(); i++) {
                JSONObject paymentMethodObject = paymentMethodsArray.getJSONObject(i);

                PaymentMethod paymentMethod = new PaymentMethod();
                paymentMethod.setId(paymentMethodObject.getLong("id"));
                paymentMethod.setDescription(paymentMethodObject.getString("description"));
                paymentMethod.setTag(paymentMethodObject.getString("tag"));

                paymentMethodDataSource.add(paymentMethod);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Credit Card Flags
        CreditCardFlagDataSource creditCardFlagDataSource = CreditCardFlag.datasource(context);

        try {
            JSONArray creditCardFlagsArray = system.getJSONArray("credit_card_flags");

            for (int i = 0; i < creditCardFlagsArray.length(); i++) {
                JSONObject creditCardFlagObject = creditCardFlagsArray.getJSONObject(i);

                CreditCardFlag creditCardFlag = new CreditCardFlag();
                creditCardFlag.setId(creditCardFlagObject.getLong("id"));
                creditCardFlag.setDescription(creditCardFlagObject.getString("description"));

                creditCardFlagDataSource.add(creditCardFlag);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Shipping types
        ShippingTypeDataSource shippingTypeDataSource = ShippingType.datasource(context);

        try {
            JSONArray shippingTypesArray = system.getJSONArray("shipping_types");

            for (int i = 0; i < shippingTypesArray.length(); i++) {
                JSONObject shippingTypeObject = shippingTypesArray.getJSONObject(i);

                ShippingType shippingType= new ShippingType();
                shippingType.setId(shippingTypeObject.getLong("id"));
                shippingType.setDescription(shippingTypeObject.getString("description"));
                shippingType.setTag(shippingTypeObject.getString("tag"));

                shippingTypeDataSource.add(shippingType);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveUserData() {
        String url = AppConfig.webserviceUrl("user");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("name", user.getName());
        params.put("last_name", user.getLastName());
        params.put("gender", user.getGender());
        params.put("birthdate", user.getBirthdate());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());
        params.put("optin", (user.isOptIn() ? "1" : "0"));
        params.put("facebookId", user.getFacebookId());
        params.put("facebookToken", user.getFacebookToken());
        params.put("legal_text_accepted", "1");
        params.put("type", AppConfig.DEVICE_TYPE);
        params.put("gcm_id", GcmUtil.getId(getBaseContext()));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, RegisterActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                if (response.get("message").toString().equals("User already exists")) {
                                    error();
                                }
                            }
                            else {
                                user.setId(Long.valueOf(response.get("user").toString()));
                                loadSystemData(response.getJSONObject("system"));
                                success();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopLoading();
                        btnContinue.setEnabled(true);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, RegisterActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(AppConfig.WEBSERVICE_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }
}
