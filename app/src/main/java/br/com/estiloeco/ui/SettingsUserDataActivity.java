package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.User;
import br.com.estiloeco.utils.AppUtil;
import br.com.estiloeco.utils.GcmUtil;

public class SettingsUserDataActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private User user;
    private RequestQueue request;
    // Components
    private EditText etUserName;
    private EditText etUserLastName;
    private EditText etUserBirthdate;
    private EditText etUserDocument;
    private EditText etUserEmail;
    private Spinner spiUserGender;
    private EditText etUserPassword;
    private EditText etUserConfirmPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_settings_user_data);

        // Context
        context = this;

        // Request
        request = Volley.newRequestQueue(context);

        // User
        user = User.logged(context);

        // Prepare Actionbar
        prepareActionBar();

        // Components
        components();
        componentsData();
        componentsActions();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_user_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (user.getName().equals(etUserName.getText().toString()) && user.getLastName().equals(etUserLastName.getText().toString()) &&
                    user.getBirthdate().equals(Helper.convertBrDateToSqlDate(etUserBirthdate.getText().toString())) && user.getDocument().equals(etUserDocument.getText().toString()) &&
                    user.getGender().equals((spiUserGender.getSelectedItem().equals(getResources().getString(R.string.male)) ? "M" : "F"))) {
                finish();
            }
            else {
                final AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources().getString(R.string.message_save_updated_data), 0, 0);

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        finish();
                    }
                });
            }
        }
        else if (id == R.id.action_save) {
            if (Helper.validString(etUserPassword.getText().toString()) &&
                    !etUserPassword.getText().toString().equals(etUserConfirmPassword.getText().toString())) {

                Toast.makeText(context, R.string.message_password_do_not_match, Toast.LENGTH_SHORT).show();
            }
            else if(validation()) {
                user.setName(etUserName.getText().toString());
                user.setLastName(etUserLastName.getText().toString());
                user.setGender(spiUserGender.getSelectedItem().toString().equals(getResources().getString(R.string.male)) ? "M" : "F");
                user.setBirthdate(Helper.convertBrDateToSqlDate(etUserBirthdate.getText().toString()));
                user.setDocument(etUserDocument.getText().toString());

                if (Helper.validString(etUserPassword.getText().toString())) {
                    user.setPassword(AppUtil.generateUserPassword(etUserPassword.getText().toString()));
                }

                saveUserData();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        etUserName = (EditText) findViewById(R.id.etUserName);
        etUserLastName = (EditText) findViewById(R.id.etUserLastName);
        etUserDocument = (EditText) findViewById(R.id.etUserDocument);
        etUserEmail = (EditText) findViewById(R.id.etUserEmail);
        etUserBirthdate = (EditText) findViewById(R.id.etUserBirthdate);
        spiUserGender = (Spinner) findViewById(R.id.spiUserGender);
        etUserPassword = (EditText) findViewById(R.id.etUserPassword);
        etUserConfirmPassword = (EditText) findViewById(R.id.etUserConfirmPassword);
    }

    private void componentsData() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                R.layout.simple_spinner_item, getResources().getStringArray(R.array.genders));
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spiUserGender.setAdapter(adapter);

        // User data
        if (user != null) {
            etUserName.setText(user.getName());
            etUserLastName.setText(user.getLastName());
            etUserBirthdate.setText(Helper.convertSqlDateToBrDate(user.getBirthdate()));
            etUserDocument.setText(user.getDocument());
            etUserEmail.setText(user.getEmail());
            spiUserGender.setSelection(adapter.getPosition(user.getGender().equals("M") ?
                    getResources().getString(R.string.male) : getResources().getString(R.string.female)));
        }
    }

    private void componentsActions() {
        // Date field
        etUserBirthdate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Calendar calendar = Calendar.getInstance();
                    Map<String, String> dateParts = Helper.getDateParts(Helper.convertBrDateToSqlDate(etUserBirthdate.getText().toString()), false);
                    int calendarYear = Helper.validString(dateParts.get("year")) ? Integer.parseInt(dateParts.get("year")) : 1997;
                    int calendarMonth = Helper.validString(dateParts.get("month")) ? Integer.parseInt(dateParts.get("month"))-1 : calendar.get(Calendar.MONTH);
                    int calendarDay = Helper.validString(dateParts.get("day")) ? Integer.parseInt(dateParts.get("day")) : calendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener(){

                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            String d = String.valueOf(day);
                            String m = String.valueOf((month+1));

                            d = d.length() == 1 ? "0" + d : d;
                            m = m.length() == 1 ? "0" + m : m;

                            etUserBirthdate.setText(d + "/" + m + "/" + year);
                        }
                    }, calendarYear, calendarMonth, calendarDay);

                    datePickerDialog.show();
                }
            }
        });
    }

    private boolean validation() {
        boolean status = true;

        if (Helper.isEmpty(etUserName) || Helper.isEmpty(etUserLastName) || Helper.isEmpty(etUserBirthdate) ||
                Helper.isEmpty(etUserDocument)) {

            Toast.makeText(context, R.string.message_error_fields, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (!Helper.isEmpty(etUserPassword) && etUserPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(context, R.string.message_short_password, Toast.LENGTH_SHORT).show();
            status = false;
        }

        return status;
    }

    private void success() {
        // Save user
        UserDataSource userDatasource = User.datasource(context);
        userDatasource.updateAllInfo(user);

        Toast.makeText(context, R.string.message_data_updated, Toast.LENGTH_LONG).show();
    }
    private void error() {
        Toast.makeText(context, R.string.message_error_on_save_data, Toast.LENGTH_LONG).show();
    }

    private void saveUserData() {
        String url = AppConfig.webserviceUrl("user/update");
        Map<String, String> params = new HashMap<String, String>();

        // User data
        params.put("id", String.valueOf(user.getId()));
        params.put("name", user.getName());
        params.put("last_name", user.getLastName());
        params.put("gender", user.getGender());
        params.put("birthdate", user.getBirthdate());
        params.put("document", user.getDocument());
        params.put("type", AppConfig.DEVICE_TYPE);
        params.put("gcm_id", GcmUtil.getId(getBaseContext()));

        if (user.getPassword() != null) {
            params.put("password", user.getPassword());
        }

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, SettingsUserDataActivity.this.getLocalClassName() + " - " +
                                        response.get("message").toString());

                                if (response.get("message").toString().equals("Invalid User")) {
                                    error();
                                }
                            }
                            else {
                                success();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, SettingsUserDataActivity.this.getLocalClassName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
