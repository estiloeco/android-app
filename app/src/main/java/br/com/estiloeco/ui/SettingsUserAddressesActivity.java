package br.com.estiloeco.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.AddressAdapter;
import br.com.estiloeco.database.AddressDataSource;
import br.com.estiloeco.model.Address;

public class SettingsUserAddressesActivity extends Activity {

    private Context context;
    private ActionBar actionBar;
    private AddressAdapter addressAdapter;
    private AddressDataSource addressDataSource;
    // Components
    private TextView tvNoAddresses;
    private ListView lvAddresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_user_addresses);

        // Context
        context = this;

        // Datasource
        addressDataSource = Address.datasource(context);

        // Prepare Actionbar
        prepareActionBar();
        components();
    }

    @Override
    protected void onResume() {
        super.onResume();
        componentsData();
        componentsActions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_user_addresses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        else if (id == R.id.action_add_address) {
            Intent intent = new Intent(context, FormUserAddressActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void components() {
        tvNoAddresses = (TextView) findViewById(R.id.tvNoAddresses);
        lvAddresses = (ListView) findViewById(R.id.lvAddresses);
    }

    private void componentsData() {
        ArrayList<Address> list = addressDataSource.getAll();

        if (list.size() > 0) {
            lvAddresses.setVisibility(View.VISIBLE);
            tvNoAddresses.setVisibility(View.INVISIBLE);
        }
        else {
            lvAddresses.setVisibility(View.GONE);
            tvNoAddresses.setVisibility(View.VISIBLE);
        }

        addressAdapter = new AddressAdapter(context, list);
        lvAddresses.setAdapter(addressAdapter);
    }

    private void componentsActions() {
        lvAddresses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(context, FormUserAddressActivity.class);
                intent.putExtra("addressId", addressAdapter.getItem(i).getId());
                startActivity(intent);
            }
        });
    }

    private void prepareActionBar() {
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
