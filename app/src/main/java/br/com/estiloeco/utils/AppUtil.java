package br.com.estiloeco.utils;

import android.app.AlertDialog;
import android.content.Context;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.helpers.Helper;

/**
 * Created by paulomartins on 31/03/15.
 */
public class AppUtil {

    static public AlertDialog confirmDialog(Context context, String message, int negativeButton, int positiveButton) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        final AlertDialog alertDialog = alertDialogBuilder
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton((positiveButton != 0 ? positiveButton : R.string.btn_yes), null)
                .setNegativeButton((negativeButton != 0 ? negativeButton : R.string.btn_no), null)
                .create();

        alertDialog.show();

        return alertDialog;
    }

    static public String generateUserPassword(String password)
    {
        return Helper.md5(AppConfig.SECURITY_KEY + password + AppConfig.SECURITY_KEY);
    }

}
