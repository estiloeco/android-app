package br.com.estiloeco.utils;

import android.content.Context;

import br.com.estiloeco.helpers.Helper;

/**
 * Created by paulomartins on 06/02/15.
 */
public class GcmUtil {

    static public void register(Context context, String userGcmId) {
        Helper.setPreferences(context, "userGcmId", userGcmId).commit();
        Helper.setPreferences(context, "appVersion", Helper.appVersion(context)).commit();
    }

    static public String getId(Context context) {
        String userGcmId = Helper.getStringPreference(context, "userGcmId");

        if (userGcmId == null || userGcmId.trim().length() == 0) {
            return null;
        }

        int oldAppVersion = Helper.getIntPreference(context, "appVersion");
        int currentAppVersion = Helper.appVersion(context);

        if (oldAppVersion != currentAppVersion) {
            return null;
        }

        return userGcmId;
    }

}
