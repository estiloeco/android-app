package br.com.estiloeco.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import br.com.estiloeco.R;
import br.com.estiloeco.ui.MainActivity;

/**
 * Created by paulomartins on 24/02/15.
 */
public class AndroidNotificationUtil {

    static public int NEW_FRIEND = 1;
    static public int FRIEND_ACCEPT = 2;
    static public int SHARE_LOOK = 3;

    static public void sendNotitification(Context context, String title, String message, int notificationId, Intent intent) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon_brand)
                .setContentTitle(title)
//                .setColor(context.getResources().getColor(R.color.principal))
                .setContentText(message);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        android.app.Notification notification = builder.build();
        notification.flags |= android.app.Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(notificationId, notification);
    }

    static public void cancel(Context context, int id) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
    }

}
