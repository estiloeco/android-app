package br.com.estiloeco.helpers;

import java.text.NumberFormat;

/**
 * Created by paulomartins on 14/01/15.
 */
public class NumberFormats {

    public static float toFloat(String number) {
        String str1 = number.substring(0, number.length()-2);
        String str2 = number.substring(number.length()-2, number.length());

        return Float.parseFloat(str1 + "." + str2);
    }

    public static String toMoney(float number) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setMinimumFractionDigits(2);

        return numberFormat.format(number);
    }

}
