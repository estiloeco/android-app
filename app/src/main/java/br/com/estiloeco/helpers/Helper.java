package br.com.estiloeco.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.ViewConfiguration;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.picasso.Transformation;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;

/**
 * Created by paulomartins on 29/01/15.
 */
public class Helper {

    static public SharedPreferences.Editor setPreferences(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(key, value);

        return editor;
    }

    static public SharedPreferences.Editor setPreferences(Context context, String key, boolean value) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(key, value);

        return editor;
    }

    static public SharedPreferences.Editor setPreferences(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(key, value);

        return editor;
    }

    static public SharedPreferences.Editor setPreferences(Context context, String key, long value) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(key, value);

        return editor;
    }

    static public boolean getBooleanPreference(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    static public String getStringPreference(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        return preferences.getString(key, null);
    }

    static public int getIntPreference(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        return preferences.getInt(key, 0);
    }

    static public long getLongPreference(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        return preferences.getLong(key, 0);
    }

    static public void buttonMenuBugFix(Context context) {
        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (Exception e) {
            Log.e(AppConfig.TAG, "Menu bugfix - " + e.getMessage());
        }
    }

    static public boolean isEmpty(EditText editText) {
        return TextUtils.isEmpty(editText.getText().toString());
    }

    static public boolean validString(String text) {
        if (text == null) {
            return false;
        }
        else if (text.trim().length() == 0 || text.equals("NULL") || text.equals("null")) {
            return false;
        }

        return true;
    }

    static public String convertImageToBase64(String path){
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(path);

            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;


            ByteArrayOutputStream output = new ByteArrayOutputStream();

            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            bytes = output.toByteArray();
            String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);

            return TextUtils.htmlEncode(encodedString);
        } catch (FileNotFoundException e1) {

            e1.printStackTrace();
        }
        finally{
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    static public String convertBitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
        byte[] bytes = output.toByteArray();
        String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);

        return encodedString;
    }

    static public String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());

                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);

                        if (useIPv4 && isIPv4) {
                            return sAddr;
                        } else if (!useIPv4 && !isIPv4) {
                            int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                            return delim<0 ? sAddr : sAddr.substring(0, delim);
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions

        return "";
    }

    static public boolean checkPlayServices(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        AppConfig.GOOGLE_PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }
        return true;
    }

    static public int rand() {
        Random rand = new Random();
        return rand.nextInt((50000 - 0) + 1) + 0;
    }

    static public int appVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {}


        return 0;
    }

    static public String getDatetime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        Date date = new Date();

        return dateFormat.format(date);
    }

    static public String convertSqlDateToBrDate(String date) {
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        return day + "/" + month + "/" + year;
    }

    static public String convertBrDateToSqlDate(String date) {
        if (date.length() > 0) {
            String day = date.substring(0, 2);
            String month = date.substring(3, 5);
            String year = date.substring(6, 10);

            return year + "-" + month + "-" + day;
        }
        else {
            return null;
        }
    }

    static public String convertSqlDateToString(String date, boolean smarty, Context context) {
        Map<String, String> dateParts = getDateParts(date, true);
        Map<String, String> currentDateParts = getDateParts(getDatetime(), true);

        if (smarty) {
            if (dateParts.get("day").equals(currentDateParts.get("day")) && dateParts.get("month").equals(currentDateParts.get("month"))
                    && dateParts.get("year").equals(currentDateParts.get("year"))) {

                return context.getResources().getString(R.string.label_today) + " " +
                        context.getResources().getString(R.string.label_at) + " " + dateParts.get("hour");
            }
            else if ((Integer.parseInt(dateParts.get("day")) == Integer.parseInt(currentDateParts.get("day"))-1)
                    && dateParts.get("month").equals(currentDateParts.get("month")) && dateParts.get("year").equals(currentDateParts.get("year"))) {

                return context.getResources().getString(R.string.label_yesterday) + " " + dateParts.get("hour");
            }
        }

        return dateParts.get("day") + "/" + dateParts.get("month") + "/" +
                dateParts.get("year") + " " + dateParts.get("hour");
    }

    public static Map<String, String> getDateParts(String date, boolean withHour) {
        HashMap<String, String> parts = new HashMap<String, String>();
        String year = null;
        String month = null;
        String day = null;
        String hour = null;

        if (date != null) {
            year = date.substring(0, 4);
            month = date.substring(5, 7);
            day = date.substring(8, 10);

            if (withHour) {
                hour = date.substring(11, 16);
            }
        }

        parts.put("year", year);
        parts.put("month", month);
        parts.put("day", day);

        if (withHour) {
            parts.put("hour", hour);
        }

        return parts;
    }

    public static boolean isValidEmail(String email) {
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return false;
        }

        return true;
    }

    public static String md5(String text) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(text.getBytes(), 0, text.length());

            return new BigInteger(1, messageDigest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            Log.e(AppConfig.TAG, "Helper - " + e.getMessage());
        }

        return null;
    }

    public static String finalString(String text, int amount) {
        return text.substring(text.length()-amount, text.length());
    }

    public class CircleTransform implements Transformation {

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size/2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();

            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

}
