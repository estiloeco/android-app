package br.com.estiloeco.model;

import android.content.Context;
import android.util.Log;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.database.ItemCartDataSource;

/**
 * Created by paulomartins on 10/10/14.
 */
public class ItemCart {
    private String brand;
    private long productId;
    private String product;
    private int sku;
    private String size;
    private int amount;
    private String image;
    private float cost;

    public ItemCart() {
    }

    public ItemCart(String brand, long productId, String product, int sku, String size, int amount, String image, float cost) {
        this.brand = brand;
        this.productId = productId;
        this.product = product;
        this.sku = sku;
        this.size = size;
        this.amount = amount;
        this.image = image;
        this.cost = cost;
    }

    public String getBrand() {
        return brand;
    }

    public long getProductId() {
        return productId;
    }

    public String getProduct() {
        return product;
    }

    public int getSku() {
        return sku;
    }

    public String getSize() {
        return size;
    }

    public int getAmount() {
        return amount;
    }

    public String getImage() {
        return image;
    }

    public float getCost() {
        return cost;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void setSku(int sku) {
        this.sku = sku;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public static ItemCartDataSource datasource(Context context) {
        ItemCartDataSource datasource = new ItemCartDataSource(context);

        try {
            datasource.open();
        }
        catch (Exception e) {
            Log.e(AppConfig.TAG + " - Datasource", e.getMessage());
        }

        return datasource;
    }
}
