package br.com.estiloeco.model;

/**
 * Created by paulomartins on 07/11/14.
 */
public class OrderStatus {

    private String title;
    private String description;

    public OrderStatus() {
    }

    public OrderStatus(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
