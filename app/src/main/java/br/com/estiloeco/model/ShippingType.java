package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.ShippingTypeDataSource;

/**
 * Created by paulomartins on 14/04/15.
 */
public class ShippingType {

    private long id;
    private String description;
    private String tag;

    public ShippingType() {
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getShortDescription() {
        return description.substring(0, description.length()-8);
    }

    public String getTag() {
        return tag;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    static public ShippingTypeDataSource datasource(Context context) {
        return new ShippingTypeDataSource(context);
    }
}
