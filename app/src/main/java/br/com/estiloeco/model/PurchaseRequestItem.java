package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.PurchaseRequestItemDataSource;

/**
 * Created by paulomartins on 14/04/15.
 */
public class PurchaseRequestItem {

    private long id;
    private long purchase_request;
    private long sku;
    private int amount;
    private float cost;

    public PurchaseRequestItem() {
    }

    public long getId() {
        return id;
    }

    public long getPurchaseRequest() {
        return purchase_request;
    }

    public long getSku() {
        return sku;
    }

    public int getAmount() {
        return amount;
    }

    public float getCost() {
        return cost;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPurchaseRequest(long purchase_request_id) {
        this.purchase_request = purchase_request_id;
    }

    public void setSku(long sku) {
        this.sku = sku;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    static public PurchaseRequestItemDataSource datasource(Context context) {
        return new PurchaseRequestItemDataSource(context);
    }
}
