package br.com.estiloeco.model;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;

/**
 * Created by paulomartins on 29/01/15.
 */
public class User {

    private long id;
    private String name;
    private String lastName;
    private String gender;
    private String birthdate;
    private String document;
    private String email;
    private String password;
    private String avatar;
    private String facebookId;
    private String facebookToken;
    private int friendsCount;
    private int notificationsCount;
    private boolean optIn;
    private boolean logged;

    public User() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getDocument() {
        return document;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public int getNotificationsCount() {
        return notificationsCount;
    }

    public boolean isOptIn() {
        return optIn;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public void setNotificationsCount(int notificationsCount) {
        this.notificationsCount = notificationsCount;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setOptIn(boolean optIn) {
        this.optIn = optIn;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", document='" + document + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", avatar='" + avatar + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", facebookToken='" + facebookToken + '\'' +
                ", friendsAmount=" + friendsCount +
                ", notificationsAmount=" + notificationsCount +
                ", optIn=" + optIn +
                ", logged=" + logged +
                '}';
    }

    static public UserDataSource datasource(Context context) {
        return new UserDataSource(context);
    }

    static public User logged(Context context) {
        User user = new UserDataSource(context).get();

        if (user != null) {
            user.setLogged(true);
        }

        return user;
    }
}
