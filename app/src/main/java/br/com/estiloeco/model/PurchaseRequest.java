package br.com.estiloeco.model;

import android.content.Context;

import java.util.ArrayList;

import br.com.estiloeco.database.PurchaseRequestDataSource;

/**
 * Created by paulomartins on 14/04/15.
 */
public class PurchaseRequest {

    private long id;
    private long payment_method;
    private long credit_card;
    private long shipping_type;
    private long address;
    private int credit_card_code;
    private String bank_bill_user_email;
    private String bank_bill_number;
    private float cost;
    private float shipping_cost;
    private String date;
    private ArrayList<PurchaseRequestItem> items;
    private ArrayList<PurchaseRequestStatus> status;

    public PurchaseRequest() {
    }

    public long getId() {
        return id;
    }

    public long getPaymentMethod() {
        return payment_method;
    }

    public long getCreditCard() {
        return credit_card;
    }

    public long getShippingType() {
        return shipping_type;
    }

    public long getAddress() {
        return address;
    }

    public int getCreditCardCode() {
        return credit_card_code;
    }

    public String getBankBillUserEmail() {
        return bank_bill_user_email;
    }

    public String getBankBillNumber() {
        return bank_bill_number;
    }

    public float getCost() {
        return cost;
    }

    public float getShippingCost() {
        return shipping_cost;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<PurchaseRequestItem> getItems() {
        return items;
    }

    public ArrayList<PurchaseRequestStatus> getStatus() {
        return status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPaymentMethod(long payment_method) {
        this.payment_method = payment_method;
    }

    public void setCreditCard(long credit_card) {
        this.credit_card = credit_card;
    }

    public void setShippingType(long shipping_type) {
        this.shipping_type = shipping_type;
    }

    public void setAddress(long address) {
        this.address = address;
    }

    public void setCreditCardCode(int credit_card_code) {
        this.credit_card_code = credit_card_code;
    }

    public void setBankBillUserEmail(String bank_bill_user_email) {
        this.bank_bill_user_email = bank_bill_user_email;
    }

    public void setBankBillNumber(String bank_bill_number) {
        this.bank_bill_number = bank_bill_number;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setShippingCost(float shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setItems(ArrayList<PurchaseRequestItem> items) {
        this.items = items;
    }

    public void setStatus(ArrayList<PurchaseRequestStatus> status) {
        this.status = status;
    }

    static public PurchaseRequestDataSource datasource(Context context) {
        return new PurchaseRequestDataSource(context);
    }
}
