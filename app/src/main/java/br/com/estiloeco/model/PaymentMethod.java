package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.PaymentMethodDataSource;

/**
 * Created by paulomartins on 14/04/15.
 */
public class PaymentMethod {

    private long id;
    private String description;
    private String tag;
    private boolean checked;

    public PaymentMethod() {
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getTag() {
        return tag;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    static public PaymentMethodDataSource datasource(Context context) {
        return new PaymentMethodDataSource(context);
    }
}
