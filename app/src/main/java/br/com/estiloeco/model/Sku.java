package br.com.estiloeco.model;

import java.util.List;

/**
 * Created by paulomartins on 14/01/15.
 */
public class Sku {

    private int id;
    private String size;
    private String color;

    public Sku() {
    }

    public Sku(int id, String size, String color) {
        this.id = id;
        this.size = size;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
