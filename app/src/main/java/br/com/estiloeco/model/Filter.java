package br.com.estiloeco.model;

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;

import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.database.PersonalFiltersDataSource;

/**
 * Created by paulomartins on 16/01/15.
 */
public class Filter {

    private long id;
    private String name;
    private boolean checked;

    public Filter() {}

    public Filter(long id, String name, boolean checked) {
        this.id = id;
        this.name = name;
        this.checked = checked;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PersonalFiltersDataSource datasource(Context context) {
        return new PersonalFiltersDataSource(context);
    }
}
