package br.com.estiloeco.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paulomartins on 10/10/14.
 */
public class Product {
    private long id;
    private String name;
    private String description;
    private String brand;
    private String image;
    private String imageShoppingCart;
    private boolean available;
    private float cost;
    private List<Sku> skus;

    public Product() {
    }

    public Product(long id, String name, String description, String brand, String image, String imageShoppingCart, boolean available, float cost, List<Sku> skus) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.brand = brand;
        this.image = image;
        this.imageShoppingCart = imageShoppingCart;
        this.available = available;
        this.cost = cost;
        this.skus = skus;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBrand() {
        return brand;
    }

    public String getImage() {
        return image;
    }

    public String getImageShoppingCart() {
        return imageShoppingCart;
    }

    public boolean isAvailable() {
        return available;
    }

    public float getCost() {
        return cost;
    }

    public List<Sku> getSkus() {
        return skus;
    }

    public List<String> getSizes() {
        List<String> sizes = new ArrayList<String>();

        if (skus != null && skus.size() > 0) {
            for (int i = 0; i < skus.size(); i++) {
                sizes.add(skus.get(i).getSize());
            }
        }

        return sizes;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setImageShoppingCart(String imageShoppingCart) {
        this.imageShoppingCart = imageShoppingCart;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setSkus(List<Sku> skus) {
        this.skus = skus;
    }
}
