package br.com.estiloeco.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paulomartins on 03/10/14.
 */
public class Look {

    private long id;
    private int likes;
    private boolean userLike;
    private ArrayList<String> images;
    private ArrayList<Product> products;
    private ArrayList<Friend> friends;

    public Look() {}

    public Look(long id, ArrayList<String> images, int likes, ArrayList<Product> products) {
        this.id = id;
        this.images = images;
        this.likes = likes;
        this.products = products;
    }

    public long getId() {
        return id;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public String getImage(int i) {
        String image = null;

        if (i <= images.size()) {
            image = images.get(i);
        }

        return image;
    }

    public int getLikes() {
        return likes;
    }

    public boolean userLike() {
        return userLike;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public Product getProduct(int i) {
        Product product = null;

        if (i <= products.size()) {
            product = products.get(i);
        }

        return product;
    }

    public ArrayList<Friend> getFriends() {
        return friends;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public void setUserLike(boolean userLike) {
        this.userLike = userLike;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public void setFriends(ArrayList<Friend> friends) {
        this.friends = friends;
    }
}
