package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.CreditCardFlagDataSource;

/**
 * Created by paulomartins on 14/04/15.
 */
public class CreditCardFlag {

    private long id;
    private String description;

    public CreditCardFlag() {
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    static public CreditCardFlagDataSource datasource(Context context) {
        return new CreditCardFlagDataSource(context);
    }

    @Override
    public String toString() {
        return description;
    }
}
