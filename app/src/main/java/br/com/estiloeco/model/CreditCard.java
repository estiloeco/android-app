package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.CreditCardDataSource;

/**
 * Created by paulomartins on 17/10/14.
 */
public class CreditCard {

    private long id;
    private long flag_id;
    private String name;
    private String number;
    private String expire_date;

    public CreditCard() {
    }

    public CreditCard(long id, long flag_id, String name, String number, String expire_date) {
        this.id = id;
        this.flag_id = flag_id;
        this.name = name;
        this.number = number;
        this.expire_date = expire_date;
    }

    public long getId() {
        return id;
    }

    public long getFlagId() {
        return flag_id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getExpireDate() {
        return expire_date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFlagId(long flag_id) {
        this.flag_id = flag_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setExpireDate(String expire_date) {
        this.expire_date = expire_date;
    }

    static public CreditCardDataSource datasource(Context context) {
        return new CreditCardDataSource(context);
    }
}
