package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.FriendsDataSource;

/**
 * Created by paulomartins on 29/10/14.
 */
public class Friend {

    private long id;
    private String name;
    private String avatar;
    private int status;
    // Constants
    static public int STATUS_ACCEPTED = 1;
    static public int STATUS_WAITING_FRIEND = 2;
    static public int STATUS_WAITING_YOU = 3;
    static public int STATUS_NON_FRIEND = 0;

    public Friend() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getStatus() {
        return status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    static public FriendsDataSource datasource(Context context) {
        return new FriendsDataSource(context);
    }
}
