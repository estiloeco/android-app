package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.NotificationsDataSource;

/**
 * Created by paulomartins on 22/11/14.
 */
public class Notification {

    private long id;
    private String type;
    private long user_id;
    private String user_name;
    private String user_avatar;
    private String message;
    private String date;
    private int status;
    // Constant
    public static int READ = 1;
    public static int UNREAD = 0;

    public Notification() {
    }

    public Notification(String type, long user_id, String user_name, String user_avatar, String message, String date, int status) {
        this.type = type;
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_avatar = user_avatar;
        this.message = message;
        this.date = date;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public long getUserId() {
        return user_id;
    }

    public String getUserName() {
        return user_name;
    }

    public String getUserAvatar() {
        return user_avatar;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public int getStatus() {
        return status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUserId(long user_id) {
        this.user_id = user_id;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public void setUserAvatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    static public NotificationsDataSource datasource(Context context) {
        return new NotificationsDataSource(context);
    }
}
