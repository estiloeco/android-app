package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.AddressDataSource;

/**
 * Created by paulomartins on 17/10/14.
 */
public class Address {

    private long id;
    private String name;
    private String cep;
    private String street;
    private String number;
    private String complement;
    private String neighborhood;
    private String city;
    private String state;
    private String country;
    private String obs;
    private boolean principal;

    public Address() {
    }

    public Address(long id, String name, String cep, String street, String number, String complement,
                   String neighborhood, String city, String state, String country, String obs, boolean principal) {

        this.id = id;
        this.name = name;
        this.cep = cep;
        this.street = street;
        this.number = number;
        this.complement = complement;
        this.neighborhood = neighborhood;
        this.city = city;
        this.state = state;
        this.country = country;
        this.obs = obs;
        this.principal = principal;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCep() {
        return cep;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getComplement() {
        return complement;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getObs() {
        return obs;
    }

    public boolean isPrincipal() {
        return principal;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    static public AddressDataSource datasource(Context context) {
        return new AddressDataSource(context);
    }
}
