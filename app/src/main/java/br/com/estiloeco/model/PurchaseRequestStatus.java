package br.com.estiloeco.model;

import android.content.Context;

import br.com.estiloeco.database.PurchaseRequestStatusDataSource;

/**
 * Created by paulomartins on 15/04/15.
 */
public class PurchaseRequestStatus {

    private long id;
    private long purchase_request;
    private String title;
    private String description;
    private String date;

    public PurchaseRequestStatus() {
    }

    public long getId() {
        return id;
    }

    public long getPurchaseRequest() {
        return purchase_request;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPurchaseRequest(long purchase_request) {
        this.purchase_request = purchase_request;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(String date) {
        this.date = date;
    }

    static public PurchaseRequestStatusDataSource datasource(Context context) {
        return new PurchaseRequestStatusDataSource(context);
    }
}

