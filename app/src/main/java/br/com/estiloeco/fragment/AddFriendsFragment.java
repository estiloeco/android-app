package br.com.estiloeco.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.AddFriendsAdapter;
import br.com.estiloeco.adapter.FriendsAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.User;

public class AddFriendsFragment extends Fragment {

    private Context context;
    private RequestQueue request;
    private View view;
    private AddFriendsAdapter addFriendsAdapter;
    private User user;
    // Components
    private EditText etSearchFriend;
    private ListView lvAddFriends;
    private LinearLayout llSearch;
    private Button btnSearch;
    private Button btnInvite;
    private TextView tvMessage;
    private TextView tvFriendInvited;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_friends, container, false);

        context = getActivity();

        // Request
        request = Volley.newRequestQueue(context);

        // User
        user = User.logged(context);

        // Friends
        addFriendsAdapter = new AddFriendsAdapter(context, user.getId(), getActivity());

        components();
        componentsData();
        componentsActions();

        return view;
    }

    private void components() {
        etSearchFriend = (EditText) view.findViewById(R.id.etSearchFriend);
        lvAddFriends = (ListView) view.findViewById(R.id.lvFriends);
        llSearch = (LinearLayout) view.findViewById(R.id.llSearch);
        btnSearch = (Button) view.findViewById(R.id.btnSearch);
        btnInvite = (Button) view.findViewById(R.id.btnInvite);
        tvFriendInvited = (TextView) view.findViewById(R.id.tvFriendInvited);
        tvMessage = (TextView) view.findViewById(R.id.tvMessage);

        lvAddFriends.setAdapter(addFriendsAdapter);
    }

    private void componentsData() {

    }

    private void componentsActions() {
        etSearchFriend.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER) && !Helper.isEmpty(etSearchFriend)) {
                    search();
                    return true;
                }

                return false;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Helper.isEmpty(etSearchFriend)) {
                    tvFriendInvited.setText("");
                    tvFriendInvited.setVisibility(View.GONE);

                    search();
                }
            }
        });

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Helper.isValidEmail(etSearchFriend.getText().toString())) {
                    Toast.makeText(context, R.string.message_invalid_email, Toast.LENGTH_SHORT).show();
                }
                else {
                    invite();
                }
            }
        });
    }

    private void populateFriendsList(JSONObject data) {
        List<Friend> friends = new ArrayList<Friend>();

        try {
            if (!data.getBoolean("status")) {
                Log.e(AppConfig.TAG, AddFriendsFragment.this.getClass().toString() + " - " + data.get("message").toString());
            }
            else {
                JSONArray array = new JSONArray(data.get("data").toString());

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = new JSONObject(array.getString(i));

                    Friend friend = new Friend();
                    friend.setId(object.getInt("id"));
                    friend.setName(object.getString("name"));
                    friend.setAvatar(object.getString("avatar"));
                    friend.setStatus(Integer.parseInt(object.getString("status")));

                    friends.add(friend);
                }

                addFriendsAdapter.refresh(friends);

                if (addFriendsAdapter.getCount() > 0) {
                    lvAddFriends.setVisibility(View.VISIBLE);
                    btnInvite.setVisibility(View.GONE);
                    tvMessage.setVisibility(View.GONE);
                    tvFriendInvited.setVisibility(View.GONE);
                }
                else {
                    lvAddFriends.setVisibility(View.INVISIBLE);
                    btnInvite.setVisibility(View.VISIBLE);
                    tvMessage.setVisibility(View.VISIBLE);
                }
            }

        } catch (JSONException e) {
            Log.e(AppConfig.TAG, AddFriendsFragment.this.getClass().toString() + " - " + e.getMessage());
        }
    }

    private void search() {
        String url = AppConfig.webserviceUrl("users");
        Map<String, String> params = new HashMap<String, String>();
        params.put("userId", String.valueOf(user.getId()));
        params.put("term", etSearchFriend.getText().toString());

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopLoading();
                        populateFriendsList(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, AddFriendsFragment.this.getClass().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void invite() {
        String url = AppConfig.webserviceUrl("invite");
        Map<String, String> params = new HashMap<String, String>();
        params.put("userId", String.valueOf(user.getId()));
        params.put("email", etSearchFriend.getText().toString());

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                Log.e(AppConfig.TAG, AddFriendsFragment.this.getClass().toString() + " - " +
                                        response.get("message").toString());
                            } else {
                                btnInvite.setVisibility(View.INVISIBLE);
                                tvMessage.setVisibility(View.INVISIBLE);

                                tvFriendInvited.setText(getActivity().getResources().getString(R.string.message_invitation_sent_to) +
                                        " " + etSearchFriend.getText().toString());

                                tvFriendInvited.setVisibility(View.VISIBLE);

                                etSearchFriend.setText("");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        stopLoading();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, AddFriendsFragment.this.getClass().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(AppConfig.WEBSERVICE_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        getActivity().setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        getActivity().setProgressBarIndeterminateVisibility(Boolean.FALSE);
    }
}
