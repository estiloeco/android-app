package br.com.estiloeco.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.AddressDataSource;
import br.com.estiloeco.database.CreditCardDataSource;
import br.com.estiloeco.database.CreditCardFlagDataSource;
import br.com.estiloeco.database.PaymentMethodDataSource;
import br.com.estiloeco.database.PurchaseRequestDataSource;
import br.com.estiloeco.database.PurchaseRequestItemDataSource;
import br.com.estiloeco.database.PurchaseRequestStatusDataSource;
import br.com.estiloeco.database.ShippingTypeDataSource;
import br.com.estiloeco.database.UserDataSource;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.CreditCard;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.PaymentMethod;
import br.com.estiloeco.model.PurchaseRequest;
import br.com.estiloeco.model.PurchaseRequestItem;
import br.com.estiloeco.model.PurchaseRequestStatus;
import br.com.estiloeco.model.ShippingType;
import br.com.estiloeco.model.User;
import br.com.estiloeco.ui.CheckoutShippingActivity;
import br.com.estiloeco.ui.RegisterActivity;
import br.com.estiloeco.utils.AppUtil;
import br.com.estiloeco.utils.GcmUtil;

/**
 * Created by paulomartins on 02/02/15.
 */
public class LoginFragment extends Fragment {

    private User user;
    private Context context;
    private RequestQueue request;
    private ProgressDialog progressDialog;
    // Components
    private EditText etUserEmail;
    private EditText etUserPassword;
    private Button btnCreateAnAccount;
    private Button btnLogin;
    private LoginButton btnLoginWithFacebook;
    private RelativeLayout rlOr;
    private TextView tvForgotPassword;
    // Facebook
    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);

        context = getActivity();

        // Request
        request = Volley.newRequestQueue(context);

        // User object
        user = new User();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_login, container, false);

        components(view);
        componentsAction();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        Session session = Session.getActiveSession();

        if (session != null && (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    private void components(View view) {
        etUserEmail = (EditText) view.findViewById(R.id.etUserEmail);
        etUserPassword = (EditText) view.findViewById(R.id.etUserPassword);
        btnCreateAnAccount = (Button) view.findViewById(R.id.btnCreateAnAccount);
        tvForgotPassword = (TextView) view.findViewById(R.id.tvForgotPassword);
        btnLogin = (Button) view.findViewById(R.id.btnLogin);
        rlOr = (RelativeLayout) view.findViewById(R.id.rlOr);

        // Facebook
        btnLoginWithFacebook = (LoginButton) view.findViewById(R.id.authButton);
        btnLoginWithFacebook.setBackgroundColor(getResources().getColor(R.color.facebook));
        btnLoginWithFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends"));
        btnLoginWithFacebook.setFragment(this);
    }

    private void componentsAction() {
        // Simple Login
        etUserPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER) && validation()) {
                    login();
                    return true;
                }

                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    login();
                }
            }
        });

        // Register
        btnCreateAnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Helper.isEmpty(etUserEmail)) {
                    final AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources()
                            .getString(R.string.message_type_email_forgot_password), R.string.empty, R.string.btn_ok);

                    alertDialog.show();
                } else if (!Helper.isValidEmail(etUserEmail.getText().toString())) {
                    final AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources()
                            .getString(R.string.message_invalid_email), R.string.empty, R.string.btn_ok);

                    alertDialog.show();
                } else {
                    final AlertDialog alertDialog = AppUtil.confirmDialog(context, getResources()
                            .getString(R.string.message_send_email_forgot), 0, 0);

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            forgotPassword();
                        }
                    });

                    alertDialog.show();
                }
            }
        });
    }

    private void loadUserAddresses(JSONArray addressesArray) {
        AddressDataSource addressDataSource = Address.datasource(context);

        try {
            for (int i = 0; i < addressesArray.length(); i++) {
                JSONObject addressObject = addressesArray.getJSONObject(i);

                Address address = new Address();
                address.setId(addressObject.getLong("id"));
                address.setName(addressObject.getString("name"));
                address.setCep(addressObject.getString("cep"));
                address.setStreet(addressObject.getString("street"));
                address.setNumber(addressObject.getString("number"));
                address.setComplement(addressObject.getString("complement"));
                address.setNeighborhood(addressObject.getString("neighborhood"));
                address.setCity(addressObject.getString("city"));
                address.setState(addressObject.getString("state"));
                address.setCountry(addressObject.getString("country"));
                address.setObs(addressObject.getString("obs"));
                address.setPrincipal(addressObject.getInt("principal") == 1 ? true : false);

                addressDataSource.add(address);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadUserCreditCards(JSONArray creditCardsArray) {
        CreditCardDataSource creditCardDataSource = CreditCard.datasource(context);

        try {
            for (int i = 0; i < creditCardsArray.length(); i++) {
                JSONObject creditCardObject = creditCardsArray.getJSONObject(i);

                CreditCard creditCard = new CreditCard();
                creditCard.setId(creditCardObject.getLong("id"));
                creditCard.setName(creditCardObject.getString("name"));
                creditCard.setFlagId(creditCardObject.getLong("flag_id"));
                creditCard.setNumber(creditCardObject.getString("number"));
                creditCard.setExpireDate(creditCardObject.getString("expiration_date"));

                creditCardDataSource.add(creditCard);
            }
        } catch (JSONException e) {
            Log.e(AppConfig.TAG, LoginFragment.this.getClass().toString() + " - " + e.getMessage());
        }
    }

    private void loadUserPurchaseRequests(JSONArray purchaseRequestsArray) {
        PurchaseRequestDataSource purchaseRequestDataSource = PurchaseRequest.datasource(context);
        PurchaseRequestItemDataSource purchaseRequestItemDataSource = PurchaseRequestItem.datasource(context);
        PurchaseRequestStatusDataSource purchaseRequestStatusDataSource = PurchaseRequestStatus.datasource(context);

        try {
            for (int i = 0; i < purchaseRequestsArray.length(); i++) {
                float currentCost = 0;

                JSONObject purchaseRequestObject = purchaseRequestsArray.getJSONObject(i);
                JSONArray purchaseRequestItemsArray = purchaseRequestObject.getJSONArray("items");
                JSONArray purchaseRequestStatusArray = purchaseRequestObject.getJSONArray("status");

                // Purchase Request
                PaymentMethod paymentMethod = PaymentMethod.datasource(context).get(purchaseRequestObject.getLong("payment_method_id"));

                PurchaseRequest purchaseRequest = new PurchaseRequest();
                purchaseRequest.setId(purchaseRequestObject.getLong("id"));
                purchaseRequest.setPaymentMethod(paymentMethod.getId());
                purchaseRequest.setShippingCost((float) purchaseRequestObject.getDouble("shipping_cost"));
                purchaseRequest.setShippingType(purchaseRequestObject.getLong("shipping_type_id"));
                purchaseRequest.setAddress(purchaseRequestObject.getLong("address_id"));
                purchaseRequest.setDate(purchaseRequestObject.getString("created_at"));

                if (paymentMethod.getTag().equals("credit_card")) {
                    purchaseRequest.setCreditCard(purchaseRequestObject.getLong("credit_card_id"));
                }
                else if (paymentMethod.getTag().equals("bank_bill")) {
                    purchaseRequest.setBankBillNumber(purchaseRequestObject.getString("bank_bill_number"));
                    purchaseRequest.setBankBillUserEmail(purchaseRequestObject.getString("bank_bill_user_email"));
                }

                // Purchase Request Items
                for (int j = 0; j < purchaseRequestItemsArray.length(); j++) {
                    JSONObject purchaseRequestItemObject = purchaseRequestItemsArray.getJSONObject(j);

                    PurchaseRequestItem purchaseRequestItem = new PurchaseRequestItem();
                    purchaseRequestItem.setPurchaseRequest(purchaseRequest.getId());
                    purchaseRequestItem.setSku(purchaseRequestItemObject.getInt("sku_id"));
                    purchaseRequestItem.setAmount(purchaseRequestItemObject.getInt("amount"));
                    purchaseRequestItem.setCost((float) purchaseRequestItemObject.getDouble("cost"));

                    currentCost += (float) purchaseRequestItemObject.getDouble("cost");

                    purchaseRequestItemDataSource.add(purchaseRequestItem);
                }

                // Purchase Request Status
                for (int k = 0; k < purchaseRequestStatusArray.length(); k++) {
                    JSONObject purchaseRequestStatusObject = purchaseRequestStatusArray.getJSONObject(k);

                    PurchaseRequestStatus purchaseRequestStatus= new PurchaseRequestStatus();
                    purchaseRequestStatus.setPurchaseRequest(purchaseRequest.getId());
                    purchaseRequestStatus.setTitle(purchaseRequestStatusObject.getString("title"));
                    purchaseRequestStatus.setDescription(purchaseRequestStatusObject.getString("description"));
                    purchaseRequestStatus.setDate(purchaseRequestStatusObject.getString("created_at"));

                    purchaseRequestStatusDataSource.add(purchaseRequestStatus);
                }

                purchaseRequest.setCost(currentCost);
                purchaseRequestDataSource.add(purchaseRequest);
            }
        } catch (JSONException e) {
            Log.e(AppConfig.TAG, LoginFragment.this.getClass().toString() + " - " + e.getMessage());
        }
    }

    private void loadSystemData(JSONObject system) {
        // Payment methods
        PaymentMethodDataSource paymentMethodDataSource = PaymentMethod.datasource(context);

        try {
            JSONArray paymentMethodsArray = system.getJSONArray("payment_methods");

            for (int i = 0; i < paymentMethodsArray.length(); i++) {
                JSONObject paymentMethodObject = paymentMethodsArray.getJSONObject(i);

                PaymentMethod paymentMethod = new PaymentMethod();
                paymentMethod.setId(paymentMethodObject.getLong("id"));
                paymentMethod.setDescription(paymentMethodObject.getString("description"));
                paymentMethod.setTag(paymentMethodObject.getString("tag"));

                paymentMethodDataSource.add(paymentMethod);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Credit Card Flags
        CreditCardFlagDataSource creditCardFlagDataSource = CreditCardFlag.datasource(context);

        try {
            JSONArray creditCardFlagsArray = system.getJSONArray("credit_card_flags");

            for (int i = 0; i < creditCardFlagsArray.length(); i++) {
                JSONObject creditCardFlagObject = creditCardFlagsArray.getJSONObject(i);

                CreditCardFlag creditCardFlag = new CreditCardFlag();
                creditCardFlag.setId(creditCardFlagObject.getLong("id"));
                creditCardFlag.setDescription(creditCardFlagObject.getString("description"));

                creditCardFlagDataSource.add(creditCardFlag);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Shipping types
        ShippingTypeDataSource shippingTypeDataSource = ShippingType.datasource(context);

        try {
            JSONArray shippingTypesArray = system.getJSONArray("shipping_types");

            for (int i = 0; i < shippingTypesArray.length(); i++) {
                JSONObject shippingTypeObject = shippingTypesArray.getJSONObject(i);

                ShippingType shippingType= new ShippingType();
                shippingType.setId(shippingTypeObject.getLong("id"));
                shippingType.setDescription(shippingTypeObject.getString("description"));
                shippingType.setTag(shippingTypeObject.getString("tag"));

                shippingTypeDataSource.add(shippingType);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loginSuccess() {
        stopProgressBarDialog();

        if (user != null) {
            UserDataSource userDatasource = User.datasource(context);
            userDatasource.add(user);

            if (Helper.getBooleanPreference(context, "isCheckout")) {
                Helper.setPreferences(context, "isCheckout", false).commit();

                Intent intent = new Intent(context, CheckoutShippingActivity.class);
                startActivity(intent);
            }

            if (getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    private void loginError() {
        Toast.makeText(getActivity(), R.string.message_login_fail, Toast.LENGTH_LONG).show();
    }

    private void sendToRegister() {
        Intent intent = new Intent(getActivity(), RegisterActivity.class);

        intent.putExtra("facebookId", user.getFacebookId());
        intent.putExtra("facebookAccessToken", user.getFacebookToken());
        intent.putExtra("userFirstName", user.getName());
        intent.putExtra("userLastName", user.getLastName());
        intent.putExtra("userEmail", user.getEmail());
        intent.putExtra("userGender", user.getGender());

        startActivity(intent);
    }

    private boolean validation() {
        boolean status = true;

        if (Helper.isEmpty(etUserEmail) || Helper.isEmpty(etUserPassword)) {
            Toast.makeText(getActivity(), R.string.message_error_login, Toast.LENGTH_SHORT).show();
            status = false;
        }
        else if (!Helper.isValidEmail(etUserEmail.getText().toString())) {
            Toast.makeText(getActivity(), R.string.message_invalid_email, Toast.LENGTH_SHORT).show();
            status = false;
        }

        return status;
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Log.i(AppConfig.TAG, "Facebook Login - logged in");

            Request.executeMeRequestAsync(session, new Request.GraphUserCallback(){
                @Override
                public void onCompleted(GraphUser facebookUser, Response response) {
                    if (facebookUser != null) {
                        JSONObject userObject = facebookUser.getInnerJSONObject();

                        try {
                            user.setFacebookId(facebookUser.getId());
                            user.setName(userObject.get("first_name").toString());
                            user.setLastName(userObject.get("last_name").toString());
                            user.setEmail(userObject.get("email").toString());
                            user.setGender(userObject.get("gender").toString());
                            user.setFacebookToken(session.getAccessToken());

                            checkEmail();
                        } catch (JSONException e) {
                            loginError();
                            Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + e.getMessage());
                        }

                        session.closeAndClearTokenInformation();
                        session.setActiveSession(null);
                    }
                }
            });
        }
        else {
            Log.i(AppConfig.TAG, "Facebook Login - Logged out");
        }
    }

    private void login() {
        String url = AppConfig.webserviceUrl("login");
        Map<String, String> params = new HashMap<String, String>();

        // Params
        params.put("email", etUserEmail.getText().toString());
        params.put("password", AppUtil.generateUserPassword(etUserPassword.getText().toString()));
        params.put("gcmId", GcmUtil.getId(context));
        params.put("type", AppConfig.DEVICE_TYPE);

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("status")) {
                                loginError();
                                Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + response.get("message").toString());
                            }
                            else {
                                JSONObject userObject = new JSONObject(response.get("data").toString());

                                user.setId(userObject.getLong("id"));
                                user.setName(userObject.getString("name"));
                                user.setLastName(userObject.getString("last_name"));
                                user.setAvatar(userObject.getString("avatar"));
                                user.setGender(userObject.getString("gender"));
                                user.setBirthdate(userObject.getString("birthdate"));
                                user.setDocument(userObject.getString("document"));
                                user.setEmail(userObject.getString("email"));

                                // System data
                                loadSystemData(userObject.getJSONObject("system"));

                                // Addresses
                                loadUserAddresses(userObject.getJSONArray("addresses"));

                                // Credit Cars
                                loadUserCreditCards(userObject.getJSONArray("credit_cards"));

                                // Purchase Requests
                                loadUserPurchaseRequests(userObject.getJSONArray("purchase_requests"));

                                loginSuccess();
                            }
                        } catch (JSONException e) {
                            loginError();
                            Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + e.getMessage());
                        }

                        stopLoading();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();
                        Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void checkEmail() {
        String url = AppConfig.webserviceUrl("login/email");
        Map<String, String> params = new HashMap<String, String>();

        // Params
        params.put("email", user.getEmail());
        params.put("facebookId", user.getFacebookId());
        params.put("facebookToken", user.getFacebookToken());
        params.put("gcmId", GcmUtil.getId(context));
        params.put("type", AppConfig.DEVICE_TYPE);

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {
                                JSONObject userObject = new JSONObject(response.get("data").toString());

                                user.setId(userObject.getLong("id"));
                                user.setAvatar(userObject.getString("avatar"));
                                user.setGender(userObject.getString("gender"));
                                user.setBirthdate(userObject.getString("birthdate"));
                                user.setDocument(userObject.getString("document"));

                                // System data
                                loadSystemData(userObject.getJSONObject("system"));

                                // Addresses
                                loadUserAddresses(userObject.getJSONArray("addresses"));

                                // Credit Cars
                                loadUserCreditCards(userObject.getJSONArray("credit_cards"));

                                // Purchase Requests
                                loadUserPurchaseRequests(userObject.getJSONArray("purchase_requests"));

                                loginSuccess();
                            }
                            else {
                                sendToRegister();
                            }
                        } catch (JSONException e) {
                            sendToRegister();
                            Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + e.getMessage());
                        }

                        stopProgressBarDialog();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    private void forgotPassword() {
        String url = AppConfig.webserviceUrl("login/forgot-password");
        Map<String, String> params = new HashMap<String, String>();

        // Params
        params.put("email", etUserEmail.getText().toString());

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {
                                Toast.makeText(context, getResources().getString(R.string.message_email_sended_to_recover_password),
                                        Toast.LENGTH_LONG).show();

                                etUserEmail.setText("");
                            }
                            else {
                                Toast.makeText(context, getResources().getString(R.string.message_error_on_forgot_password),
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            sendToRegister();
                            Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + e.getMessage());
                        }

                        stopProgressBarDialog();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopProgressBarDialog();
                        Log.e(AppConfig.TAG, LoginFragment.this.getClass().getName().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(AppConfig.WEBSERVICE_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.add(jsonRequest);

        // Starts loading
        startProgressDialog();
    }

    private void startProgressDialog() {
        if (progressDialog == null && context != null) {
            progressDialog = ProgressDialog.show(context, null, getResources().getString(R.string.message_wait), true);
        }
    }

    private void startLoading() {
        if (getActivity() != null) {
            getActivity().setProgressBarIndeterminateVisibility(Boolean.TRUE);
        }
    }

    private void stopLoading() {
        if (getActivity() != null) {
            getActivity().setProgressBarIndeterminateVisibility(Boolean.FALSE);
        }
    }

    private void stopProgressBarDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
