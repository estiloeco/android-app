package br.com.estiloeco.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.FriendsAdapter;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.custom.CustomJsonObjectRequest;
import br.com.estiloeco.database.FriendsDataSource;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.User;

public class FriendsFragment extends Fragment {

    private Context context;
    private RequestQueue request;
    protected View view;
    static private FriendsAdapter friendsAdapter;
    private User user;
    static private FriendsDataSource friendsDataSource;
    // Components
    static private ListView lvFriends;
    static private TextView tvNoFriends;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_friends, container, false);

        context = getActivity();

        // Request
        request = Volley.newRequestQueue(context);

        // User
        user = User.logged(context);

        // Data source
        friendsDataSource = Friend.datasource(context);

        components();
        componentsData();

        friends();

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        request.cancelAll(AppConfig.TAG);
    }

    static public void refresh() {
        List<Friend> friends = friendsDataSource.getAll();
        friendsAdapter.refresh(friends);

        if (friends.size() == 0) {
            lvFriends.setVisibility(View.GONE);
            tvNoFriends.setVisibility(View.VISIBLE);
        }
        else {
            lvFriends.setVisibility(View.VISIBLE);
            tvNoFriends.setVisibility(View.INVISIBLE);
        }
    }

    private void components() {
        tvNoFriends = (TextView) view.findViewById(R.id.tvNoFriends);
        lvFriends = (ListView) view.findViewById(R.id.lvFriends);

        // Adapter
        friendsAdapter = new FriendsAdapter(context, user.getId(), getActivity(), lvFriends, tvNoFriends);

        lvFriends.setAdapter(friendsAdapter);
    }

    private void componentsData() {
        List<Friend> friends = friendsDataSource.getAll();
        friendsAdapter.refresh(friends);

        if (friends.size() == 0) {
            lvFriends.setVisibility(View.GONE);
            tvNoFriends.setVisibility(View.VISIBLE);
        }
        else {
            lvFriends.setVisibility(View.VISIBLE);
            tvNoFriends.setVisibility(View.INVISIBLE);
        }
    }

    private void populateFriendsList(JSONObject data) {
        try {
            if (!data.getBoolean("status")) {
                Log.e(AppConfig.TAG, FriendsFragment.this.getClass().toString() + " - " + data.get("message").toString());
            }
            else {
                JSONArray array = new JSONArray(data.get("data").toString());

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = new JSONObject(array.getString(i));

                    Friend friend = new Friend();
                    friend.setId(object.getInt("id"));
                    friend.setName(object.getString("name"));
                    friend.setAvatar(object.getString("avatar"));
                    friend.setStatus(object.getInt("status"));

                    friendsDataSource.add(friend);
                }

                componentsData();
            }

        } catch (JSONException e) {
            Log.e(AppConfig.TAG, FriendsFragment.this.getClass().toString() + " - " + e.getMessage());
        }
    }

    private void friends() {
        String url = AppConfig.webserviceUrl("profile/friends");
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(user.getId()));

        CustomJsonObjectRequest jsonRequest = new CustomJsonObjectRequest(url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        stopLoading();
                        friendsDataSource.deleteAll();
                        populateFriendsList(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopLoading();

                        Log.e(AppConfig.TAG, FriendsFragment.this.getClass().toString() + " - " + error);
                        Toast.makeText(context, R.string.message_network_error, Toast.LENGTH_SHORT).show();

                        componentsData();
                    }
                }
        );

        jsonRequest.setTag(AppConfig.TAG);
        request.add(jsonRequest);

        // Starts loading
        startLoading();
    }

    private void startLoading() {
        getActivity().setProgressBarIndeterminateVisibility(Boolean.TRUE);
    }

    private void stopLoading() {
        if (getActivity() != null) {
            getActivity().setProgressBarIndeterminateVisibility(Boolean.FALSE);
        }
    }
}
