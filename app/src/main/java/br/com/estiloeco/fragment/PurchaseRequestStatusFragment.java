package br.com.estiloeco.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.estiloeco.R;
import br.com.estiloeco.adapter.PurchaseRequestStatusAdapter;
import br.com.estiloeco.model.PurchaseRequest;
import br.com.estiloeco.model.PurchaseRequestStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class PurchaseRequestStatusFragment extends Fragment {

    protected View view;
    private PurchaseRequest purchaseRequest;
    // Components
    protected ListView lvPurchaseRequestStatus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_purchase_request_status, container, false);

        if (getActivity().getIntent().hasExtra("id")) {
            long id = getActivity().getIntent().getExtras().getLong("id");
            purchaseRequest = PurchaseRequest.datasource(getActivity()).get(id);
        }

        components();
        componentsData();

        return view;
    }

    private void components() {
        lvPurchaseRequestStatus = (ListView) view.findViewById(R.id.lvPurchaseRequestStatus);
    }

    private void componentsData() {
        ArrayList<PurchaseRequestStatus> purchaseRequestStatuses = purchaseRequest.getStatus();

        PurchaseRequestStatusAdapter purchaseRequestStatusAdapter =
                new PurchaseRequestStatusAdapter(getActivity(), purchaseRequestStatuses);
        lvPurchaseRequestStatus.setAdapter(purchaseRequestStatusAdapter);
    }

}
