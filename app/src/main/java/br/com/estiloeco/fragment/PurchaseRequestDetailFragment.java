package br.com.estiloeco.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.estiloeco.R;
import br.com.estiloeco.helpers.Helper;
import br.com.estiloeco.model.Address;
import br.com.estiloeco.model.CreditCard;
import br.com.estiloeco.model.CreditCardFlag;
import br.com.estiloeco.model.PaymentMethod;
import br.com.estiloeco.model.PurchaseRequest;
import br.com.estiloeco.model.ShippingType;
import br.com.estiloeco.model.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class PurchaseRequestDetailFragment extends Fragment {

    private View view;
    private User user;
    private PurchaseRequest purchaseRequest;
    // Components
    private TextView tvAddress;
    private TextView tvShippingLimit;
    private TextView tvMessage;
    private TextView tvBankBillNumberLabel;
    private TextView tvBankBillNumber;
    private TextView tvPaymentMethod;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_purchase_request_detail, container, false);

        // User
        user = User.logged(getActivity());

        if (getActivity().getIntent().hasExtra("id")) {
            long id = getActivity().getIntent().getExtras().getLong("id");
            purchaseRequest = PurchaseRequest.datasource(getActivity()).get(id);
        }

        components();
        componentsData();

        return view;
    }

    private void components() {
        tvAddress = (TextView) view.findViewById(R.id.tvAddress);
        tvShippingLimit = (TextView) view.findViewById(R.id.tvShippingLimit);
        tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        tvBankBillNumberLabel = (TextView) view.findViewById(R.id.tvBankBillNumberLabel);
        tvBankBillNumber = (TextView) view.findViewById(R.id.tvBankBillNumber);
        tvPaymentMethod = (TextView) view.findViewById(R.id.tvPaymentMethod);
    }

    private void componentsData() {
        if (purchaseRequest != null) {
            CreditCard creditCard = CreditCard.datasource(getActivity()).get(purchaseRequest.getCreditCard());
            Address address = Address.datasource(getActivity()).get(purchaseRequest.getAddress());
            PaymentMethod paymentMethod = PaymentMethod.datasource(getActivity()).get(purchaseRequest.getPaymentMethod());

            tvShippingLimit.setText(ShippingType.datasource(getActivity()).get(
                    purchaseRequest.getShippingType()).getShortDescription());

            if (address != null) {
                tvAddress.setText(address.getName() + " (" + address.getStreet() + ", " +
                        address.getNumber() + ", " + address.getComplement() + ")");
            }

            if (paymentMethod != null) {
                if (paymentMethod.getTag().equals("bank_bill")) {
                    tvBankBillNumberLabel.setVisibility(View.VISIBLE);
                    tvBankBillNumber.setVisibility(View.VISIBLE);

                    tvPaymentMethod.setText(paymentMethod.getDescription());

                    tvBankBillNumber.setText(String.valueOf(purchaseRequest.getBankBillNumber()));
                    tvMessage.setText(getResources().getString(R.string.message_purchase_request_finished_bank_bill)
                            .replace("#email#", purchaseRequest.getBankBillUserEmail()));
                }
                else {
                    tvBankBillNumberLabel.setVisibility(View.GONE);
                    tvBankBillNumber.setVisibility(View.GONE);

                    if (creditCard != null) {
                        CreditCardFlag creditCardFlag = CreditCardFlag.datasource(getActivity()).get(creditCard.getFlagId());

                        if (creditCardFlag != null) {
                            tvPaymentMethod.setText(paymentMethod.getDescription() + " (" +
                                    creditCardFlag.getDescription() + " " + getResources().getString(R.string.title_final) +
                                    " " + Helper.finalString(creditCard.getNumber(), 4) + ")");
                        }
                    }
                    else {
                        tvPaymentMethod.setText(paymentMethod.getDescription());
                    }


                    tvMessage.setText(getResources().getString(R.string.message_purchase_request_finished)
                            .replace("#email#", user.getEmail()));
                }
            }
        }
    }

}
