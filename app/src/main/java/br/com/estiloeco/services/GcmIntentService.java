package br.com.estiloeco.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import br.com.estiloeco.R;
import br.com.estiloeco.config.AppConfig;
import br.com.estiloeco.database.NotificationsDataSource;
import br.com.estiloeco.model.Friend;
import br.com.estiloeco.model.Notification;
import br.com.estiloeco.model.User;
import br.com.estiloeco.receivers.GcmBroadcastReceiver;
import br.com.estiloeco.ui.MainActivity;
import br.com.estiloeco.utils.AndroidNotificationUtil;

/**
 * Created by paulomartins on 24/02/15.
 */
public class GcmIntentService extends IntentService {

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(GcmIntentService.this);
        String key, messageType = gcm.getMessageType(intent);

        if (extras != null) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.w(AppConfig.TAG, "GCM - " + extras.toString());
            }
            else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Log.w(AppConfig.TAG, "GCM - " + extras.toString());
            }
            else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                key = extras.getString("key");

                // Friends actions
                if (key.equals(AppConfig.GCM_FRIEND_KEY)) {
                    friend(key, extras);
                }
                else if (key.equals(AppConfig.GCM_SHARE_LOOK_KEY)) {
                    shareLook(extras);
                }
            }
        }

        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void friend(String key, Bundle extras) {
        String action = extras.getString("action");
        long userId = Long.parseLong(extras.getString("friendId"));
        String userName = extras.getString("friendName");
        String userAvatar = extras.getString("friendAvatar");

        // Notifications
        if (action.equals("new")) {
            // Add notification
            NotificationsDataSource notificationsDataSource = Notification.datasource(GcmIntentService.this);

            Notification notification = new Notification();
            notification.setType(key);
            notification.setUserId(userId);
            notification.setUserName(userName);
            notification.setUserAvatar(userAvatar);

            notificationsDataSource.add(notification);

            // Intent
            Intent intent = new Intent(GcmIntentService.this, MainActivity.class);
            intent.putExtra("directEntry", "notifications");

            AndroidNotificationUtil.sendNotitification(GcmIntentService.this, AppConfig.APP_NAME,
                    userName + " " + getResources().getString(R.string.message_new_friend), AndroidNotificationUtil.NEW_FRIEND, intent);
        }
        else if (action.equals("accepted")) {
            // Intent
            Intent intent = new Intent(GcmIntentService.this, MainActivity.class);
            intent.putExtra("directEntry", "friends");

            // Update friend
            Friend.datasource(GcmIntentService.this).updateStatus(userId, Friend.STATUS_ACCEPTED);

            AndroidNotificationUtil.sendNotitification(GcmIntentService.this, AppConfig.APP_NAME,
                    userName + " " + getResources().getString(R.string.message_friend_accept), AndroidNotificationUtil.FRIEND_ACCEPT, intent);
        }
    }

    private void shareLook(Bundle extras) {
        String userName = extras.getString("friendName");
        long lookId = Long.parseLong(extras.getString("lookId"));

        // Intent
        Intent intent = new Intent(GcmIntentService.this, MainActivity.class);
        intent.putExtra("goToLook", lookId);

        AndroidNotificationUtil.sendNotitification(GcmIntentService.this, AppConfig.APP_NAME,
                userName + " " + getResources().getString(R.string.message_frind_share_with_you), AndroidNotificationUtil.SHARE_LOOK, intent);
    }
}
