package br.com.estiloeco.custom;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import br.com.estiloeco.config.AppConfig;

/**
 * Created by paulomartins on 18/02/15.
 */
public class CustomJsonArrayRequest extends Request<JSONArray> {

    private Listener<JSONArray> response;
    private Map<String, String> params;

    public CustomJsonArrayRequest(int method, String url, Map<String, String> params, Listener<JSONArray> response, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.params = params;
        this.response = response;

        Log.i(AppConfig.TAG, this.getClass().getName().toString() + " - Call: " + url);
    }

    public CustomJsonArrayRequest(String url, Map<String, String> params, Listener<JSONArray> response, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.params = params;
        this.response = response;

        Log.i(AppConfig.TAG, this.getClass().getName().toString() + " - Call: " + url);
    }

    public Map<String, String> getParams() throws AuthFailureError {
        return params;
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Authorization", AppConfig.WEBSERVICE_AUTHORIZATION_CODE);

        return header;
    }

    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONArray(json), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            Log.e(AppConfig.TAG, this.getClass().getName().toString() + " - " + e.getMessage());
        } catch (JSONException e) {
            Log.e(AppConfig.TAG, this.getClass().getName().toString() + " - " + e.getMessage());
        }

        return null;
    }

    @Override
    protected void deliverResponse(JSONArray response) {
        this.response.onResponse(response);
    }
}
