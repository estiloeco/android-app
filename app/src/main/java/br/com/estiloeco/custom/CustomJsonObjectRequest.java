package br.com.estiloeco.custom;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import br.com.estiloeco.config.AppConfig;

/**
 * Created by paulomartins on 18/02/15.
 */
public class CustomJsonObjectRequest extends Request<JSONObject> {

    private Listener<JSONObject> response;
    private Map<String, String> params;

    public CustomJsonObjectRequest(int method, String url, Map<String, String> params, Listener<JSONObject> response, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.params = params;
        this.response = response;

        Log.i(AppConfig.TAG, this.getClass().getName().toString() + " - Call: " + url);
    }

    public CustomJsonObjectRequest(String url, Map<String, String> params, Listener<JSONObject> response, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.params = params;
        this.response = response;

        Log.i(AppConfig.TAG, this.getClass().getName().toString() + " - Call: " + url);
    }

    public Map<String, String> getParams() throws AuthFailureError {
        return params;
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Authorization", AppConfig.WEBSERVICE_AUTHORIZATION_CODE);

        return header;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(json), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            Log.e(AppConfig.TAG, this.getClass().getName().toString() + " - " + e.getMessage());
        } catch (JSONException e) {
            Log.e(AppConfig.TAG, this.getClass().getName().toString() + " - " + e.getMessage());
        }

        return null;
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        this.response.onResponse(response);
    }
}
