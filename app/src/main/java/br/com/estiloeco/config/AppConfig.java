package br.com.estiloeco.config;

import java.util.ArrayList;

/**
 * Created by paulomartins on 09/01/15.
 */
public class AppConfig {
    public static final String APP_NAME = "Estilo & Co";
    private static final String WEBSERVICE_URL = "http://192.168.1.102/personal/estilo-e-co/webservice";
    public static final String WEBSERVICE_CEP_URL = "http://cep.correiocontrol.com.br/";
    public static final String ASSETS_URL = "http://192.168.1.102/personal/estilo-e-co/webservice/public/";
    public static final String TAG = "EstiloECo";
    public static final String WEBSERVICE_AUTHORIZATION_CODE = "dsa786";
    public static final int WEBSERVICE_TIME_OUT = 10000;
    public static final String DEVICE_TYPE = "android";
    // Security
    public static final String SECURITY_KEY = "IYUDSA^&*DSA()(*L:K#@JLK#@LK";
    // Gcm
    public static String GOOGLE_GCM_SENDER_ID = "460967172738";
    public static int GOOGLE_PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String GCM_FRIEND_KEY = "friend";
    public static final String GCM_SHARE_LOOK_KEY = "shareLook";

    public static String webserviceUrl(String uri) {
        return WEBSERVICE_URL + "/" + uri;
    }

    public static String webserviceUrl(String uri, ArrayList<String> params) {
        String url = WEBSERVICE_URL + "/" + uri;

        for (int i = 0; i < params.size(); i++) {
            url += "/" + params.get(i);
        }

        return url;
    }
}
